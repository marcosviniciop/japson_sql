USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_rnUpdInformacionCliente]    Script Date: 15/6/2022 11:23:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






--exec [sp_rnUpdInformacionCliente] ,

ALTER procedure [dbo].[sp_rnUpdInformacionCliente] (
@i_codSucursal int,
@i_codCliente int,
@i_cedula varchar (50),
@i_telefono varchar (50),
@i_direccion varchar (50),
@i_email varchar (100),
@i_contactoFacebook varchar (100),
@i_telefonoMovil varchar (100),
@i_telefonoParticular varchar (100),
@i_telefonoOficina varchar (100),
--campos agregados para actualizar la informacion
@i_sector varchar (100),
@i_callePrincipal varchar (100), 
@i_numeracion varchar (25),
@i_calleSecundaria varchar (100),
@i_urbanizacion varchar (100),
@i_referencia varchar (100)
)
AS
BEGIN
declare @posicion int,
	@TipoTlf varchar(10),
	@v_tieneDireccion int

/*actualizar cedula o ruc*/
if (@i_cedula <> '-1')
begin
update cliente set cedula_Ruc=@i_cedula
where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
end

/*actualizar telefono*/
/*if (@i_telefono <> '-1')
begin
update cliente set telefono1=@i_telefono 
where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
end*/

/*actualizar direccion */
if (@i_direccion <> '-1')
begin
update cliente set direccion=@i_direccion 
where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
end

/*actualizar email*/
if (@i_email <> '-1')
begin
update cliente set correoFacturacionElectronica=@i_email 
where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
end

/*actualizar facebook*/
if (@i_contactoFacebook <> '-1')
begin
update cliente set contacto_facebook=@i_contactoFacebook 
where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
end

/*actualizar celuar*/
if (@i_telefonoMovil <> '-1')
begin
	select @posicion = 0, @TipoTlf='C'
	select 
	@posicion = 
	case	
		when isnull(telefono1,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 1
		when isnull(telefono2,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 2
		when isnull(telefono3,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 3
		when isnull(telefono4,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 4
		when isnull(telefono5,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 5
		when isnull(telefono6,'') = @i_telefonoMovil and isnull(@i_telefonoMovil,'-1')<>'-1' then 6
	end
	from cliente
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	if (@posicion > 0)
	begin
	update cliente 
		set telefono1 = case when @posicion =1 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono1 end ,
			telefono2 = case when @posicion =2 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono2 end ,
			telefono3 = case when @posicion =3 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono3 end ,
			telefono4 = case when @posicion =4 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono4 end ,
			telefono5 = case when @posicion =5 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono5 end ,
			telefono6 = case when @posicion =6 and isnull(@i_telefonoMovil,'-1')<>'-1' then @i_telefonoMovil else telefono6 end,
			tipoTlf1 = case when @posicion =1 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf1 end ,
			tipoTlf2 = case when @posicion =2 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf2 end ,
			tipoTlf3 = case when @posicion =3 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf3 end ,
			tipoTlf4 = case when @posicion =4 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf4 end ,
			tipoTlf5 = case when @posicion =5 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf5 end ,
			tipoTlf6 = case when @posicion =6 and isnull(@i_telefonoMovil,'-1')<>'-1' then @TipoTlf else tipoTlf6 end 
		where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end 
	else
	begin
	update cliente 
	set telefono1 = case when tipoTlf1 =@TipoTlf and isnull(telefono1,'') <>'' then @i_telefonoMovil else telefono1 end ,
		telefono2 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 =@TipoTlf and isnull(telefono2,'') <>'' then @i_telefonoMovil else telefono2 end ,
		telefono3 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 =@TipoTlf and isnull(telefono3,'') <>'' then @i_telefonoMovil else telefono3 end ,
		telefono4 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 =@TipoTlf and isnull(telefono4,'') <>'' then @i_telefonoMovil else telefono4 end ,
		telefono5 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 =@TipoTlf and isnull(telefono5,'') <>'' then @i_telefonoMovil else telefono5 end , 
		telefono6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 =@TipoTlf and isnull(telefono6,'') <>'' then @i_telefonoMovil 
						 when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then @i_telefonoMovil 	else telefono6 end,
		tipoTlf6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then  @TipoTlf else tipoTlf6 end 
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end
end

/*actualizar CASA*/
if (@i_telefonoParticular <> '-1')
begin
	select @posicion = 0, @TipoTlf='A'
	select 
	@posicion = 
	case	
		when isnull(telefono1,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 1
		when isnull(telefono2,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 2
		when isnull(telefono3,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 3
		when isnull(telefono4,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 4
		when isnull(telefono5,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 5
		when isnull(telefono6,'') = @i_telefonoParticular and isnull(@i_telefonoParticular,'-1')<>'-1' then 6
	end
	from cliente
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	if (@posicion > 0)
	begin
	update cliente 
		set telefono1 = case when @posicion =1 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono1 end ,
			telefono2 = case when @posicion =2 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono2 end ,
			telefono3 = case when @posicion =3 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono3 end ,
			telefono4 = case when @posicion =4 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono4 end ,
			telefono5 = case when @posicion =5 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono5 end ,
			telefono6 = case when @posicion =6 and isnull(@i_telefonoParticular,'-1')<>'-1' then @i_telefonoParticular else telefono6 end,
			tipoTlf1 = case when @posicion =1 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf1 end ,
			tipoTlf2 = case when @posicion =2 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf2 end ,
			tipoTlf3 = case when @posicion =3 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf3 end ,
			tipoTlf4 = case when @posicion =4 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf4 end ,
			tipoTlf5 = case when @posicion =5 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf5 end ,
			tipoTlf6 = case when @posicion =6 and isnull(@i_telefonoParticular,'-1')<>'-1' then @TipoTlf else tipoTlf6 end 
		where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end 
	else
	begin
	update cliente 
	set telefono1 = case when tipoTlf1 =@TipoTlf and isnull(telefono1,'') <>'' then @i_telefonoParticular else telefono1 end ,
		telefono2 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 =@TipoTlf and isnull(telefono2,'') <>'' then @i_telefonoParticular else telefono2 end ,
		telefono3 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 =@TipoTlf and isnull(telefono3,'') <>'' then @i_telefonoParticular else telefono3 end ,
		telefono4 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 =@TipoTlf and isnull(telefono4,'') <>'' then @i_telefonoParticular else telefono4 end ,
		telefono5 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 =@TipoTlf and isnull(telefono5,'') <>'' then @i_telefonoParticular else telefono5 end , 
		telefono6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 =@TipoTlf and isnull(telefono6,'') <>'' then @i_telefonoParticular 
						 when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then @i_telefonoParticular 	else telefono6 end ,
		tipoTlf6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then  @TipoTlf else tipoTlf6 end 
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end
end
/*actualizar telefono trabajo*/
if (@i_telefonoOficina <> '-1')
begin
	select @posicion = 0, @TipoTlf='T'
	select 
	@posicion = 
	case	
		when isnull(telefono1,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 1
		when isnull(telefono2,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 2
		when isnull(telefono3,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 3
		when isnull(telefono4,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 4
		when isnull(telefono5,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 5
		when isnull(telefono6,'') = @i_telefonoOficina and isnull(@i_telefonoOficina,'-1')<>'-1' then 6
	end
	from cliente
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	if (@posicion > 0)
	begin
	update cliente 
		set telefono1 = case when @posicion =1 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono1 end ,
			telefono2 = case when @posicion =2 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono2 end ,
			telefono3 = case when @posicion =3 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono3 end ,
			telefono4 = case when @posicion =4 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono4 end ,
			telefono5 = case when @posicion =5 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono5 end ,
			telefono6 = case when @posicion =6 and isnull(@i_telefonoOficina,'-1')<>'-1' then @i_telefonoOficina else telefono6 end,
			tipoTlf1 = case when @posicion =1 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf1 end ,
			tipoTlf2 = case when @posicion =2 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf2 end ,
			tipoTlf3 = case when @posicion =3 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf3 end ,
			tipoTlf4 = case when @posicion =4 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf4 end ,
			tipoTlf5 = case when @posicion =5 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf5 end ,
			tipoTlf6 = case when @posicion =6 and isnull(@i_telefonoOficina,'-1')<>'-1' then @TipoTlf else tipoTlf6 end 
		where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end 
	else
	begin
	update cliente 
	set telefono1 = case when tipoTlf1 =@TipoTlf and isnull(telefono1,'') <>'' then @i_telefonoOficina else telefono1 end ,
		telefono2 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 =@TipoTlf and isnull(telefono2,'') <>'' then @i_telefonoOficina else telefono2 end ,
		telefono3 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 =@TipoTlf and isnull(telefono3,'') <>'' then @i_telefonoOficina else telefono3 end ,
		telefono4 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 =@TipoTlf and isnull(telefono4,'') <>'' then @i_telefonoOficina else telefono4 end ,
		telefono5 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 =@TipoTlf and isnull(telefono5,'') <>'' then @i_telefonoOficina else telefono5 end , 
		telefono6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 =@TipoTlf and isnull(telefono6,'') <>'' then @i_telefonoOficina 
						 when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then @i_telefonoOficina 	else telefono6 end,
		tipoTlf6 = case when tipoTlf1 <>@TipoTlf and tipoTlf2 <>@TipoTlf and tipoTlf3 <>@TipoTlf and tipoTlf4 <>@TipoTlf and tipoTlf5 <>@TipoTlf and tipoTlf6 <>@TipoTlf then  @TipoTlf else tipoTlf6 end 
	where codCliente=@i_codCliente and codCiudad_Sucursal=@i_codSucursal
	end
end

/*actualizar sector 
if (@i_sector <> '-1')
begin
update direccion set barrioSector = @i_sector
where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
end*/

--cambios 307
select @v_tieneDireccion = count(*) from direccion where codCliente = @i_codCliente and codCiudadSucursal = @i_codSucursal;
if(@v_tieneDireccion >= 1)
begin
	print('ACTUALIZAR INFORMACIÓN DE DIRECCION');
	/*UPDATE DIRECCION SET
	barrioSector =@i_sector,
	callePrincipal = @i_callePrincipal,
	numeracion = @i_numeracion,
	calleSecundaria	= @i_calleSecundaria,
	Urbanizacion = @i_urbanizacion,
	referencia = @i_referencia,
	fActualizacion = getDate(),
	fechaVencimiento = (SELECT DATEADD(YEAR,1,GETDATE())),
	informacionActualizada = 'S'
	where codCiudadSucursal= @i_codSucursal and  codcliente = @i_codCliente*/
	/*actualizar sector */
	if (@i_sector <> '-1')
	begin
	update direccion set barrioSector = @i_sector
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

/*actualizar callePrincipal */
	if (@i_callePrincipal <> '-1')
	begin
	update direccion set callePrincipal = @i_callePrincipal
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

	/*actualizar numeracion */
	if (@i_numeracion <> '-1')
	begin
	update direccion set numeracion = @i_numeracion
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

	/*actualizar calleSecundaria */
	if (@i_calleSecundaria <> '-1')
	begin
	update direccion set calleSecundaria = @i_calleSecundaria
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

	/*actualizar urbanizacion */
	if (@i_urbanizacion <> '-1')
	begin
	update direccion set Urbanizacion = @i_urbanizacion
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

	/*actualizar referencia */
	if (@i_referencia <> '-1')
	begin
	update direccion set referencia = @i_referencia
	where codCliente=@i_codCliente and codCiudadSucursal=@i_codSucursal
	end

	-- ACTUALIZAR FECHAS Y ESTADOS
	update direccion set 
	fActualizacion = getDate(), 
	fechaVencimiento = (SELECT DATEADD(YEAR,1,GETDATE())), 
	informacionActualizada = 'S'
	where codCiudadSucursal = @i_codSucursal and codCliente = @i_codCliente
	end
else
begin
INSERT INTO [dbo].[direccion]
           ([codCiudadSucursal]
           ,[codCliente]
           ,[barrioSector]
           ,[callePrincipal]
           ,[numeracion]
           ,[calleSecundaria]
           ,[Urbanizacion]
           ,[referencia]
           ,[fActualizacion]
           ,[fechaVencimiento]
           ,[informacionActualizada])
     VALUES
           (@i_codSucursal,@i_codCliente,@i_sector
           ,@i_callePrincipal
           ,@i_numeracion
           ,@i_calleSecundaria
           ,@i_urbanizacion
           ,@i_referencia
           ,GETDATE()
           ,GETDATE()
           ,'S')
end

print('ACTUALIZAR INFORMACIÓN DE DIRECCION FINALIZADO');




select 1 as estado

END


GO

