USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_cuLstCliente]    Script Date: 15/6/2022 11:22:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









--exec sp_cuLstCliente 1,182941
ALTER     PROCEDURE [dbo].[sp_cuLstCliente](
  @i_codSucursal	int,
  @i_codCliente	int
)
AS
BEGIN

  select c.*, isnull(u.nombre_usuario,'') as nombreUsuarioVerifica,
    ci.desCiudad, ci.abreviatura,
              case
              when (cl.codsucursal is not null) then '1'
              else '0'
              end as tarjetaPuntonet,
              case
              when (cl.codsucursal is not null) and (vn.codSucursal is null) and (isnull(l.cliEstatus,0) <> 0) then '1'
              when (cl.codsucursal is not null) and (vn.codSucursal is not null) then '-1'
              else '0'
              end as  estadoTarjeta,
              isnull(cl.fechaInclusion,'') as fechaInclusion,
              isnull(CONVERT(VARCHAR(10), t.tarFechaEmision, 111),'') as fechaOriginal,
              cl.renovado as renovado,
              isnull(cl.secuencial,'') as secuencial,
              case
              when DATEDIFF(year, t.tarFechaEmision, GETDATE())% 2 = 0 
			  and DATEADD(year, DATEDIFF(year, t.tarFechaEmision, GETDATE()), t.tarFechaEmision)>GETDATE() 
			  then DATEADD(year, DATEDIFF(year, t.tarFechaEmision, GETDATE()), t.tarFechaEmision)
			  when DATEDIFF(year, t.tarFechaEmision, GETDATE())% 2 = 0 
			  and DATEADD(year, DATEDIFF(year, t.tarFechaEmision, GETDATE()), t.tarFechaEmision)<GETDATE() 
			  then DATEADD(year, DATEDIFF(year, t.tarFechaEmision, GETDATE())+2, t.tarFechaEmision)
			  else DATEADD(year, DATEDIFF(year, t.tarFechaEmision, GETDATE())+1, t.tarFechaEmision)
			  end as fechaCaducidad,
              --isnull(t.tarFechaExp,'') as fechaCaducidad,
              isnull(l.clisaldo,0) as numeroPuntos,
    c.facturacionElectronica,
              isnull((cr.apellidos + ' ' +cr.nombres + ' ' +cr.nombre_Empresa),'')as clienteReferido--dceli
    , dbo.validarActualizacionCliente(c.codCiudad_Sucursal, c.codcliente) as     datos_actualizados
    --ftorres
    ,sc.registraTCActiva
    ,sc.gastoFinanciero
    ,sc.endeudamientoPromedio
	, case when isnull(sp.codServicios_adicionales_contratoPlan,0) > 0 then 1
  else 0 end as premium,--AGREGAR INNER JOIN A LA TABLA DE DERECCION PARA OBTENER LOS NUEVOS CAMPOS
   dir.barrioSector, dir.callePrincipal,dir.calleSecundaria, dir.numeracion, 
  dir.referencia, dir.Urbanizacion, ISNULL(convert(varchar(16),convert(datetime,dir.fActualizacion),120),'') as FechaActualizacion,
  ISNULL(convert(varchar(16),convert(datetime,dir.fechaVencimiento),120),'') as FechaVencimiento, dir.informacionActualizada
  from  cliente c 
  left join direccion dir on dir.codCiudadSucursal = c.codCiudad_Sucursal and dir.codCliente = c.codCliente
    inner join ciudad ci on c.codciudad_sucursal = ci.codCiudad
    LEFT JOIN cliente_score_crediticio sc on (c.codCliente=sc.codCliente and c.codCiudad_Sucursal=sc.codCiudad_Sucursal )
    left outer join usuario u on c.codUsuarioVerifica = u.codusuario
    left outer join clubpuntonet cl on c.codciudad_sucursal = cl.codsucursal
                                       and c.codcliente = cl.codcliente
    left outer join lealtad_clientes l on c.cedula_ruc = l.clicodigo
    left outer join view_noDeseaTarjeta vn on c.codciudad_sucursal = vn.codSucursal and
                                              c.codcliente = vn.codcliente
    left outer join cliente cr on  c.codCiudad_Sucursal_Referido=cr.codCiudad_Sucursal and c.codCliente_Referido=cr.codCliente--dceli
    left outer join tarjeta t on t.cliCodigo=c.cedula_Ruc
	left outer join view_obtenerSuscripcion sp on sp.codCliente=c.codCliente and sp.codSucursal=c.codCiudad_Sucursal
    --ftorres

  where c.codCiudad_Sucursal=@i_codSucursal and c.codcliente=@i_codCliente
END
GO

