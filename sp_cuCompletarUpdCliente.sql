USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_cuCompletarUpdCliente]    Script Date: 15/6/2022 11:21:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER PROCEDURE [dbo].[sp_cuCompletarUpdCliente](
@i_cedula_ruc varchar(20),
@i_fecha_nacimiento datetime,			
@i_estado_civil varchar(1),			
@i_nombre_empresa varchar(100),
@i_nombre_comercial varchar(100),
@i_nombres_Representante varchar(100),
@i_apellidos_Representante varchar(100),
@i_cedula_Representante varchar(20),
@i_cargo varchar(100),				
@i_codUsuario int,
@i_nombreReferencial varchar(100),
@i_sector varchar(100),
@i_telefono1 varchar(10),
@i_extension1 varchar(6),
@i_tipoTlf1 varchar(1),
@i_marcaTlf1 smallint,
@i_telefono2 varchar(10),
@i_extension2 varchar(6),
@i_tipoTlf2 varchar(1),
@i_marcaTlf2 smallint,
@i_telefono3 varchar(10),
@i_extension3 varchar(6),
@i_tipoTlf3 varchar(1),
@i_marcaTlf3 smallint,
@i_telefono4 varchar(10),
@i_extension4 varchar(6),
@i_tipoTlf4 varchar(1),
@i_marcaTlf4 smallint,
@i_telefono5 varchar(10),
@i_extension5 varchar(6),
@i_tipoTlf5 varchar(1),
@i_marcaTlf5 smallint,
@i_telefono6 varchar(10),
@i_extension6 varchar(6),
@i_tipoTlf6 varchar(1),
@i_marcaTlf6 smallint,
@i_verificaTlf smallint,
@i_codOperadora1 varchar(1),
@i_codOperadora2 varchar(1),
@i_codOperadora3 varchar(1),
@i_codOperadora4 varchar(1),
@i_codOperadora5 varchar(1),
@i_codOperadora6 varchar(1),
@i_nombresReferencia varchar(100),
@i_parentesco varchar(50),
@i_telefonos varchar(100),
@i_discapacitado int,
@i_codParroquia varchar(10),
@i_facturacionElectronica int,
@i_correoFacturacionElectronica varchar(255),
@i_correoFacturacionElectronica2 varchar(255),
@i_correoFacturacionElectronica3 varchar(255),
/*MOFICICACIONES REALIZADAS POR: JAPSON MOREIRA - 23/03/2022
AGREGAR CAMPOS PARA REALIZAR LA MODIFICACIÓN EN LA TABLA DIRECCION*/
@i_barrioSector varchar(100),
@i_callePrincipal varchar(100),
@i_numeracion varchar(25),
@i_calleSecundaria varchar(100),
@i_urbanizacion varchar(100),
@i_referencia varchar(100)


)
AS
BEGIN

declare @v_datos_actuales varchar(1023)
declare  @v_codCiudad_Sucursal int, @v_codCliente int, @v_nombres varchar(100), @v_apellidos varchar(100)

select @v_codCiudad_Sucursal=codCiudad_Sucursal, @v_codCliente=codCliente, @v_nombres=nombres, @v_apellidos=apellidos
from cliente where cedula_Ruc=@i_cedula_ruc

declare  @v_resultadoModelo varchar(50),@v_segmentacion varchar(50),@v_capacidadPago varchar(50),@v_rangoIngresos varchar(50),@v_formaPago varchar(50),@v_segmentacionOriginal varchar(50)
select @v_segmentacion=(select attrition from segmentacion where segmentacion=sc.segmentacion),
@v_resultadoModelo=resultadoModelo,@v_capacidadPago=capacidadPago,@v_rangoIngresos=rangoIngresos,@v_formaPago=formaPago,@v_segmentacionOriginal=segmentacionOriginal
from scoreCrediticio sc where numeroIdentificacion=@i_cedula_ruc
if(@i_cedula_Representante <> '')
begin 
	select @v_segmentacion=(select attrition from segmentacion where segmentacion=sc.segmentacion),
	@v_resultadoModelo=resultadoModelo,@v_capacidadPago=capacidadPago,@v_rangoIngresos=rangoIngresos,@v_formaPago=formaPago,@v_segmentacionOriginal=segmentacionOriginal
	from scoreCrediticio sc where numeroIdentificacion=@i_cedula_Representante
end 

if(len(@i_nombre_empresa)>4)
begin
	select @v_nombres = '';
	select @v_apellidos = '';
end

update Cliente set 
cedula_ruc = replace(ltrim(rtrim(isnull(@i_cedula_ruc,''))),' ','')  ,
sector = ltrim(rtrim(isnull(@i_sector,'')))  ,
fecha_nacimiento = @i_fecha_nacimiento ,			
estado_civil = isnull(@i_estado_civil,'') ,	
nombres = isnull(@v_nombres,''),
apellidos = isnull(@v_apellidos,''),
nombre_empresa = ltrim(rtrim(isnull(@i_nombre_empresa,'')))    ,
nombre_comercial = ltrim(rtrim(isnull(@i_nombre_comercial,'')))    ,
nombres_Representante = ltrim(rtrim(isnull(@i_nombres_Representante,'')))    ,
apellidos_Representante = ltrim(rtrim(isnull(@i_apellidos_Representante,'')))    ,
cedula_Representante = ltrim(rtrim(isnull(@i_cedula_Representante,'')))    ,
cargo = @i_cargo  ,
telefono1 = @i_telefono1 ,
extension1 = @i_extension1 ,
tipoTlf1 = @i_tipoTlf1 ,
marcaTlf1 = @i_marcaTlf1 ,
telefono2 = @i_telefono2 ,
extension2 = @i_extension2 ,
tipoTlf2 = @i_tipoTlf2 ,
marcaTlf2 = @i_marcaTlf2 ,
telefono3 = @i_telefono3 ,
extension3 = @i_extension3 ,
tipoTlf3 = @i_tipoTlf3 ,
marcaTlf3 = @i_marcaTlf3 ,
telefono4 = @i_telefono4 ,
extension4 = @i_extension4 ,
tipoTlf4 = @i_tipoTlf4 ,
marcaTlf4 = @i_marcaTlf4 ,
telefono5 = @i_telefono5 ,
extension5 = @i_extension5 ,
tipoTlf5 = @i_tipoTlf5 ,
marcaTlf5 = @i_marcaTlf5 ,
telefono6 = @i_telefono6 ,
extension6 = @i_extension6 ,
tipoTlf6 = @i_tipoTlf6 ,
marcaTlf6 = @i_marcaTlf6,
verificaTlf = @i_verificaTlf,
codOperadora1 = @i_codOperadora1 ,
codOperadora2 = @i_codOperadora2,
codOperadora3 = @i_codOperadora3,
codOperadora4 = @i_codOperadora4,
codOperadora5 = @i_codOperadora5,
codOperadora6 = @i_codOperadora6,
nombresReferencia=@i_nombresReferencia,
parentescoReferencia=@i_parentesco,
telefonosReferencia=@i_telefonos,
discapacitado=@i_discapacitado,
codParroquia=@i_codParroquia,
facturacionElectronica=@i_facturacionElectronica,
correoFacturacionElectronica=lower(replace(isnull(@i_correoFacturacionElectronica,''),' ','')),
correoFacturacionElectronica2=lower(replace(isnull(@i_correoFacturacionElectronica2,''),' ','')),
correoFacturacionElectronica3=lower(replace(isnull(@i_correoFacturacionElectronica3,''),' ','')),
resultadoModelo=@v_resultadoModelo,
segmentacion=@v_segmentacion,
capacidadPago=@v_capacidadPago,
rangoIngresos=@v_rangoIngresos,
formaPago=@v_formaPago,
segmentacionOriginal=@v_segmentacionOriginal,
esPrincipal=1
where codCiudad_Sucursal= @v_codCiudad_Sucursal and  codcliente = @v_codCliente

/*UPDATE A LA TABLA DE DIRECCION */
UPDATE DIRECCION SET
barrioSector = @i_barrioSector,
callePrincipal = @i_callePrincipal,
numeracion = @i_numeracion,
calleSecundaria	= @i_calleSecundaria,
Urbanizacion = @i_urbanizacion,
referencia = @i_referencia,
fActualizacion = getDate(),
fechaVencimiento = (SELECT DATEADD(YEAR,1,GETDATE())),
informacionActualizada = 'S'
where codCiudadSucursal= @v_codCiudad_Sucursal and  codcliente = @v_codCliente




select @v_datos_actuales = ''
if (@i_sector  is not null and @i_sector <> @i_sector )
 select @v_datos_actuales = @v_datos_actuales + 'sector' + ltrim(rtrim(@i_sector)) + CHAR(9) 
if (@i_fecha_nacimiento is not null and @i_fecha_nacimiento <> @i_fecha_nacimiento ) 
 select @v_datos_actuales = @v_datos_actuales + 'fechaNacimiento:' + ltrim(rtrim(convert(varchar(8),@i_fecha_nacimiento,112))) + CHAR(9) 
if (@i_estado_civil is not null and @i_estado_civil <> @i_estado_civil )
 select @v_datos_actuales = @v_datos_actuales + 'estadoCivil:' + ltrim(rtrim(@i_estado_civil)) + CHAR(9) 
if (@i_nombre_empresa  is not null and @i_nombre_empresa <> @i_nombre_empresa )
 select @v_datos_actuales = @v_datos_actuales + 'nombre_empresa:' + ltrim(rtrim(@i_nombre_empresa)) + CHAR(9) 
if (@i_nombre_comercial  is not null and @i_nombre_comercial <> @i_nombre_comercial )
 select @v_datos_actuales = @v_datos_actuales + 'nombre_comercial:' + ltrim(rtrim(@i_nombre_comercial))+ CHAR(9)
if (@i_nombres_Representante  is not null and @i_nombres_Representante <> @i_nombres_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'nombres_Representante:' + ltrim(rtrim(@i_nombres_Representante)) + CHAR(9) 
if (@i_apellidos_Representante  is not null and @i_apellidos_Representante <> @i_apellidos_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'apellidos_Representante:' + ltrim(rtrim(@i_apellidos_Representante)) + CHAR(9) 
if (@i_cedula_Representante  is not null and @i_cedula_Representante <> @i_cedula_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'cedula_Representante:' + ltrim(rtrim(@i_cedula_Representante)) + CHAR(9) 
if (@i_cargo  is not null and @i_cargo <> @i_cargo )
 select @v_datos_actuales = @v_datos_actuales + 'cargo:' + ltrim(rtrim(@i_cargo)) + CHAR(9)

 select @v_datos_actuales = @v_datos_actuales + 'tlf1:' + ltrim(rtrim(@i_telefono1))
 select @v_datos_actuales = @v_datos_actuales + 'ext1:' + ltrim(rtrim(@i_extension1))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf1:' + ltrim(rtrim(@i_tipoTlf1))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe1:' + ltrim(rtrim(@i_codOperadora1))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf1:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf1)))

 select @v_datos_actuales = @v_datos_actuales + 'tlf2:' + ltrim(rtrim(@i_telefono2))
 select @v_datos_actuales = @v_datos_actuales + 'ext2:' + ltrim(rtrim(@i_extension2))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf2:' + ltrim(rtrim(@i_tipoTlf2))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe2:' + ltrim(rtrim(@i_codOperadora2))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf2:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf2)))

 select @v_datos_actuales = @v_datos_actuales + 'tlf3:' + ltrim(rtrim(@i_telefono3))
 select @v_datos_actuales = @v_datos_actuales + 'ext3:' + ltrim(rtrim(@i_extension3))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf3:' + ltrim(rtrim(@i_tipoTlf3))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe3:' + ltrim(rtrim(@i_codOperadora3))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf3:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf3)))

 select @v_datos_actuales = @v_datos_actuales + 'tlf4:' + ltrim(rtrim(@i_telefono4))
 select @v_datos_actuales = @v_datos_actuales + 'ext4:' + ltrim(rtrim(@i_extension4))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf4:' + ltrim(rtrim(@i_tipoTlf4))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe4:' + ltrim(rtrim(@i_codOperadora4))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf4:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf4)))

 select @v_datos_actuales = @v_datos_actuales + 'tlf5:' + ltrim(rtrim(@i_telefono5))
 select @v_datos_actuales = @v_datos_actuales + 'ext5:' + ltrim(rtrim(@i_extension5))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf5:' + ltrim(rtrim(@i_tipoTlf5))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe5:' + ltrim(rtrim(@i_codOperadora5))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf5:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf5)))

 select @v_datos_actuales = @v_datos_actuales + 'tlf6:' + ltrim(rtrim(@i_telefono6))
 select @v_datos_actuales = @v_datos_actuales + 'ext6:' + ltrim(rtrim(@i_extension6))
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf6:' + ltrim(rtrim(@i_tipoTlf6))
 select @v_datos_actuales = @v_datos_actuales + 'codOpe6:' + ltrim(rtrim(@i_codOperadora6))
 select @v_datos_actuales = @v_datos_actuales + 'marTlf6:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf6)))

 select @v_datos_actuales = @v_datos_actuales + 'Nombres Referencia:' + ltrim(rtrim(@i_nombresReferencia)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'Parentesco:' + ltrim(rtrim(@i_parentesco)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'TlfRef:' + ltrim(rtrim(@i_telefonos)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'codParroquia:' + ltrim(rtrim(@i_codParroquia)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_facturacionElectronica)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica)) + CHAR(9) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica2)) + CHAR(9)  
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica3)) + CHAR(9) 

if (@v_datos_actuales <> '')
	insert into log_general_transaccion (
		nombre_Tabla, codSucursal, codigo, datos_Actuales, fecha_Registro, codUsuario, codProceso)
		values('cliente', @v_codCiudad_Sucursal, @v_codCliente,@v_datos_Actuales,getdate(),36,'M')
		
END



GO

