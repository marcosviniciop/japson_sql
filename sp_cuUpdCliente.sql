USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_cuUpdCliente]    Script Date: 15/6/2022 11:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








ALTER                       PROCEDURE [dbo].[sp_cuUpdCliente](
@i_codCliente int,
@i_codEmpresa int,
@i_codCiudad_Sucursal int ,
@i_codCiudad_Cliente int ,
@i_codTipo_Cliente int ,
@i_cedula_ruc varchar(20),
@i_esEmpresa int ,
@i_nombres varchar(100),
@i_apellidos varchar(100),
@i_direccion varchar(255),
@i_sector varchar(100),
@i_lugar_trabajo varchar(100),
@i_dir_trabajo varchar(100),			
@i_sexo varchar(1),
@i_fecha_nacimiento datetime,			
@i_estado_civil varchar(1),			
@i_numero_hijos_dependientes int,			
@i_nombre_empresa varchar(100),
@i_nombre_comercial varchar(100),
@i_nombres_Representante varchar(100),
@i_apellidos_Representante varchar(100),
@i_cedula_Representante varchar(20),
@i_cargo varchar(100),						
@i_codTipo_Actividad int ,
@i_codUltimo_ISP int ,
@i_estado varchar(1),
@i_covinco varchar(1),
@i_verificado varchar(1),
@i_observacion varchar(100),
@i_verifica varchar(100),
@i_zona varchar(100),
@i_esPrincipal int,
@i_codCourier int,
@i_codUsuario int,
@i_conIva int,
@i_email varchar(255),
@i_sujetoRetencion int,
@i_contribuyenteEspecial int,
@i_exportador int,
@i_nombreReferencial varchar(100),
@i_telefono1 varchar(10),
@i_extension1 varchar(6),
@i_tipoTlf1 varchar(1),
@i_marcaTlf1 smallint,
@i_telefono2 varchar(10),
@i_extension2 varchar(6),
@i_tipoTlf2 varchar(1),
@i_marcaTlf2 smallint,
@i_telefono3 varchar(10),
@i_extension3 varchar(6),
@i_tipoTlf3 varchar(1),
@i_marcaTlf3 smallint,
@i_telefono4 varchar(10),
@i_extension4 varchar(6),
@i_tipoTlf4 varchar(1),
@i_marcaTlf4 smallint,
@i_telefono5 varchar(10),
@i_extension5 varchar(6),
@i_tipoTlf5 varchar(1),
@i_marcaTlf5 smallint,
@i_telefono6 varchar(10),
@i_extension6 varchar(6),
@i_tipoTlf6 varchar(1),
@i_marcaTlf6 smallint,
@i_verificaTlf smallint,
@i_codOperadora1 varchar(1),
@i_codOperadora2 varchar(1),
@i_codOperadora3 varchar(1),
@i_codOperadora4 varchar(1),
@i_codOperadora5 varchar(1),
@i_codOperadora6 varchar(1),
@i_codCalificacion varchar(3),
@i_diaPago int,
@i_nombresReferencia varchar(100),
@i_parentescoReferencia varchar(50),
@i_telefonosReferencia varchar(100),
@i_fechaEntregaTarjeta datetime,
@i_recibeTarjeta varchar(100),
@i_baseActa varchar(100),
@i_seguro int,
@i_devolucionSeguro int,
@i_discapacitado int,
@i_codProvincia varchar(10),
@i_codParroquia varchar(10),
@i_facturacionElectronica int,
@i_correoFacturacionElectronica varchar(255),
@i_correoFacturacionElectronica2 varchar(255),
@i_correoFacturacionElectronica3 varchar(255),
@i_codGrupoComercial int,
@i_esCanalVentaPromocional int,
@i_codCanal int,
@i_codCiudad_Sucursal_Referido int,
@i_codCliente_Referido int,
@i_resultadoModelo varchar(30),
@i_segmentacion varchar(30),
@i_capacidadPago money,
@i_rangoIngresos varchar(30),
@i_formaPago varchar(50),
@i_esActualizacion int--dceli para actualizacion cliente
,@i_enviadoTarjeta int
,@i_confirmacionTelefonica int
,@i_calificacionAsesor int
,@i_fecha_actualizacion_discapacitado datetime,
/*MOFICICACIONES REALIZADAS POR: JAPSON MOREIRA - 23/03/2022
AGREGAR CAMPOS PARA REALIZAR LA MODIFICACIÓN EN LA TABLA DIRECCION*/
@i_barrioSector varchar(100),
@i_callePrincipal varchar(100),
@i_numeracion varchar(25),
@i_calleSecundaria varchar(100),
@i_urbanizacion varchar(100),
@i_referencia varchar(100)

)
AS
BEGIN

declare
@v_fecha_verifica datetime,
@v_codCliente int, 
@v_codEmpresa int,
@v_codCiudad_Sucursal int ,
@v_codCiudad_Cliente int ,
@v_codTipo_Cliente int ,
@v_cedula_ruc varchar(20),
@v_esEmpresa int ,
@v_nombres varchar(100),
@v_apellidos varchar(100),
@v_direccion varchar(255),
@v_sector varchar(100),
@v_lugar_trabajo varchar(100),
@v_dir_trabajo varchar(100),			
@v_sexo varchar(1),
@v_fecha_nacimiento datetime,			
@v_estado_civil varchar(1),			
@v_numero_hijos_dependientes int,			
@v_nombre_empresa varchar(100),
@v_nombre_comercial varchar(100),
@v_nombres_Representante varchar(100),
@v_apellidos_Representante varchar(100),
@v_cedula_Representante varchar(20),
@v_cargo varchar(100),						
@v_codTipo_Actividad int ,
@v_codUltimo_ISP int ,
@v_estado varchar(1),
@v_covinco varchar(1),
@v_verificado varchar(1),
@v_observacion varchar(100),
@v_verifica varchar(100),
@v_zona varchar(100),
@v_esPrincipal int,
@v_codCourier int,
@v_codUsuario int,
@v_cambia_fecha_verificacion smallint,
@v_conIva int,
@v_email varchar(255),
@v_sujetoRetencion int,
@v_contribuyenteEspecial int,
@v_exportador int,
@v_nombreReferencial varchar(100),
@v_telefono1 varchar(10),
@v_extension1 varchar(6),
@v_tipoTlf1 varchar(1),
@v_marcaTlf1 smallint,
@v_telefono2 varchar(10),
@v_extension2 varchar(6),
@v_tipoTlf2 varchar(1),
@v_marcaTlf2 smallint,
@v_telefono3 varchar(10),
@v_extension3 varchar(6),
@v_tipoTlf3 varchar(1),
@v_marcaTlf3 smallint,
@v_telefono4 varchar(10),
@v_extension4 varchar(6),
@v_tipoTlf4 varchar(1),
@v_marcaTlf4 smallint,
@v_telefono5 varchar(10),
@v_extension5 varchar(6),
@v_tipoTlf5 varchar(1),
@v_marcaTlf5 smallint,
@v_telefono6 varchar(10),
@v_extension6 varchar(6),
@v_tipoTlf6 varchar(1),
@v_marcaTlf6 smallint,
@v_verificaTlf smallint,
@v_codUsuarioVerifica int,
@v_fechaVerificaTlf datetime,
@v_codOperadora1 varchar(1),
@v_codOperadora2 varchar(1),
@v_codOperadora3 varchar(1),
@v_codOperadora4 varchar(1),
@v_codOperadora5 varchar(1),
@v_codOperadora6 varchar(1),
@v_codCalificacion varchar(3),
@v_diaPago int,
@v_nombresReferencia varchar(100),
@v_parentescoReferencia varchar(50),
@v_telefonosReferencia varchar(100),
@v_fechaEntregaTarjeta datetime,
@v_recibeTarjeta varchar(100),
@v_baseActa varchar(100),
@v_seguro int,
@v_devolucionSeguro int,
@v_discapacitado int,
@v_fechaSeguro datetime,
@v_codProvincia varchar(10),
@v_codParroquia varchar(10),
@v_facturacionElectronica int,
@v_correoFacturacionElectronica varchar(255),
@v_correoFacturacionElectronica2 varchar(255),
@v_correoFacturacionElectronica3 varchar(255),
@v_codGrupoComercial int,
@v_esCanalVentaPromocional int, 
@v_codCanal int,
@v_codCiudad_Sucursal_Referido int,
@v_codCliente_Referido int,
@v_resultadoModelo varchar(30),
@v_segmentacion varchar(30),
@v_capacidadPago money,
@v_rangoIngresos varchar(30),
@v_formaPago varchar(50),
@v_enviadoTarjeta int,
@v_fechaActualizacion datetime,
@v_codUsuario_actualizacion int,
@v_confirmacionTelefonica int,
@v_calificacionAsesor int,
@v_fecha_actualizacion_discapacitado datetime,
@v_tieneDireccion int


if @i_nombre_empresa is not null
begin 
	select @i_nombre_empresa = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombre_empresa,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 
if @i_nombre_comercial is not null
begin 
	select @i_nombre_comercial = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombre_comercial,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 

if @i_nombres is not null
begin 
	select @i_nombres = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombres,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 
if @i_apellidos is not null
begin 
	select @i_apellidos = replace(replace(replace(replace(replace(replace(replace(replace(@i_apellidos,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 


/*Para campaña actualizacion cliente*/
if (@i_esActualizacion =1 or @i_verificaTlf = 1 )
begin
	select @v_fechaActualizacion = GETDATE(), @v_codUsuario_actualizacion = @i_codUsuario
end

/*fin*/

declare @aux_codUsuarioVerifica int, 
		@aux_fechaVerificaTlf datetime 


declare @v_datos_actuales varchar(1023)

if not exists(select codcliente from cliente 
	where codCiudad_Sucursal = @i_codCiudad_Sucursal and codcliente = @i_codCliente)
begin;
		--raiserror 60001 'No existe el Cliente solicitado'
		throw 60001, 'No existe el Cliente solicitado',1
		return
end


if len(ltrim(rtrim(isnull(@i_email,''))))<12
	select @i_email = ''
else
	select @i_email = ltrim(rtrim(isnull(@i_email,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica,''))))<12
	select @i_correoFacturacionElectronica = ''
else
	select @i_correoFacturacionElectronica = ltrim(rtrim(isnull(@i_correoFacturacionElectronica,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica2,''))))<12
	select @i_correoFacturacionElectronica2 = ''
else
	select @i_correoFacturacionElectronica2 = ltrim(rtrim(isnull(@i_correoFacturacionElectronica2,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica3,''))))<12
	select @i_correoFacturacionElectronica3 = ''
else
	select @i_correoFacturacionElectronica3 = ltrim(rtrim(isnull(@i_correoFacturacionElectronica3,'')))

if (isnull(@i_email,'') <>''	and 
	not (
	ascii(substring(isnull(@i_email,''),1,1))>=65 and ascii(substring(isnull(@i_email,''),1,1))<=90 or 
	ascii(substring(isnull(@i_email,''),1,1))>=97 and ascii(substring(isnull(@i_email,''),1,1))<=122 or
	ascii(substring(isnull(@i_email,''),1,1))>=48 and ascii(substring(isnull(@i_email,''),1,1))<=57 
	))
	select @i_email = SUBSTRING(@i_email, 2,LEN(@i_email))	
if (isnull(@i_correoFacturacionElectronica,'') <>''
	and 
	not (
	ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=90 or 
	ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=122 or
	ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=57 
	))
	select @i_correoFacturacionElectronica = SUBSTRING(@i_correoFacturacionElectronica, 2,LEN(@i_correoFacturacionElectronica))
if (isnull(@i_correoFacturacionElectronica2,'') <>''
	and 
	not (
	ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=90 or 
	ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=122 or
	ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=57 
	))
	select @i_correoFacturacionElectronica2 = SUBSTRING(@i_correoFacturacionElectronica2, 2,LEN(@i_correoFacturacionElectronica2))

if (isnull(@i_correoFacturacionElectronica3,'') <>''
	and 
	not (
	ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=90 or 
	ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=122 or
	ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=57 
	))
	select @i_correoFacturacionElectronica3 = SUBSTRING(@i_correoFacturacionElectronica3, 2,LEN(@i_correoFacturacionElectronica3))
	


select 
@v_codEmpresa = codEmpresa ,
@v_codCiudad_Sucursal = codCiudad_Sucursal,
@v_codCiudad_Cliente = codCiudad_Cliente,
@v_codTipo_Cliente = codTipo_Cliente ,
@v_cedula_ruc = cedula_ruc ,
@v_esEmpresa = esEmpresa ,
@v_nombres = nombres ,
@v_apellidos = apellidos ,
@v_direccion = direccion ,
@v_sector = sector ,
@v_lugar_trabajo = lugar_trabajo ,
@v_dir_trabajo = dir_trabajo ,
@v_sexo = sexo,
@v_fecha_nacimiento = fecha_nacimiento,			
@v_estado_civil = estado_civil,			
@v_numero_hijos_dependientes = numero_hijos_dependientes,			
@v_nombre_empresa = nombre_empresa ,
@v_nombre_comercial = nombre_comercial ,
@v_nombres_Representante = nombres_Representante ,
@v_apellidos_Representante = apellidos_Representante ,
@v_cedula_Representante = cedula_Representante ,
@v_cargo = cargo ,
@v_codTipo_Actividad = codTipo_Actividad ,
@v_codUltimo_ISP = codUltimo_ISP,
@v_estado = estado ,
@v_covinco = covinco ,
@v_verificado = verificado ,
@v_observacion = observacion ,
@v_verifica = verifica ,
@v_zona=zona,
@v_esPrincipal = esPrincipal ,
@v_codCourier = codCourier ,
@v_fecha_verifica = fecha_verifica,
@v_conIva=coniva,
@v_email=email,
@v_sujetoRetencion=sujetoRetencion,
@v_contribuyenteEspecial = esContribuyenteEspecial,
@v_exportador = esExportador,
@v_nombreReferencial=nombreReferencial,
@v_telefono1 = telefono1 ,
@v_extension1 = extension1 ,
@v_tipoTlf1 = tipoTlf1 ,
@v_marcaTlf1 = marcaTlf1 ,
@v_telefono2 = telefono2 ,
@v_extension2 = extension2 ,
@v_tipoTlf2 = tipoTlf2 ,
@v_marcaTlf2 = marcaTlf2 ,
@v_telefono3 = telefono3 ,
@v_extension3 = extension3 ,
@v_tipoTlf3 = tipoTlf3 ,
@v_marcaTlf3 = marcaTlf3 ,
@v_telefono4 = telefono4 ,
@v_extension4 = extension4 ,
@v_tipoTlf4 = tipoTlf4 ,
@v_marcaTlf4 = marcaTlf4 ,
@v_telefono5 = telefono5 ,
@v_extension5 = extension5 ,
@v_tipoTlf5 = tipoTlf5 ,
@v_marcaTlf5 = marcaTlf5 ,
@v_telefono6 = telefono6 ,
@v_extension6 = extension6 ,
@v_tipoTlf6 = tipoTlf6 ,
@v_marcaTlf6 = marcaTlf6,
@v_verificaTlf = verificaTlf,
@v_codUsuarioVerifica = codUsuarioVerifica,
@v_fechaVerificaTlf = fechaVerificaTlf,
@v_codOperadora1 = codOperadora1 ,
@v_codOperadora2 = codOperadora2,
@v_codOperadora3 = codOperadora3,
@v_codOperadora4 = codOperadora4,
@v_codOperadora5 = codOperadora5,
@v_codOperadora6 = codOperadora6,
@v_codCalificacion=calificacion,
@v_diaPago=diaPago,
@v_nombresReferencia=nombresReferencia,
@v_parentescoReferencia =parentescoReferencia,
@v_telefonosReferencia=telefonosReferencia,
@v_fechaEntregaTarjeta=fechaEntregaTarjeta,
@v_recibeTarjeta =recibeTarjeta,
@v_baseActa=baseActa,
@v_seguro=seguro,
@v_fechaSeguro = fechaSeguro,
@v_devolucionSeguro=devolucionSeguro,
@v_discapacitado=discapacitado,
@v_codProvincia=codProvincia,
@v_codParroquia=codProvincia,
@v_facturacionElectronica=facturacionElectronica,
@v_correoFacturacionElectronica=correoFacturacionElectronica,
@v_correoFacturacionElectronica2=correoFacturacionElectronica2,
@v_correoFacturacionElectronica3=correoFacturacionElectronica3,
@v_codGrupoComercial=codGrupoComercial,
@v_esCanalVentaPromocional = esCanalVentaPromocional, 
@v_codCanal= codCanal,
@v_codCiudad_Sucursal_Referido = codCiudad_Sucursal_Referido,
@v_codCliente_Referido = codCliente_Referido,
@v_resultadoModelo = resultadoModelo,
@v_segmentacion = segmentacion,
@v_capacidadPago = capacidadPago,
@v_rangoIngresos = rangoIngresos,
@v_formaPago = formaPago,
@v_enviadoTarjeta = enviadoTarjeta,
@v_confirmacionTelefonica = confirmacionTelefonica,
@v_calificacionAsesor = calificacionAsesor,
@v_fecha_actualizacion_discapacitado = fecha_actualizacion_discapacitado
from cliente where codcliente = @i_codCliente

select @aux_codUsuarioVerifica = @v_codUsuarioVerifica, @aux_fechaVerificaTlf =@v_fechaVerificaTlf
if (@i_verificaTlf >0)
begin
	select @aux_codUsuarioVerifica = @i_codUsuario,
	@aux_fechaVerificaTlf = getdate()
end


select @v_cambia_fecha_verificacion = 0
if @v_verifica is not null
	if @v_verifica <> @i_verifica and @v_verifica <>'' 
		begin
			select @v_fecha_verifica = getdate()
			select @v_cambia_fecha_verificacion =1
		end 
		
if (@i_seguro<>@v_Seguro)
begin
	if (@i_seguro>0)
		select @v_fechaSeguro=getDate()
	else
		select @v_fechaSeguro=null
end		

select @i_cedula_ruc = replace(ltrim(rtrim(isnull(@i_cedula_ruc,''))),' ','')	
select @i_cedula_ruc = replace(replace(replace(replace(replace(replace(@i_cedula_ruc,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),'')

if @i_telefono1=''
	select @i_tipotlf1='', @i_extension1='', @i_codOperadora1='', @i_marcaTlf1=0
if @i_telefono2=''
	select @i_tipotlf2='', @i_extension2='', @i_codOperadora2='', @i_marcaTlf2=0
if @i_telefono3=''
	select @i_tipotlf3='', @i_extension3='', @i_codOperadora3='', @i_marcaTlf3=0
if @i_telefono4=''
	select @i_tipotlf4='', @i_extension4='', @i_codOperadora4='', @i_marcaTlf4=0
if @i_telefono5=''
	select @i_tipotlf5='', @i_extension5='', @i_codOperadora5='', @i_marcaTlf5=0
--baguirre fecha actualizacion discapacitado
if @i_fecha_actualizacion_discapacitado=''
	select @i_fecha_actualizacion_discapacitado=NULL

print('ACTUALIZAR INFORMACIÓN DEL CLIENTE');

update Cliente set 
codEmpresa = @i_codEmpresa   ,
codCiudad_Cliente= @i_codCiudad_Cliente  ,
codTipo_Cliente = @i_codTipo_Cliente  ,
cedula_ruc = replace(ltrim(rtrim(isnull(@i_cedula_ruc,''))),' ','')  ,
esEmpresa = @i_esEmpresa  ,
nombres = ltrim(rtrim(isnull(@i_nombres,'')))  ,
apellidos = ltrim(rtrim(isnull(@i_apellidos,'')))  ,
direccion = ltrim(rtrim(isnull(@i_direccion,'')))  ,
sector = ltrim(rtrim(isnull(@i_sector,'')))  ,
lugar_trabajo = ltrim(rtrim(isnull(@i_lugar_trabajo,'')))  ,
dir_trabajo = ltrim(rtrim(isnull(@i_dir_trabajo,'')))  ,
sexo = isnull(@i_sexo,'') ,
fecha_nacimiento = @i_fecha_nacimiento ,			
estado_civil = isnull(@i_estado_civil,'') ,			
numero_hijos_dependientes = @i_numero_hijos_dependientes ,			
nombre_empresa = ltrim(rtrim(isnull(@i_nombre_empresa,'')))    ,
nombre_comercial = ltrim(rtrim(isnull(@i_nombre_comercial,'')))    ,
nombres_Representante = ltrim(rtrim(isnull(@i_nombres_Representante,'')))    ,
apellidos_Representante = ltrim(rtrim(isnull(@i_apellidos_Representante,'')))    ,
cedula_Representante = ltrim(rtrim(isnull(@i_cedula_Representante,'')))    ,
cargo = @i_cargo  ,
codTipo_Actividad = @i_codTipo_Actividad  ,
codUltimo_ISP = @i_codUltimo_ISP,
estado = @i_estado  ,
covinco = @i_covinco  ,
verificado = @i_verificado  ,
observacion = @i_observacion  ,
verifica = @i_verifica  ,
zona=@i_zona,
esPrincipal = @i_esPrincipal  ,
codCourier = @i_codCourier ,
fecha_verifica = @v_fecha_verifica,
coniva=@i_conIva,
email= lower(replace(isnull(@i_email,''),' ','')),
sujetoRetencion=@i_sujetoRetencion,
esContribuyenteEspecial=@i_contribuyenteEspecial,
esExportador=@i_exportador,
nombreReferencial=@i_nombreReferencial,
telefono1 = @i_telefono1 ,
extension1 = @i_extension1 ,
tipoTlf1 = @i_tipoTlf1 ,
marcaTlf1 = @i_marcaTlf1 ,
telefono2 = @i_telefono2 ,
extension2 = @i_extension2 ,
tipoTlf2 = @i_tipoTlf2 ,
marcaTlf2 = @i_marcaTlf2 ,
telefono3 = @i_telefono3 ,
extension3 = @i_extension3 ,
tipoTlf3 = @i_tipoTlf3 ,
marcaTlf3 = @i_marcaTlf3 ,
telefono4 = @i_telefono4 ,
extension4 = @i_extension4 ,
tipoTlf4 = @i_tipoTlf4 ,
marcaTlf4 = @i_marcaTlf4 ,
telefono5 = @i_telefono5 ,
extension5 = @i_extension5 ,
tipoTlf5 = @i_tipoTlf5 ,
marcaTlf5 = @i_marcaTlf5 ,
telefono6 = @i_telefono6 ,
extension6 = @i_extension6 ,
tipoTlf6 = @i_tipoTlf6 ,
marcaTlf6 = @i_marcaTlf6,
verificaTlf = @i_verificaTlf,
codUsuarioVerifica = @aux_codUsuarioVerifica,
fechaVerificaTlf = @aux_fechaVerificaTlf,
codOperadora1 = @i_codOperadora1 ,
codOperadora2 = @i_codOperadora2,
codOperadora3 = @i_codOperadora3,
codOperadora4 = @i_codOperadora4,
codOperadora5 = @i_codOperadora5,
codOperadora6 = @i_codOperadora6,
calificacion=@i_codCalificacion,
diaPago=@i_diaPago,
nombresReferencia=@i_nombresReferencia,
parentescoReferencia=@i_parentescoReferencia,
telefonosReferencia=@i_telefonosReferencia,
fechaEntregaTarjeta=@i_fechaEntregaTarjeta,
recibeTarjeta=@i_recibeTarjeta,
baseActa=@i_baseActa,
seguro=@i_seguro,
fechaSeguro = @v_fechaSeguro,
devolucionSeguro = @i_devolucionSeguro,
discapacitado = @i_discapacitado,
codProvincia=@i_codProvincia,
codParroquia=@i_codParroquia,
facturacionElectronica=@i_facturacionElectronica,
correoFacturacionElectronica=lower(replace(isnull(@i_correoFacturacionElectronica,''),' ','')),
correoFacturacionElectronica2=lower(replace(isnull(@i_correoFacturacionElectronica2,''),' ','')),
correoFacturacionElectronica3=lower(replace(isnull(@i_correoFacturacionElectronica3,''),' ','')),
codGrupoComercial = @i_codGrupoComercial,
esCanalVentaPromocional = @i_esCanalVentaPromocional,
codCanal = @i_codCanal,
codCiudad_Sucursal_Referido = @i_codCiudad_Sucursal_Referido,
codCliente_Referido = @i_codCliente_Referido,
resultadoModelo = @i_resultadoModelo,
segmentacion = @i_segmentacion,
capacidadPago = @i_capacidadPago,
rangoIngresos = @i_rangoIngresos,
formaPago = @i_formaPago,
fecha_actualizacion = @v_fechaActualizacion,
enviadoTarjeta = @i_enviadoTarjeta, 
codUsuario_actualizacion = @v_codUsuario_actualizacion,
confirmacionTelefonica = @i_confirmacionTelefonica,
calificacionAsesor = @i_calificacionAsesor,
fecha_actualizacion_discapacitado = @i_fecha_actualizacion_discapacitado
where codCiudad_Sucursal= @i_codCiudad_Sucursal and  codcliente = @i_codCliente
print('ACTUALIZAR INFORMACIÓN DEL CLIENTE FINALIZADO');
/*UPDATE A LA TABLA DE DIRECCION*/

select @v_tieneDireccion = count(*) from direccion where codCliente = @i_codCliente and codCiudadSucursal = @i_codCiudad_Sucursal;

if(@v_tieneDireccion >= 1)
begin
	print('ACTUALIZAR INFORMACIÓN DE DIRECCION');
	UPDATE DIRECCION SET
	barrioSector =@i_barrioSector,
	callePrincipal = @i_callePrincipal,
	numeracion = @i_numeracion,
	calleSecundaria	= @i_calleSecundaria,
	Urbanizacion = @i_urbanizacion,
	referencia = @i_referencia,
	fActualizacion = getDate(),
	fechaVencimiento = (SELECT DATEADD(YEAR,1,GETDATE())),
	informacionActualizada = 'S'
	where codCiudadSucursal= @i_codCiudad_Sucursal and  codcliente = @i_codCliente
end
else
begin
INSERT INTO [dbo].[direccion]
           ([codCiudadSucursal]
           ,[codCliente]
           ,[barrioSector]
           ,[callePrincipal]
           ,[numeracion]
           ,[calleSecundaria]
           ,[Urbanizacion]
           ,[referencia]
           ,[fActualizacion]
           ,[fechaVencimiento]
           ,[informacionActualizada])
     VALUES
           (@i_codCiudad_Sucursal,@i_codCliente,@i_barrioSector
           ,@i_callePrincipal
           ,@i_numeracion
           ,@i_calleSecundaria
           ,@i_urbanizacion
           ,@i_referencia
           ,GETDATE()
           ,GETDATE()
           ,'N')
end

print('ACTUALIZAR INFORMACIÓN DE DIRECCION FINALIZADO');



select @v_datos_actuales = ''
if (@i_codEmpresa  is not null and @i_codEmpresa <> @v_codEmpresa ) 
 select @v_datos_actuales = @v_datos_actuales + 'codEmpresa:' + ltrim(rtrim(convert(varchar(20),@i_codEmpresa))) + CHAR(9) 
if (@i_codCiudad_Sucursal  is not null and @i_codCiudad_Sucursal<> @v_codCiudad_Sucursal )
 select @v_datos_actuales = @v_datos_actuales + 'codCiudadSucursal:' + ltrim(rtrim(convert(varchar(20),@i_codCiudad_Sucursal))) + CHAR(9)
if (@i_codCiudad_Cliente  is not null and @i_codCiudad_Cliente<> @v_codCiudad_Cliente )
 select @v_datos_actuales = @v_datos_actuales + 'codCiudadCliente:' + ltrim(rtrim(convert(varchar(20),@i_codCiudad_Cliente)))+ CHAR(9)
if (@i_codTipo_Cliente  is not null and @i_codTipo_Cliente <> @v_codTipo_Cliente )
 select @v_datos_actuales = @v_datos_actuales + 'codTipoCliente:' + ltrim(rtrim(convert(varchar(20),@i_codTipo_Cliente)))+ CHAR(9)
if (@i_cedula_ruc  is not null and @i_cedula_ruc <> @v_cedula_ruc )
 select @v_datos_actuales = @v_datos_actuales + 'cedulaRuc:' + ltrim(rtrim(@i_cedula_ruc)) +  CHAR(9) 
if (@i_esEmpresa  is not null and @i_esEmpresa <> @v_esEmpresa )
 select @v_datos_actuales = @v_datos_actuales + 'esEmpresa:' + ltrim(rtrim(convert(varchar(20),@i_esEmpresa)))+ CHAR(9)
if (@i_nombres  is not null and @i_nombres <> @v_nombres )
 select @v_datos_actuales = @v_datos_actuales + 'nombres:' + ltrim(rtrim(@i_nombres)) + CHAR(9) 
if (@i_apellidos  is not null and @i_apellidos <> @v_apellidos )
 select @v_datos_actuales = @v_datos_actuales + 'apellidos:' + ltrim(rtrim(@i_apellidos)) + CHAR(9) 
if (@i_direccion  is not null and @i_direccion <> @v_direccion )
 select @v_datos_actuales = @v_datos_actuales + 'direccion:' + ltrim(rtrim(@i_direccion)) + CHAR(9) 
if (@i_sector  is not null and @i_sector <> @v_sector )
 select @v_datos_actuales = @v_datos_actuales + 'sector' + ltrim(rtrim(@i_sector)) + CHAR(9) 
if (@i_lugar_trabajo  is not null and @i_lugar_trabajo <> @v_lugar_trabajo )
 select @v_datos_actuales = @v_datos_actuales + 'lugarTrabajo:' + ltrim(rtrim(@i_lugar_trabajo)) + CHAR(9) 
if (@i_dir_trabajo  is not null and @i_dir_trabajo <> @v_dir_trabajo )
 select @v_datos_actuales = @v_datos_actuales + 'dirTrabajo:' + ltrim(rtrim(@i_dir_trabajo)) + CHAR(9) 
if (@i_sexo is not null and @i_sexo <> @v_sexo )
 select @v_datos_actuales = @v_datos_actuales + 'sexo:' + ltrim(rtrim(@i_sexo)) + CHAR(9) 
if (@i_fecha_nacimiento is not null and @i_fecha_nacimiento <> @v_fecha_nacimiento ) 
 select @v_datos_actuales = @v_datos_actuales + 'fechaNacimiento:' + ltrim(rtrim(convert(varchar(8),@i_fecha_nacimiento,112))) + CHAR(9) 
if (@i_estado_civil is not null and @i_estado_civil <> @v_estado_civil )
 select @v_datos_actuales = @v_datos_actuales + 'estadoCivil:' + ltrim(rtrim(@i_estado_civil)) + CHAR(9) 
if (@i_numero_hijos_dependientes is not null and @i_numero_hijos_dependientes <> @v_numero_hijos_dependientes ) 
 select @v_datos_actuales = @v_datos_actuales + 'numHijosDep:' + ltrim(rtrim(convert(varchar(20),@i_numero_hijos_dependientes))) + CHAR(9) 
if (@i_nombre_empresa  is not null and @i_nombre_empresa <> @v_nombre_empresa )
 select @v_datos_actuales = @v_datos_actuales + 'nombre_empresa:' + ltrim(rtrim(@i_nombre_empresa)) + CHAR(9) 
if (@i_nombre_comercial  is not null and @i_nombre_comercial <> @v_nombre_comercial )
 select @v_datos_actuales = @v_datos_actuales + 'nombre_comercial:' + ltrim(rtrim(@i_nombre_comercial))+ CHAR(9)
if (@i_nombres_Representante  is not null and @i_nombres_Representante <> @v_nombres_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'nombres_Representante:' + ltrim(rtrim(@i_nombres_Representante)) + CHAR(9) 
if (@i_apellidos_Representante  is not null and @i_apellidos_Representante <> @v_apellidos_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'apellidos_Representante:' + ltrim(rtrim(@i_apellidos_Representante)) + CHAR(9) 
if (@i_cedula_Representante  is not null and @i_cedula_Representante <> @v_cedula_Representante )
 select @v_datos_actuales = @v_datos_actuales + 'cedula_Representante:' + ltrim(rtrim(@i_cedula_Representante)) + CHAR(9) 
if (@i_cargo  is not null and @i_cargo <> @v_cargo )
 select @v_datos_actuales = @v_datos_actuales + 'cargo:' + ltrim(rtrim(@i_cargo)) + CHAR(9)
if (@i_codTipo_Actividad  is not null and @i_codTipo_Actividad <> @v_codTipo_Actividad )
 select @v_datos_actuales = @v_datos_actuales + 'codTipo_Actividad:' + ltrim(rtrim(convert(varchar(20),@i_codTipo_Actividad))) + CHAR(9)
if (@i_codUltimo_ISP is not null and @i_codUltimo_ISP <> @v_codUltimo_ISP ) 
 select @v_datos_actuales = @v_datos_actuales + 'codUltimo_ISP:' + ltrim(rtrim(convert(varchar(20),@i_codUltimo_ISP))) + CHAR(9) 
if (@i_estado  is not null and @i_estado <> @v_estado )
 select @v_datos_actuales = @v_datos_actuales + 'estado:' + ltrim(rtrim(@i_estado)) + CHAR(9) 
if (@i_covinco  is not null and @i_covinco <> @v_covinco )
 select @v_datos_actuales = @v_datos_actuales + 'covinco:' + ltrim(rtrim(@i_covinco)) + CHAR(9) 
if (@i_verificado  is not null and @i_verificado <> @v_verificado )
 select @v_datos_actuales = @v_datos_actuales + 'verificado:' + ltrim(rtrim(@i_verificado)) +  CHAR(9)
if (@i_observacion  is not null and @i_observacion <> @v_observacion )
 select @v_datos_actuales = @v_datos_actuales + 'observacion:' + ltrim(rtrim(@i_observacion))+ CHAR(9) 
if (@i_verifica  is not null and @i_verifica <> @v_verifica )
 select @v_datos_actuales = @v_datos_actuales + 'verifica:' + ltrim(rtrim(@i_verifica)) + CHAR(9)
if (@i_zona  is not null and @i_zona <> @v_zona )
 select @v_datos_actuales = @v_datos_actuales + 'zona:' + ltrim(rtrim(@i_zona))
if ( @i_esPrincipal is not null and @i_esPrincipal <> @v_esPrincipal )
 select @v_datos_actuales = @v_datos_actuales + 'esPrincipal:' + ltrim(rtrim(convert(varchar(20),@i_esPrincipal)))
if ( @i_codCourier is not null and @i_codCourier<> @v_codCourier )
 select @v_datos_actuales = @v_datos_actuales + 'codCourier:' + ltrim(rtrim(convert(varchar(20),@i_codCourier)))
if ( @i_conIva is not null and @i_conIva<> @v_conIva )
 select @v_datos_actuales = @v_datos_actuales + 'conIva:' + ltrim(rtrim(convert(varchar(2),@i_conIva)))
if (@V_cambia_fecha_verificacion =1)
 select @v_datos_actuales = @v_datos_actuales + 'fecha_verifica:' + ltrim(rtrim(convert(varchar(8),@v_fecha_verifica,112)))
if ( @i_email is not null and @i_email<> @v_email )
 select @v_datos_actuales = @v_datos_actuales + 'Email:' + ltrim(rtrim(@i_email))
if ( @i_sujetoRetencion is not null and @i_sujetoRetencion<> @v_sujetoRetencion )
 select @v_datos_actuales = @v_datos_actuales + 'Sujeto Retencion:' + ltrim(rtrim(@i_sujetoRetencion))
if ( @i_contribuyenteEspecial is not null and @i_contribuyenteEspecial<> @v_contribuyenteEspecial )
 select @v_datos_actuales = @v_datos_actuales + 'Contribuyente Especial:' + ltrim(rtrim(@i_contribuyenteEspecial))
if ( @i_exportador is not null and @i_exportador<> @v_exportador )
 select @v_datos_actuales = @v_datos_actuales + 'Exportador:' + ltrim(rtrim(@i_exportador))
if ( @i_nombreReferencial is not null and @i_nombreReferencial<> @v_nombreReferencial )
 select @v_datos_actuales = @v_datos_actuales + 'Nombre Referencial:' + ltrim(rtrim(@i_nombreReferencial))

if ( @i_telefono1 is not null and @i_telefono1<> @v_telefono1)
 select @v_datos_actuales = @v_datos_actuales + 'tlf1:' + ltrim(rtrim(@i_telefono1))
if ( @i_extension1 is not null and @i_extension1<> @v_extension1)
 select @v_datos_actuales = @v_datos_actuales + 'ext1:' + ltrim(rtrim(@i_extension1))
if ( @i_tipoTlf1 is not null and @i_tipoTlf1<> @v_tipoTlf1)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf1:' + ltrim(rtrim(@i_tipoTlf1))
if ( @i_codOperadora1 is not null and @i_codOperadora1<> @v_codOperadora1)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe1:' + ltrim(rtrim(@i_codOperadora1))
if ( @i_marcaTlf1 is not null and @i_marcaTlf1<> @v_marcaTlf1 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf1:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf1)))

if ( @i_telefono2 is not null and @i_telefono2<> @v_telefono2)
 select @v_datos_actuales = @v_datos_actuales + 'tlf2:' + ltrim(rtrim(@i_telefono2))
if ( @i_extension2 is not null and @i_extension2<> @v_extension2)
 select @v_datos_actuales = @v_datos_actuales + 'ext2:' + ltrim(rtrim(@i_extension2))
if ( @i_tipoTlf2 is not null and @i_tipoTlf2<> @v_tipoTlf2)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf2:' + ltrim(rtrim(@i_tipoTlf2))
if ( @i_codOperadora2 is not null and @i_codOperadora2<> @v_codOperadora2)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe2:' + ltrim(rtrim(@i_codOperadora2))
if ( @i_marcaTlf2 is not null and @i_marcaTlf2<> @v_marcaTlf2 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf2:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf2)))

if ( @i_telefono3 is not null and @i_telefono3<> @v_telefono3)
 select @v_datos_actuales = @v_datos_actuales + 'tlf3:' + ltrim(rtrim(@v_telefono3))
if ( @i_extension3 is not null and @i_extension3<> @v_extension3)
 select @v_datos_actuales = @v_datos_actuales + 'ext3:' + ltrim(rtrim(@v_extension3))
if ( @i_tipoTlf3 is not null and @i_tipoTlf3<> @v_tipoTlf3)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf3:' + ltrim(rtrim(@v_tipoTlf3))
if ( @i_codOperadora3 is not null and @i_codOperadora3<> @v_codOperadora3)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe3:' + ltrim(rtrim(@i_codOperadora3))
if ( @i_marcaTlf3 is not null and @i_marcaTlf3<> @v_marcaTlf3 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf3:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf3)))

if ( @i_telefono4 is not null and @i_telefono4<> @v_telefono4)
 select @v_datos_actuales = @v_datos_actuales + 'tlf4:' + ltrim(rtrim(@i_telefono4))
if ( @i_extension4 is not null and @i_extension4<> @v_extension4)
 select @v_datos_actuales = @v_datos_actuales + 'ext4:' + ltrim(rtrim(@i_extension4))
if ( @i_tipoTlf4 is not null and @i_tipoTlf4<> @v_tipoTlf4)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf4:' + ltrim(rtrim(@i_tipoTlf4))
if ( @i_codOperadora4 is not null and @i_codOperadora4<> @v_codOperadora4)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe4:' + ltrim(rtrim(@i_codOperadora4))
if ( @i_marcaTlf4 is not null and @i_marcaTlf4<> @v_marcaTlf4 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf4:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf4)))

if ( @i_telefono5 is not null and @i_telefono5<> @v_telefono5)
 select @v_datos_actuales = @v_datos_actuales + 'tlf5:' + ltrim(rtrim(@i_telefono5))
if ( @i_extension5 is not null and @i_extension5<> @v_extension5)
 select @v_datos_actuales = @v_datos_actuales + 'ext5:' + ltrim(rtrim(@i_extension5))
if ( @i_tipoTlf5 is not null and @i_tipoTlf5<> @v_tipoTlf5)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf5:' + ltrim(rtrim(@i_tipoTlf5))
if ( @i_codOperadora5 is not null and @i_codOperadora5<> @v_codOperadora5)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe5:' + ltrim(rtrim(@i_codOperadora5))
if ( @i_marcaTlf5 is not null and @i_marcaTlf5<> @v_marcaTlf5 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf5:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf5)))

if ( @i_telefono6 is not null and @i_telefono6<> @v_telefono6)
 select @v_datos_actuales = @v_datos_actuales + 'tlf6:' + ltrim(rtrim(@i_telefono6))
if ( @i_extension6 is not null and @i_extension6<> @v_extension6)
 select @v_datos_actuales = @v_datos_actuales + 'ext6:' + ltrim(rtrim(@i_extension6))
if ( @i_tipoTlf6 is not null and @i_tipoTlf6<> @v_tipoTlf6)
 select @v_datos_actuales = @v_datos_actuales + 'tipTlf6:' + ltrim(rtrim(@i_tipoTlf6))
if ( @i_codOperadora6 is not null and @i_codOperadora6<> @v_codOperadora6)
 select @v_datos_actuales = @v_datos_actuales + 'codOpe6:' + ltrim(rtrim(@i_codOperadora6))
if ( @i_marcaTlf6 is not null and @i_marcaTlf6<> @v_marcaTlf6 )
 select @v_datos_actuales = @v_datos_actuales + 'marTlf6:' + ltrim(rtrim(convert(varchar(3),@i_marcaTlf6)))


if ( @i_verificaTlf is not null and @i_verificaTlf<> @v_verificaTlf )
 select @v_datos_actuales = @v_datos_actuales + 'verificaTlf:' + ltrim(rtrim(convert(varchar(3),@i_verificaTlf)))
if ( @aux_codUsuarioVerifica is not null and @aux_codUsuarioVerifica<> @v_codUsuarioVerifica )
 select @v_datos_actuales = @v_datos_actuales + 'codUsuarioVerifica:' + ltrim(rtrim(convert(varchar(3),@aux_codUsuarioVerifica)))
if (@aux_fechaVerificaTlf is not null and @aux_fechaVerificaTlf <> @v_fechaVerificaTlf ) 
 select @v_datos_actuales = @v_datos_actuales + 'fechaVerificaTlf:' + ltrim(rtrim(convert(varchar(8),@aux_fechaVerificaTlf,112))) + CHAR(9) 
if (@v_codCalificacion is not null and @i_codCalificacion <> @v_codCalificacion ) 
 select @v_datos_actuales = @v_datos_actuales + 'Calificacion:' + ltrim(rtrim(convert(varchar(8),@i_codCalificacion,112))) + CHAR(9) 

if (@v_diaPago is not null and @i_diaPago <> @v_diaPago ) 
 select @v_datos_actuales = @v_datos_actuales + 'diaPago:' + ltrim(rtrim(convert(varchar(8),@i_diaPago,112))) + CHAR(9) 
if (@v_nombresReferencia is not null and @i_nombresReferencia <> @v_nombresReferencia ) 
 select @v_datos_actuales = @v_datos_actuales + 'Nombres Referencia:' + ltrim(rtrim(@i_nombresReferencia)) + CHAR(9) 
if (@v_parentescoReferencia is not null and @i_parentescoReferencia <> @v_parentescoReferencia ) 
 select @v_datos_actuales = @v_datos_actuales + 'Parentesco:' + ltrim(rtrim(@i_parentescoReferencia)) + CHAR(9) 
if (@v_telefonosReferencia is not null and @i_telefonosReferencia <> @v_telefonosReferencia) 
 select @v_datos_actuales = @v_datos_actuales + 'TlfRef:' + ltrim(rtrim(@i_telefonosReferencia)) + CHAR(9) 
if (@v_fechaEntregaTarjeta is not null and @i_fechaEntregaTarjeta <> @v_fechaEntregaTarjeta) 
 select @v_datos_actuales = @v_datos_actuales + 'FecEntTarjeta:' + ltrim(rtrim(@i_fechaEntregaTarjeta)) + CHAR(9) 
if (@v_recibeTarjeta is not null and @i_recibeTarjeta <> @v_recibeTarjeta) 
 select @v_datos_actuales = @v_datos_actuales + 'RecibeTarjeta:' + ltrim(rtrim(@i_recibeTarjeta)) + CHAR(9) 
if (@v_baseActa is not null and @i_baseActa <> @v_baseActa) 
 select @v_datos_actuales = @v_datos_actuales + 'baseActa:' + ltrim(rtrim(@i_baseActa)) + CHAR(9) 
if (@v_seguro is not null and @i_seguro <> @v_seguro) 
 select @v_datos_actuales = @v_datos_actuales + 'Seguro:' + ltrim(rtrim(@i_seguro)) + CHAR(9) 
if (@v_fechaSeguro is not null and @i_seguro <> @v_seguro) 
 select @v_datos_actuales = @v_datos_actuales + 'fecSeguro:' + ltrim(rtrim(convert(varchar(8),@v_fechaSeguro,112))) + CHAR(9) 
if (@v_devolucionSeguro is not null and @i_devolucionSeguro <> @v_devolucionSeguro) 
 select @v_datos_actuales = @v_datos_actuales + 'devSeguro:' + ltrim(rtrim(@i_devolucionSeguro)) + CHAR(9) 
if (@v_discapacitado is not null and @i_discapacitado <> @v_discapacitado) 
 select @v_datos_actuales = @v_datos_actuales + 'discapacitado:' + ltrim(rtrim(@i_discapacitado)) + CHAR(9) 
if (@v_codProvincia is not null and @i_codProvincia <> @v_codProvincia) 
 select @v_datos_actuales = @v_datos_actuales + 'codProvincia:' + ltrim(rtrim(@i_codProvincia)) + CHAR(9) 
if (@v_codParroquia is not null and @i_codParroquia <> @v_codParroquia) 
 select @v_datos_actuales = @v_datos_actuales + 'codParroquia:' + ltrim(rtrim(@i_codParroquia)) + CHAR(9) 
if (@v_facturacionElectronica is not null and @i_facturacionElectronica <> @v_facturacionElectronica) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_facturacionElectronica)) + CHAR(9) 
if (@v_correoFacturacionElectronica is not null and @i_correoFacturacionElectronica <> @v_correoFacturacionElectronica) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica)) + CHAR(9) 
if (@v_correoFacturacionElectronica2 is not null and @i_correoFacturacionElectronica2 <> @v_correoFacturacionElectronica2) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica2)) + CHAR(9) 
if (@v_correoFacturacionElectronica3 is not null and @i_correoFacturacionElectronica2 <> @v_correoFacturacionElectronica2) 
 select @v_datos_actuales = @v_datos_actuales + 'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica2)) + CHAR(9) 
if (@v_codGrupoComercial is not null and @i_codGrupoComercial <> @v_codGrupoComercial) 
 select @v_datos_actuales = @v_datos_actuales + 'codGrupoComercial:' + ltrim(rtrim(convert(varchar(8),@i_codGrupoComercial,112))) 
if (@v_esCanalVentaPromocional is not null and @i_esCanalVentaPromocional <> @v_esCanalVentaPromocional) 
 select @v_datos_actuales = @v_datos_actuales + 'esCanalVentaPromocional:' + ltrim(rtrim(convert(varchar(8),@i_esCanalVentaPromocional,112))) 
if (@v_codCanal is not null and @i_codCanal <> @v_codCanal) 
 select @v_datos_actuales = @v_datos_actuales + 'codCanal:' + ltrim(rtrim(convert(varchar(8),@i_codCanal,112))) 
if (@v_codCiudad_Sucursal_Referido is not null and @i_codCiudad_Sucursal_Referido <> @v_codCiudad_Sucursal_Referido) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'codCiudad_Sucursal_Referido:' + ltrim(rtrim(convert(varchar(8),@i_codCiudad_Sucursal_Referido,112))) --dceli
if (@v_codCliente_Referido is not null and @i_codCliente_Referido <> @v_codCliente_Referido) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'codCliente_Referido:' + ltrim(rtrim(convert(varchar(8),@i_codCliente_Referido,112))) --dceli
if (@v_resultadoModelo is not null and @i_resultadoModelo <> @v_resultadoModelo) --hportocarrero
 select @v_datos_actuales = @v_datos_actuales + 'resultadoModelo:' + ltrim(rtrim(convert(varchar(30),isnull(@i_resultadoModelo,''),112))) --hportocarrero
if (@v_segmentacion is not null and @i_segmentacion <> @v_segmentacion) --hportocarrero
 select @v_datos_actuales = @v_datos_actuales + 'segmentacion:' + ltrim(rtrim(convert(varchar(30),isnull(@i_segmentacion,''),112))) --hportocarrero
if (@v_capacidadPago is not null and @i_capacidadPago <> @v_capacidadPago) --hportocarrero
 select @v_datos_actuales = @v_datos_actuales + 'capacidadPago:' + ltrim(rtrim(convert(varchar(30),isnull(@i_capacidadPago,0),112))) --hportocarrero
if (@v_rangoIngresos is not null and @i_rangoIngresos <> @v_rangoIngresos) --hportocarrero
 select @v_datos_actuales = @v_datos_actuales + 'rangoIngresos:' + ltrim(rtrim(convert(varchar(30),isnull(@i_rangoIngresos,''),112))) --hportocarrero
if (@v_formaPago is not null and @i_formaPago <> @v_formaPago) --hportocarrero
 select @v_datos_actuales = @v_datos_actuales + 'formaPago:' + ltrim(rtrim(convert(varchar(50),isnull(@i_formaPago,''),112))) --hportocarrero 
if (@v_enviadoTarjeta is not null and @i_enviadoTarjeta <> @v_enviadoTarjeta) --dbasurto
 select @v_datos_actuales = @v_datos_actuales + 'enviadoTarjeta:' + ltrim(rtrim(convert(varchar(50),isnull(@i_enviadoTarjeta,''),112))) --dbasurto
if (@v_fechaActualizacion is not null ) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'fechaActualizacion:' + ltrim(rtrim(convert(varchar(50),isnull(@v_fechaActualizacion,''),112))) --dceli
if (@v_codUsuario_actualizacion is not null ) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'codUsuarioActualizacion:' + ltrim(rtrim(convert(varchar(50),isnull(@v_codUsuario_actualizacion,''),112))) 
if (@v_confirmacionTelefonica is not null ) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'confirmacionTelefonica:' + ltrim(rtrim(convert(varchar(50),isnull(@v_confirmacionTelefonica,''),112))) 
if (@v_calificacionAsesor is not null ) --dceli
 select @v_datos_actuales = @v_datos_actuales + 'calificacionAsesor:' + ltrim(rtrim(convert(varchar(50),isnull(@v_calificacionAsesor,''),112))) 
if (@v_fecha_actualizacion_discapacitado is not null and @i_fecha_actualizacion_discapacitado <> @v_fecha_actualizacion_discapacitado) --baguirre
 select @v_datos_actuales = @v_datos_actuales + ' fechaActualizacionDiscapacitado:' + ltrim(rtrim(convert(varchar(50),isnull(@i_fecha_actualizacion_discapacitado,''),112))) --baguirre
 
if (@v_datos_actuales <> '')
	insert into log_general_transaccion (
		nombre_Tabla, codSucursal, codigo, datos_Actuales, fecha_Registro, codUsuario, codProceso)
		values('cliente', @i_codCiudad_Sucursal, @i_codCliente,@v_datos_Actuales,getdate(),@i_codUsuario,'M')

declare @codcontrato_plan int
if (@i_seguro <> isnull(@v_seguro,0) or @i_devolucionSeguro <> isnull(@v_devolucionSeguro,0))  
begin
	exec sp_Seguro_CreaRequerimiento @i_codCiudad_Sucursal,@i_codCliente, @i_seguro,@i_devolucionSeguro, @i_codUsuario
end
		
		
		
END



-----CANJES


GO

