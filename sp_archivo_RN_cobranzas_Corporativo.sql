USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_archivo_RN_cobranzas_Corporativo]    Script Date: 15/6/2022 12:29:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:		Evelyn Alcivar
-- Create date: 2019/08/29
-- Description:	Generación de Archivo Corporativo Para la carga en RN Cobranzas
-- =============================================
--EXEC sp_archivo_RN_cobranzas_Corporativo
--SELECT * FROM netplus.dbo.cobranzasRNCorporativo
ALTER   PROCEDURE [dbo].[sp_archivo_RN_cobranzas_Corporativo]
 @fechaEmision varchar(8)=''
AS
BEGIN
SET @fechaEmision=convert(varchar(18),(getdate()),112 )

	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'netplus_pruebas.dbo.cobranzasRNCorporativoPrueba') AND type in (N'U'))
      drop table netplus_pruebas.dbo.cobranzasRNCorporativo


--DROP TABLE #tmpCP
select cp.codcontrato, cp.codcontrato_plan, cp.codservicio, cp.codestado_csp, 
isnull(s.precedencia,0) as precedencia, e.facturable as facturable,cp.codCliente,cp.codSucursal
,CASE 
 	WHEN cp.codEstado_csp = 0 or cp.codEstado_csp = 10 THEN 1
 	ELSE 0
 END AS estadoContrato
into #tmpCP from contrato_plan cp
inner join servicio s on cp.codservicio = s.codservicio
inner join estado_csp e on cp.codestado_csp = e.codestado_csp

------------------

--DROP TABLE #tmpClienteXcontrato
SELECT 	codSucursal,codCliente,codContrato,
		MAX(codContrato_Plan) AS codContrato_Plan,
		MAX(estadoContrato) AS  estadoContrato
		INTO #tmpClienteXcontrato
FROM #tmpCP 
GROUP BY codSucursal,codCliente,codContrato

--DROP TABLE #tmpEstadoCliente
SELECT 	txc.codSucursal,txc.codCliente,
		MAX(txc.estadoContrato)AS estadoCLiente ,MAX(txc.codContrato) AS codContratoUltimo
		INTO #tmpEstadoCliente
FROM #tmpClienteXcontrato txc
WHERE estadoContrato = 1 
GROUP BY txc.codSucursal,txc.codCliente


INSERT INTO #tmpEstadoCliente
SELECT 	txc.codSucursal,txc.codCliente,
		MAX(txc.estadoContrato)AS estadoCLiente ,MAX(txc.codContrato) AS codContratoUltimo
		FROM #tmpClienteXcontrato txc
LEFT OUTER JOIN #tmpEstadoCliente ec ON ec.codCliente = txc.codCliente AND ec.codSucursal = txc.codSucursal
WHERE estadoContrato = 0 AND ec.codContratoUltimo <> txc.codContrato
GROUP BY txc.codSucursal,txc.codCliente


--drop table #tmp_mt
select cedula_ruc, codSucursal, codCliente, nombre, max(estadoCliente) as estado , sum(por_pagar)as por_pagar , 
        MAX(CODCONTRATO) AS contrato,max(ejecutivo_Ventas) AS ejecutivo
into #tmp_mt
from medios_totales
group by cedula_ruc, codSucursal, codCliente,nombre 
ORDER BY 1 DESC 

insert into #tmp_mt
SELECT c.cedula_Ruc,t.codSucursal,t.codCliente,ltrim(rtrim(c.nombres+' '+c.apellidos+c.nombre_Empresa)) AS nombre 
	,t.estadoCliente,0,t.codContratoUltimo
	,p.nombres+' '+p.apellidos AS ejecutivo 
from #tmpEstadoCliente t
INNER JOIN contrato c2 ON t.codContratoUltimo=c2.codContrato
INNER JOIN cliente c ON c.codCliente=t.codCliente AND c.codCiudad_Sucursal=t.codSucursal
LEFT OUTER JOIN #tmp_mt mt ON mt.codCliente=t.codCliente AND mt.codSucursal=t.codSucursal
LEFT OUTER JOIN personal p ON p.codPersonal=c2.codejecutivo
WHERE t.estadoCliente=1
AND mt.codCliente IS NULL

--drop table #tmp_repetidos
select cedula_ruc, count(cedula_ruc) as numero
into #tmp_repetidos
from #tmp_mt
group by cedula_ruc
having count(cedula_ruc)>1


--drop table #tmp_devoluciones
SELECT cli.cedula_Ruc, count(rc.idTicket) as numtickets 
into #tmp_devoluciones
FROM requerimiento_consolidado rc
INNER JOIN dbo.contrato_plan cp ON cp.codContrato_Plan = rc.codContratoPlan
INNER JOIN cliente cli ON cli.codCliente = cp.codCliente AND cli.codCiudad_Sucursal = cp.codSucursal
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
where  rc.desRequerimiento =  'Solicitud Nota Credito y Devolucion' 
and rc.desEstado not like '%Rechazado%' 
and r.cedula_ruc is NULL
group by cli.cedula_Ruc

--drop table #tmp_mora
SELECT cli.cedula_Ruc, count(rc.idTicket) as numtickets 
into #tmp_mora
FROM requerimiento_consolidado rc
INNER JOIN dbo.contrato_plan cp ON cp.codContrato_Plan = rc.codContratoPlan
INNER JOIN cliente cli ON cli.codCliente = cp.codCliente AND cli.codCiudad_Sucursal = cp.codSucursal
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
where  rc.desRequerimiento LIKE  '%DESACTIVACION%' AND  motivoDesactivacion LIKE '%MORA%' 
and rc.desEstado not like '%Rechazado%' 
and r.cedula_ruc is NULL
group by cli.cedula_Ruc

--drop table #tmp_RETENCIONES
SELECT cli.cedula_Ruc, count(rc.idTicket) as numtickets 
into #tmp_RETENCIONES
FROM requerimiento_consolidado rc
INNER JOIN dbo.contrato_plan cp ON cp.codContrato_Plan = rc.codContratoPlan
INNER JOIN cliente cli ON cli.codCliente = cp.codCliente AND cli.codCiudad_Sucursal = cp.codSucursal
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
where  rc.desRequerimiento LIKE  '%COMPENSACION%' 
and rc.desEstado not like '%Rechazado%' 
and r.cedula_ruc is NULL
group by cli.cedula_Ruc

--drop table #tmpmaxcontrato
SELECT MAX(codContrato) AS codcontrato,codCliente,codSucursal 
INTO #tmpmaxcontrato 
FROM #tmpClienteXcontrato 
GROUP BY codSucursal, codCliente --255320

--drop table #tmp_ejecutivo 
SELECT p.nombres + ' ' + p.apellidos AS ejecutivo, con.codContrato,con.codtipo_contrato,con.codCliente, con.codSucursal, p.estado,
case when con.codSucursal = 1 THEN 'Quito'--939 
        WHEN con.codSucursal = 8 THEN 'Cuenca'--941
		when con.codSucursal = 14 THEN 'Guayaquil'--940f
		when con.codSucursal = 6 THEN 'Santo Domingo'--943 
		when con.codSucursal = 13 THEN 'Manta'--942
		end AS ciudadFacturacion
into #tmp_ejecutivo 
FROM CONTRATO CON 
INNER JOIN dbo.personal p ON P.codPersonal = CON.codejecutivo and p.estado = 'A' 


/******INGRESO CLIENTES CON CREDITOS Y CARGOS***************/
---DROP TABLE #tmp_primera_ultima_factura_base
SELECT  f.codContrato AS contrato,
        a.codContrato_Plan,
	    max(f.codContrato)as codcontrato,		
		MIN(f.fecha) AS fecha_Primera_factura,
		MAX(f.fecha) AS fecha_Ultima_factura
INTO #tmp_primera_ultima_factura_base
FROM    dbo.factura AS f INNER JOIN
        dbo.asiento AS a ON f.codSucursal = a.codSucursal and f.puntoEmision = a.puntoEmision AND f.numFactura = a.numFactura
        left outer join view_facturas_anuladas fa on f.codSucursal = fa.codSucursal and f.puntoEmision = fa.puntoEmision AND f.numFactura = fa.numFactura --and fa.total>= f.total
WHERE   fa.codSucursal is NULL 
GROUP BY a.codContrato_Plan,f.codContrato


--DROP TABLE #tmp_primera_ultima_factura_punto_emision
select p.codcontrato as codcontrato, p.codcontrato_plan, 
p.fecha_primera_Factura as primera_factura, 
MIN(fp.puntoEmision) as primerPuntoEmision,
p.fecha_ultima_factura  AS ultima_factura,
MAX(fu.puntoEmision) as ultimoPuntoEmision
INTO #tmp_primera_ultima_factura_punto_emision
from #tmp_primera_ultima_factura_base p
left join factura fp on p.codcontrato = fp.codContrato and fp.fecha=p.fecha_primera_factura
left join factura fu on p.codcontrato = fu.codContrato and fu.fecha=p.fecha_ultima_factura
group by p.codcontrato_plan, p.codcontrato, p.fecha_primera_Factura, p.fecha_ultima_factura
ORDER BY p.codcontrato_plan

--DROP TABLE #tmp_view_primera_ultima_factura
select p.codcontrato as codcontrato, p.codcontrato_plan, 
p.primera_factura, 
case 
	when MIN(fp.emision) is null then convert(datetime,p.primera_Factura)
	else MIN(fp.emision) 
end as emisionPriFac, 
min(fp.numfactura) as priFactura,
p.ultima_factura  AS ultima_factura,
max(fu.numfactura)as ultFactura,
p.primerPuntoEmision, p.ultimoPuntoEmision
INTO #tmp_view_primera_ultima_factura
from #tmp_primera_ultima_factura_punto_emision p
inner join factura fp on p.codcontrato = fp.codContrato and fp.fecha=p.primera_factura and fp.puntoEmision = p.primerPuntoEmision
inner join factura fu on p.codcontrato = fu.codContrato and fu.fecha=p.ultima_factura and fu.puntoEmision = p.ultimoPuntoEmision
group by p.codcontrato_plan, p.codcontrato, p.primera_Factura, p.ultima_factura, p.primerPuntoEmision, p.ultimoPuntoEmision


--drop table #tmp_fechafactura
SELECT max(convert(varchar(10),convert(date,vf.ultima_factura,106),103)) AS ultima_factura, cli.cedula_Ruc, vf.codcontrato,max(vf.ultFactura) AS numeroUltimaFactura
into #tmp_fechafactura
FROM #tmp_view_primera_ultima_factura vf
INNER JOIN contrato c ON c.codContrato = vf.codcontrato
INNER JOIN #tmp_mt m on m.contrato = c.codcontrato
INNER JOIN cliente cli ON cli.codCliente = c.codCliente AND cli.codCiudad_Sucursal = c.codSucursal 
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
WHERE r.cedula_ruc is NULL
GROUP BY cli.cedula_Ruc,vf.codcontrato


--DROP TABLE #tmp_ValorTotalFacturado
SELECT  f.total, m.cedula_ruc,m.codCliente,f.por_Pagar
INTO #tmp_ValorTotalFacturado
FROM #tmp_mt m  
INNER JOIN factura f on m.contrato = f.codcontrato AND f.codSucursal = m.codSucursal AND f.codCliente = m.codCliente
INNER  JOIN #tmp_fechafactura ff ON ff.codcontrato = f.codContrato  AND ff.numeroUltimaFactura = f.numFactura 
group by m.cedula_ruc,f.total,m.codCliente,f.por_Pagar


--drop table #tmp_primerafactura
SELECT min(vf.primera_factura) AS primera_factura, cli.cedula_Ruc, max(vf.ultima_factura) AS ultima_factura
into #tmp_primerafactura
FROM #tmp_view_primera_ultima_factura vf
INNER JOIN contrato c ON c.codContrato = vf.codcontrato
INNER JOIN #tmp_mt m on m.contrato = c.codcontrato
INNER JOIN cliente cli ON cli.codCliente = c.codCliente AND cli.codCiudad_Sucursal = c.codSucursal
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
WHERE r.cedula_ruc is NULL
GROUP BY cli.cedula_Ruc 


--drop table #tmp_mesespendientes
SELECT  f.codCliente, f.codSucursal, f.codContrato, count(f.codContrato) AS total
into #tmp_mesespendientes
FROM         dbo.factura f
--LEFT OUTER join pago p on p.codSucursal = f.codSucursal and p.numFactura = f.numFactura and p.codCliente= f.codCliente 
WHERE f.por_pagar <> 0
GROUP BY f.codCliente, f.codSucursal,f.codContrato


--drop table #tmp_geografia
SELECT cli.cedula_Ruc,max(cp.codContrato_Plan)  as contratoplan 
into #tmp_geografia
FROM dbo.view_contratoPlan_costeo_netplus vcp
INNER JOIN dbo.contrato_plan cp ON cp.codContrato_Plan = vcp.codContrato_Plan
INNER JOIN cliente cli ON cli.codCliente = cp.codCliente AND cli.codCiudad_Sucursal = cp.codSucursal
INNER JOIN #tmp_mt m ON m.Contrato = cp.codContrato
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
WHERE r.cedula_ruc is NULL
group by cli.cedula_Ruc, vcp.codContrato
ORDER BY cli.cedula_Ruc


--drop table #tmp_geografiafinal
SELECT vcp.des_geografia_oracle, g.cedula_ruc,vcp.codContrato_Plan
into #tmp_geografiafinal
FROM  dbo.view_contratoPlan_costeo_netplus vcp 
INNER JOIN #tmp_geografia g ON g.contratoplan = vcp.codContrato_Plan --105.207


--drop table #tmp_celerity 
select cli.cedula_Ruc, max(p.esCelerity) as celerity 
into #tmp_celerity 
from dbo.contrato_plan cp
INNER JOIN planes p ON p.codPlan = cp.codPlan
INNER JOIN cliente cli ON cli.codCliente = cp.codCliente AND cli.codCiudad_Sucursal = cp.codSucursal
INNER JOIN #tmp_mt m ON m.Contrato = cp.codContrato
left outer join #tmp_repetidos r on cli.cedula_ruc = r.cedula_ruc
WHERE r.cedula_ruc is NULL
group by cli.cedula_Ruc

--DROP TABLE #tmp_servicio
select c.codcliente,  c.codcontrato, tp.contrato, max(s.codservicio) AS codServicio  
into #tmp_servicio
from contrato c 
inner join #tmp_mt tp on tp.contrato = c.codcontrato
inner join contrato_plan cp on cp.codcontrato = c.codcontrato 
inner join servicio s on s.codservicio = cp.codservicio 
group by c.codcliente,  c.codcontrato, tp.contrato
order by c.codcliente --29708


select 
   /*m.contrato,
   case when cli.codTipo_Cliente = 1 THEN 'Cuenta Personal'
        when cli.codTipo_Cliente = 2 then 'Cuenta Corporativa'
   END AS TipoCliente,*/
   case when cli.esEmpresa = 0 then 297 
        WHEN cli.esEmpresa = 1 THEN 298
		WHEN cli.esEmpresa = 2 THEN 299
		WHEN cli.esEmpresa = 3 THEN 300
   END  as tipoIdentificacion, 
   m.cedula_ruc AS identificacion, 
   pro.id_RN AS provincia, 
   CASE WHEN substring((cli.nombres + '' + cli.nombre_Empresa),1,80) IS NULL THEN ''
        ELSE dbo.eliminar_caracteres_especiales(replace(replace(replace((substring(cli.nombres + '' + cli.nombre_Empresa,1,80)),'º',''),'ñ','n'),'Ñ','N'), '^a-z0-9- - ') 
   END AS nombre,
   CASE WHEN substring((cli.apellidos + '' + cli.nombre_comercial),1,80) IS NULL THEN ''
        ELSE dbo.eliminar_caracteres_especiales(replace(replace(replace((substring(cli.apellidos + '' + cli.nombre_comercial,1,80)),'º',''),'ñ','n'),'Ñ','N'), '^a-z0-9- - ') 
   END AS apellido,
   isnull(cli.contacto_facebook,'') AS contactoFacebook,
   case when d.numtickets IS NULL THEN 0 ELSE d.numtickets END  AS numeroDevoluciones,
   case when m.codSucursal = 1 THEN 'Quito'--939 
        WHEN m.codSucursal = 8 THEN 'Cuenca'--941
		when m.codSucursal = 14 THEN 'Guayaquil'--940f
		when m.codSucursal = 6 THEN 'Santo Domingo'--943 
		when m.codSucursal = 13 THEN 'Manta'--942
		end AS ciudadFacturacion, ---enviar las ciudades no códigos 27/08/2019
   --0 AS suspendidoMora,
   ltrim(rtrim(isnull(CASE WHEN cli.correoFacturacionElectronica IS NULL OR cli.correoFacturacionElectronica='' THEN CLI.correoFacturacionElectronica2
        WHEN (cli.correoFacturacionElectronica IS NULL OR cli.correoFacturacionElectronica='') AND (CLI.correoFacturacionElectronica2 IS NULL OR cli.correoFacturacionElectronica2='') THEN CLI.correoFacturacionElectronica3
		ELSE cli.correoFacturacionElectronica
        END,''))) AS email,
   replace(m.por_pagar,',','.') AS saldoPendiente, --encviar con punto(.) y no con coma(,)
   isnull(cli.segmentacion,'') AS segmentacion,
   case when ret.numtickets IS NULL THEN 0 ELSE ret.numtickets end AS numeroRetenciones,
   CASE WHEN cli.direccion IS NULL THEN ''
         --ELSE dbo.eliminar_caracteres_especiales(replace(replace(replace(cli.direccion,'|',''),'ñ','n'),'Ñ','N'), '^a-z- - ')
        ELSE dbo.eliminar_caracteres_especiales(replace(replace(replace(replace(replace(replace(replace(cli.direccion,'º',''),'ñ','n'),'Ñ','N'),'|',' '),'Ã',''),'Ã³',''),'Â',''), '^a-z0-9- - ')
   END AS direccion,
   --replace(cli.direccion,'|', '') AS direccion, --reemplazar | por vacios 
   CASE WHEN cli.tipoTlf1 = 'T' THEN cli.telefono1
        WHEN cli.tipoTlf2 = 'T' THEN cli.telefono2
		WHEN cli.tipoTlf3 = 'T' THEN cli.telefono3
		WHEN cli.tipoTlf4 = 'T' THEN cli.telefono4
		WHEN cli.tipoTlf5 = 'T' THEN cli.telefono5
		WHEN cli.tipoTlf6 = 'T' THEN cli.telefono6
		ELSE ''
   END AS telefonoOficina,
   CASE WHEN cli.tipoTlf1 = 'A' THEN cli.telefono1
        WHEN cli.tipoTlf2 = 'A' THEN cli.telefono2
		WHEN cli.tipoTlf3 = 'A' THEN cli.telefono3
		WHEN cli.tipoTlf4 = 'A' THEN cli.telefono4
		WHEN cli.tipoTlf5 = 'A' THEN cli.telefono5
		WHEN cli.tipoTlf6 = 'A' THEN cli.telefono6
		ELSE ''
   END AS telefonoParticular,
   CASE WHEN cli.tipoTlf1 = 'C' THEN cli.telefono1
        WHEN cli.tipoTlf2 = 'C' THEN cli.telefono2
		WHEN cli.tipoTlf3 = 'C' THEN cli.telefono3
		WHEN cli.tipoTlf4 = 'C' THEN cli.telefono4
		WHEN cli.tipoTlf5 = 'C' THEN cli.telefono5
		WHEN cli.tipoTlf6 = 'C' THEN cli.telefono6
		ELSE ''
   END AS telefonoMovil,
   cl.celerity AS celerity,
   2 AS pais,
   case when cli.codTipo_Cliente = 1 then 305 WHEN cli.codTipo_Cliente = 2 THEN 306 END AS tipoCliente,
   case when m.codSucursal = 1 then 'Quito'
        when m.codSucursal = 8 then 'Cuenca'
		when m.codSucursal = 14 then 'Guayaquil'
		when m.codSucursal = 6 then 'Santo Domingo'
		when m.codSucursal = 13 then 'Manta' 
   END AS ciudad,
   m.codCliente AS codigoCliente,
   ej.ejecutivo as ejecutivoCobranzas,
   case when m.estado = 1 THEN 'ACTIVO'
        WHEN m.estado = 0 THEN 'DESACTIVADO' 
		END AS estadoCliente,
   ff.ultima_factura AS  FechaUltimafactura, --enviar en formato dd/mm/yyyy
   ISNULL(mp.total,0) as mesesPendiente,
   '' AS modeloPerdidaEsperada,
   DATEDIFF(mm, pf.primera_factura, pf.ultima_factura ) AS permanencia,
   case when mc.recurrenciaEnMora IS NULL THEN 'N/A' ELSE mc.recurrenciaEnMora 
   END  AS recurrenciaMora,
   vcp.codigo_postal AS ruta, -- se solicita enviar el codigo postal en vez de la zon
   --cli.zona AS ruta,
   --934 AS ruta,
   gf.des_geografia_oracle AS geografia,
   ta.desTipo_Actividad AS tipoactividad,
   --cli.codTipo_Actividad, --validar con la tabla tipo_actividad debe ir en texto
   cfp.causal_Rechazo AS causalRechazo,
   '' AS estrategia,
   isnull(replace(mp.total,',','.'),0) AS antiguedadDeuda,
   replace(m.por_pagar,',','.') as valorTotalFacturado,
   replace(isnull(vtf.total,0),',','.')  as valorFacturadoMesActual,
   replace(isnull(vtf.por_Pagar,0),',','.') as deudaMesActual,
   case when se.desServicio = 'Wifi' THEN 1140
        WHEN se.desServicio = 'Internet' THEN 1133
		WHEN se.desServicio = 'Canal de Conexion' THEN 1128
		WHEN se.desServicio = 'Fibra Home' THEN 1132
		WHEN se.desServicio = 'Seguridad Gestionada' THEN 1136
		WHEN se.desServicio = 'CLOUD' THEN 1129
		WHEN se.desServicio = 'ADSL HOME' THEN 1046
		WHEN se.desServicio = 'VSat' THEN 1138
		WHEN se.desServicio = 'Buzón Adicional' THEN 1127
		WHEN se.desServicio = 'Colocation' THEN 1130
		WHEN se.desServicio = 'Dominio' THEN 1131
		WHEN se.desServicio = 'Otros' THEN 1134
		WHEN se.desServicio = 'VoIP' THEN 1137
		WHEN se.desServicio = 'Web Hosting' THEN 1139
		WHEN se.desServicio = 'Real Audio' THEN 1135
   ELSE 1134
   END AS tipoServicio,
   CASE when fp.desforma_pago = 'Abogado' then 1096								
		when fp.desforma_pago = 'Ahorros Pacifico' then 1096
		when fp.desforma_pago = 'Ajuste Notas de Credito' then 1102
		when fp.desforma_pago = 'AJUSTE PAGO DESCUENTO ROL' then 1093
		when fp.desforma_pago = 'Ajustes' then 1080
		when fp.desforma_pago = 'American Express' then 1064
		when fp.desforma_pago = 'Arriendo Nodos' then 1105
		when fp.desforma_pago = 'Automatico' then 1047
		when fp.desforma_pago = 'Banco de Loja AH' then 1094
		when fp.desforma_pago = 'Banco de Machala AH' then 1092
		when fp.desforma_pago = 'Banco de Machala CC' then 1120
		when fp.desforma_pago = 'Banco del Austro Ahorros' then 1087
		when fp.desforma_pago = 'Banco del Austro CC' then 1088
		when fp.desforma_pago = 'Bolivariano AH' then 1067
		when fp.desforma_pago = 'Bolivariano CC' then 1066
		when fp.desforma_pago = 'BPACompany' then 1125
		when fp.desforma_pago = 'Canje Marketing' then 1082
		when fp.desforma_pago = 'Canje Oficina' then 1081
		when fp.desforma_pago = 'Canje Sistema Financiero' then 1114
		when fp.desforma_pago = 'Canjes' then  1063
		when fp.desforma_pago = 'Cartera condonada' then 1126
		when fp.desforma_pago = 'CASTIGO' then 1075
		when fp.desforma_pago = 'Castigo Ajuste' then 1077
		when fp.desforma_pago = 'Cheque' then 1113
		when fp.desforma_pago = 'Cliente en Oficina' then 1103
		when fp.desforma_pago = 'Cobra Legal' then 1086
		when fp.desforma_pago = 'COOP 29 DE OCTUBRE' then 1117
		when fp.desforma_pago = 'COOP AH Y CR POLICIA NACIONAL' then 1116
		when fp.desforma_pago = 'COOP AHO Y CREDITO JARDIN AZUAYO' then 1118
		when fp.desforma_pago = 'COOP AHO Y CREDITO NACIONAL' then 1119
		when fp.desforma_pago = 'Cooperativa JEP' then 1097
		when fp.desforma_pago = 'Cooperativa Santa Anita' then 1115
		when fp.desforma_pago = 'Corrientes Pacifico' then 1052
		when fp.desforma_pago = 'Cta en Regularizacion' then 1099
		when fp.desforma_pago = 'Ctr Factura' then 1048
		when fp.desforma_pago = 'Datamovil' then 1106
		when fp.desforma_pago = 'Depositos' then 1078
		when fp.desforma_pago = 'Diners' then 1050
		when fp.desforma_pago = 'Discover' then 1095
		when fp.desforma_pago = 'Efectivo' then 1112
		when fp.desforma_pago = 'Empleados' then 1090
		when fp.desforma_pago = 'Empleados Convenios' then 1121	
		when fp.desforma_pago = 'Empleados Rol' then 1122
		when fp.desforma_pago = 'Fideicomiso Puntonet' then 1111
		when fp.desforma_pago = 'Gestión American' then 1083
		when fp.desforma_pago = 'Gestión Bolivariano' then 1085
		when fp.desforma_pago = 'Gestión Diners' then 1070
		when fp.desforma_pago = 'Gestión Gye' then 1072
		when fp.desforma_pago = 'Gestión Internacional' then 1071
		when fp.desforma_pago = 'Gestión Mastercard' then 1084
		when fp.desforma_pago = 'Gestión Pacifico' then 1069
		when fp.desforma_pago = 'Gestión Pichincha' then 1065
		when fp.desforma_pago = 'Gestión Produbanco' then 1068
		when fp.desforma_pago = 'Gestión Rumiñahui' then 1074
		when fp.desforma_pago = 'Gestión Visa' then 1073
		when fp.desforma_pago = 'Global Support' then 1123
		when fp.desforma_pago = 'Guayaquil AH' then 1060
		when fp.desforma_pago = 'Guayaquil CC' then 1061
		when fp.desforma_pago = 'INTERCOBROS' then 1100
		when fp.desforma_pago = 'Internacional Ahorros' then 1089
		when fp.desforma_pago = 'Internacional Corriente' then 1056
		when fp.desforma_pago = 'Mastercard' then 1053
		when fp.desforma_pago = 'Mutualista Imbabura' then 1124
		when fp.desforma_pago = 'OTROS MEDIOS' then 1091
		when fp.desforma_pago = 'Pichincha AH' then 1049
		when fp.desforma_pago = 'Pichincha CC' then 1057
		when fp.desforma_pago = 'Prelegal' then 1059
		when fp.desforma_pago = 'Prelegal 1' then 1062
		when fp.desforma_pago = 'Proceso en Linea' then 1108
		when fp.desforma_pago = 'Produbanco AH' then 1054
		when fp.desforma_pago = 'Provision Cartera' then 1107
		when fp.desforma_pago = 'PuntoPago(Pichincha)' then 1101
		when fp.desforma_pago = 'Rumiñahui AH' then 1058
		when fp.desforma_pago = 'Rumiñahui CC' then 1076
		when fp.desforma_pago = 'SCI Produbanco' then 1104
		when fp.desforma_pago = 'Servipagos' then 1098
		when fp.desforma_pago = 'Tarjetas Banco del Austro' then 1109
		when fp.desforma_pago = 'Tarjetas Caja' then 1110
		when fp.desforma_pago = 'Transferencias' then 1079
		when fp.desforma_pago = 'VISA' then 1051
		when fp.desforma_pago = 'Coop Mego' then 1201
		when fp.desforma_pago = 'Banco Procredit SA AH' then 1310
		when fp.codforma_pago = 119   then 1311 -- Cooperativa 23 de Julio
		when fp.desforma_pago = 'BANCO COMERCIAL DE MANABI' then 1345
		when fp.desforma_pago = 'BANCO SOLIDARIO AH' then 1346
		when fp.desforma_pago = 'COOP. AHORRO Y CREDITO 15 DE ABRIL LTDA' then 1347
		when fp.desforma_pago = 'COOP. AHORRO Y CREDITO EL SAGRARIO' then 1348
		when fp.desforma_pago = 'COOP ANDALUCIA' then 1349
		when fp.desforma_pago = 'COOP. DE AHORRO Y CREDITO ATUNTAQUI LTDA' then 1350
		when fp.desforma_pago = 'COOP.AHORRO Y CREDITO ALIANZA DEL VALLE LTDA' then 1351
		when fp.desforma_pago = 'MUTUALISTA AZUAY' then 1352
		when fp.desforma_pago = 'MUTUALISTA PICHINCHA' then 1353
		when fp.desforma_pago = 'COOP. AHORRO Y CREDITO PUELLARO LTDA' then 1354
		when fp.desforma_pago = 'COOP. AHORRO Y CREDITO D LA PEQ EMPR CACPE BIBLIAN' then 1355
		when fp.desforma_pago = 'COOPERATIVA DE AHORRO Y CREDITO LA BENEFICA LTDA' then 1356
		when fp.desforma_pago = 'COOP AHORRO Y CREDITO MUSHUC RUNA LTDA' then 1357
		when fp.desforma_pago = 'COOP DE AHORRO Y CREDITO FERNANDO DAQUILEMA' then 1358
		when fp.desforma_pago = 'COOP DE AHORRO Y CREDITO PILAHUIN TIO LTDA' then 1359
		when fp.desforma_pago = 'BANCO COOPNACIONAL SA' then 1360
		when fp.desforma_pago = 'Produbanco CC' then 1361
		when fp.desforma_pago = 'BANCO SOLIDARIO CC' then 1362
		when fp.desforma_pago = 'Banco de Loja CC' then 1363
		when fp.desforma_pago = 'Banco Procredit SA CC' then 1364
		when fp.desforma_pago = 'Tarjeta Alia' then 1365
	ELSE ''
	END  as formaDePago,
	substring(m.ejecutivo,1,80) AS ejecutivoComercial,
	1 AS noActualizar,
	isnull(ec.descripcion,'') AS calificacion, 
	--ACTUALIZACION DE LA DIRECCIÓN DEL CLIENTE REQ 307
	isnull(dir.barrioSector,'') as barrioSector,
	dir.callePrincipal as callePricipal,
	dir.numeracion as numeracion, 
	dir.calleSecundaria as calleSecundaria,
	dir.Urbanizacion as urbanizacion,
	dir.referencia as referencia
INTO netplus_pruebas.dbo.cobranzasRNCorporativo
from #tmp_mt m
left outer join cliente cli ON cli.codCliente = m.codCliente AND m.codSucursal = cli.codCiudad_Sucursal
LEFT OUTER JOIN estados_cliente ec ON ec.nombre_campo='calificacion' AND ec.codigo=cli.calificacion
left outer join contrato_forma_pago cfp ON  m.contrato = cfp.codContrato 
LEFT OUTER JOIN forma_pago fp ON fp.codforma_pago = cfp.codForma_Pago
left outer join #tmp_ejecutivo ej ON ej.codcliente = m.codcliente AND m.codSucursal = ej.codSucursal AND m.contrato = ej.codContrato
left outer join #tmp_repetidos r on m.cedula_ruc = r.cedula_ruc
LEFT OUTER JOIN #tmp_devoluciones d ON d.cedula_ruc = m.cedula_Ruc
LEFT OUTER JOIN provincia pro ON pro.codprovincia = cli.codProvincia
LEFT OUTER JOIN #tmp_mora mr ON mr.cedula_ruc = m.cedula_Ruc
LEFT OUTER JOIN #tmp_RETENCIONES RET ON RET.cedula_ruc = m.cedula_Ruc
LEFT outer join #tmp_fechafactura ff ON ff.cedula_Ruc = m.cedula_Ruc
LEFT outer join #tmp_mesespendientes mp ON mp.codcontrato = m.contrato  AND mp.codSucursal = m.codSucursal
LEFT outer join #tmp_primerafactura pf ON pf.cedula_Ruc = m.cedula_Ruc 
LEFT OUTER JOIN tmp_modelo_cobro_ciclo mc ON mc.codCliente = m.codCliente AND mc.codSucursal = m.codSucursal
LEFT OUTER JOIN #tmp_geografiafinal gf ON gf.cedula_Ruc = m.cedula_Ruc
LEFT OUTER JOIN view_codigo_postal vcp ON  vcp.codContratoPlan = gf.codContrato_Plan
LEFT OUTER JOIN #tmp_celerity  cl ON cl.cedula_ruc = m.cedula_Ruc
LEFT OUTER JOIN tipo_actividad ta ON ta.codTipo_Actividad = cli.codTipo_Actividad
LEFT OUTER JOIN #tmp_servicio s ON s.codcontrato = m.contrato
LEFT OUTER JOIN Servicio se ON se.codServicio = s.codServicio
LEFT OUTER JOIN #tmp_ValorTotalFacturado vtf ON vtf.cedula_ruc = m.cedula_ruc
--ACTUALIZACION DE LA DIRECCIÓN DEL CLIENTE REQ 307
LEFT JOIN direccion dir ON dir.codCliente = CLI.codCliente and dir.codCiudadSucursal = CLI.codCiudad_Sucursal
where r.cedula_ruc is NULL
AND ff.ultima_factura IS NOT NULL
AND cli.codTipo_Cliente = 2

--SELECT cc.* 
UPDATE cc set cc.ejecutivoCobranzas = ejec.ejecutivo
from netplus_pruebas.dbo.cobranzasRNCorporativo cc 
INNER JOIN #tmp_ejecutivo ejec ON ejec.codCliente = cc.codigoCliente AND cc.ciudadFacturacion = ejec.ciudadFacturacion 
WHERE cc.ejecutivoCobranzas IS NULL --438 -403


declare @nombrearchivocsv varchar(150)
declare @rutaarchivocsv varchar(500)
declare @rutaynombrearchivocsv varchar(500)
declare @fechaarchivo varchar(8)
declare @sqlquery varchar(500)

set @fechaarchivo=@fechaEmision
set @rutaarchivocsv='D:\RNCOBRANZAS\'
--set @fechaarchivo=convert(varchar(8),@fechaEmision, 112)
set @nombrearchivocsv= 'Layout_Carga_Contactos_Corporativo' + '.csv' 
set @rutaynombrearchivocsv=@rutaarchivocsv+@nombrearchivocsv
set @sqlquery='SELECT * FROM ' +  DB_NAME() + '.dbo.cobranzasRNCorporativo'
 
print 'Archivo CSV_'
EXEC [comun].[usp_sys_crear_archivo_csv_separador]   @rutaynombrearchivocsv ,@sqlquery

--Union de los archivos con la cabecera
exec master..xp_cmdshell 'copy /b "d:\RNCOBRANZAS\Cabecera_Corporativo.csv"+"d:\RNCOBRANZAS\Layout_Carga_Contactos_Corporativo.csv" "d:\RNCOBRANZAS\Layout_Carga_Contactos_Corporativo_V1.csv"'

--Eliminamos el que no sirve 

exec master..xp_cmdshell 'del "d:\RNCOBRANZAS\Layout_Carga_Contactos_Corporativo.csv"'
print 'Archivo CSV__'
print  DB_NAME()
END
GO

