USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_Calidad_Consolidacion]    Script Date: 17/6/2022 9:27:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
    
    
    
    
/*******************************************************************    
                             PROPOSITO    
             
REPORTE CONSOLIDADO                                 
    
*******************************************************************    
      MODIFICACIONES                               
    FECHA             AUTOR             RAZON    
   12/11/2018    Carlos Lincango  Agregar a corporativo las columnas: migradoUsuario y migrado.    
                              
   19/09/2018      Juan Portero    Agregar para masivo y corporativo las columnas: des_linea_negocio,    
         des_producto_oracle, des_geografia_oracle,CobrarInstalacion, subida    
    
 15/10/2018  Hernan   Modificar que las instalaciones salgan con el descuento aplicado    
                               
*******************************************************************/     
    
--exec sp_Calidad_Consolidacion   
ALTER PROCEDURE [dbo].[sp_Calidad_Consolidacion]    
AS    
BEGIN    
declare @fechaDesde datetime
select @fechaDesde = getdate()
select @fechaDesde = dateadd(month,-6,dateadd(day,-(day(getdate())-1), getdate()))
select @fechaDesde = convert(datetime,convert(varchar(8),@fechaDesde,112))

----------------------nuevos requerimientos    
--------------------------------------------------------------------------------    
    
SELECT max(rc.idTicket) AS idTicket    
    ,rc.codContratoPlan    
    INTO #tmpUltimoTicketInstNuevoReq    
FROM requerimiento_consolidado rc    
WHERE rc.desRequerimiento LIKE '%solici%instalaci%'    
GROUP BY rc.codContratoPlan    
    
    
SELECT rc.desEstado    
    ,rc.fechaRegistroRequerimiento    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaRegistroRequerimiento,112),1,6),'') as MesRegistroRequerimiento    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaRegistroRequerimiento,112),7,2),'') as diaRegistroRequerimiento    
    ,rc.fechaAgendamiento AS fechaAgendamiento    
    ,rc.fechaInstalacion    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaInstalacion,112),1,6),'') as MesInstalacion    
    ,rc.motivoInstalacionMasivo     
    ,rc.codContratoPlan    
    ,rc.idTicket    
    ,CASE    
  WHEN c5.codCargo=253 THEN c5.desCargo    
  WHEN c5.codCargo IS NULL THEN ''    
  ELSE 'Planta Propia'    
 END AS cargoInstalador    
    INTO #tmpUltimoTicketInstNuevoReqFinal    
FROM requerimiento_consolidado rc    
INNER JOIN #tmpUltimoTicketInstNuevoReq nt ON nt.idTicket=rc.idTicket    
LEFT OUTER JOIN usuario u4 with (nolock)  ON u4.nombre_Usuario=rc.instalador    
LEFT OUTER JOIN personal pe4 with (nolock)  ON pe4.codPersonal=u4.codPersonal    
LEFT OUTER JOIN cargo c5 with (nolock)  ON c5.codCargo = pe4.codCargo    
    
-------------------------------------------------------------------    
---obtiene primer ticket de instalcion de clientes nuevos    
    
SELECT min(rc.idTicket) AS idTicket    
    ,rc.codContratoPlan    
    INTO #tmpPrimeroTicketInstNuevoReq    
FROM requerimiento_consolidado rc    
WHERE rc.desRequerimiento LIKE '%solici%instalaci%'    
AND (rc.motivoInstalacionMasivo='Cliente Nuevo'    
or rc.motivoInstalacionMasivo='Nodos')    
AND rc.desEstado <>'Rechazado Ventas'    
GROUP BY rc.codContratoPlan    
    
    
    
SELECT rc.desEstado    
    ,rc.fechaRegistroRequerimiento    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaRegistroRequerimiento,112),1,6),'') as MesRegistroRequerimiento    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaRegistroRequerimiento,112),7,2),'') as diaRegistroRequerimiento    
    ,rc.fechaAgendamiento AS fechaAgendamiento    
    ,rc.fechaInstalacion AS fechaPrimeraInstalacion    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaInstalacion,112),1,6),'') as MesPrimeraInstalacion    
    ,rc.fechaActivacion AS fechaPrimeraActivacion    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaActivacion,112),1,6),'') as MesPrimeraActivacion    
    ,rc.fechaAgendamiento AS fechaPrimerAgendamiento    
    ,isnull(substring(CONVERT(varchar(8),rc.fechaAgendamiento,112),1,6),'') as MesPrimerAgendamiento    
    ,rc.motivoInstalacionMasivo     
    ,rc.codContratoPlan    
    ,rc.idTicket    
    ,rc.fechaReagendamiento    
    ,rc.fechaFactibilidad    
    ,rc.fechaGestionOperaciones    
    ,rc.fechaGestionCliente    
    ,rc.fechaRechazoCliente    
    INTO #tmpprimeroTicketInstNuevoReqFinal    
FROM requerimiento_consolidado rc    
INNER JOIN #tmpPrimeroTicketInstNuevoReq nt ON nt.idTicket=rc.idTicket    
    
    
    
    
select codContrato_Plan     
, MIN(codrequerimiento_contrato_plan)as codrequerimiento_contrato_plan    
into #instalacionesPrimera     
from requerimiento_contrato_plan rcp    
inner join requerimiento r with (nolock)  on rcp.codRequerimiento=r.codRequerimiento    
inner join estado_requerimiento er with (nolock)  on rcp.codestado = er.codestado    
where (r.desRequerimiento= 'Solicitud Instalacion'    
OR r.desRequerimiento= 'Solicitud Instalacion Corporativa')    
AND er.desEstadoRequerimiento NOT LIKE '%Rechaz%'    
group by codContrato_Plan    
    
    
select rcp.codrequerimiento_contrato_plan, rcp.codContrato_Plan, rcp.fechaRegistro,    
case    
      when rcp.fechaAgendamiento is not null then rcp.fechaAgendamiento    
      else rcp.fechaRequerimiento    
end  as fechaPrimeraInstalacion,    
er.desestadoRequerimiento as EstadoRequerimiento    
,r.desRequerimiento    
,case    
      when rcp.fechaAgendamiento is not null then isnull(substring(CONVERT(varchar(8),rcp.fechaAgendamiento,112),1,6),'')     
      else isnull(substring(CONVERT(varchar(8),rcp.fechaRequerimiento,112),1,6),'')    
end  as mesPrimeraInstalacion    
into #FechaInstalacionesPrimera    
from requerimiento_contrato_plan rcp    
INNER JOIN requerimiento r with (nolock)  ON r.codRequerimiento = rcp.codRequerimiento    
inner join #instalacionesPrimera ins on rcp.codrequerimiento_contrato_plan=ins.codrequerimiento_contrato_plan    
inner join estado_requerimiento er with (nolock)  on rcp.codestado = er.codestado    
AND er.desEstadoRequerimiento NOT LIKE '%Rechaz%'    
    
    
select vcr.codrequerimiento_contrato_plan,vcr.valor as  FechaActivacionTecnica     
into #FechaActivacionTecnica     
from historico_netplus.dbo.valor_caracteristica_requerimiento vcr    
inner join caracteristica_requerimiento cr with (nolock)  on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch with (nolock)  on cr.codcaracteristica=ch.codcaracteristica     
where ch.descaracteristica like 'Fecha Activacion Tecnica'    
    
select vcr.codrequerimiento_contrato_plan,vcr.valor as  FechaActivacionFacturacion     
into #FechaActivacionFacturacion    
from historico_netplus.dbo.valor_caracteristica_requerimiento vcr    
inner join caracteristica_requerimiento cr with (nolock)  on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch with (nolock)  on cr.codcaracteristica=ch.codcaracteristica     
where ch.descaracteristica like 'Fecha Activacion Facturacion'    
    
    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion     
,t.codContrato_Plan, t.fechaRegistro, t.fechaPrimeraInstalacion, t.EstadoRequerimiento    
,t.desRequerimiento ,mesPrimeraInstalacion, fat.FechaActivacionTecnica,faf.FechaActivacionFacturacion    
into #PrimeraInstalacion     
from valor_caracteristica_requerimiento vcr    
inner join caracteristica_requerimiento cr with (nolock)  on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch with (nolock)  on cr.codcaracteristica=ch.codcaracteristica     
inner join valor_posible_requerimiento  vpr with (nolock)  on cr.codCaracteristica_Requerimiento = vpr.codCaracteristica_Requerimiento and  convert(varchar,vcr.valor)=vpr.valorposible    
INNER JOIN #FechaInstalacionesPrimera t ON t.codrequerimiento_contrato_plan=vcr.codRequerimiento_Contrato_Plan    
left outer JOIN #FechaActivacionTecnica fat ON fat.codrequerimiento_contrato_plan=vcr.codRequerimiento_Contrato_Plan    
left outer JOIN #FechaActivacionFacturacion faf ON faf.codrequerimiento_contrato_plan=vcr.codRequerimiento_Contrato_Plan    
where ch.descaracteristica like 'Motivo Instalacion'    
AND (vpr.descripcion='Cliente Nuevo'    
OR vpr.descripcion='Nodos')    
    
    
    
--Metros FO Estimados    
SELECT vcs.codContratoPlan    
    ,vcs.valor AS metrosEstimados    
    INTO #tmpMetrosEstimados    
FROM valor_caracteristica_servicio vcs    
WHERE vcs.codCaracteristica=126    
    
--Metros FO Instalados    
SELECT vcs.codContratoPlan    
    ,vcs.valor AS metrosInstalados    
     INTO #tmpMetrosInstalados    
FROM valor_caracteristica_servicio vcs    
WHERE vcs.codCaracteristica=127    
    
--Usuario FO    
SELECT vcs.codContratoPlan    
    ,vcs.valor AS UsuarioFO    
     INTO #tmpUsuarioFO    
FROM valor_caracteristica_servicio vcs    
WHERE vcs.codCaracteristica=129    
    
    
--account manager    
    
SELECT p.nombres+' '+p.apellidos AS account    
    ,vcs.codContratoPlan    
    INTO #tmpBaseAccountManager    
FROM valor_caracteristica_servicio vcs    
INNER JOIN personal p with (nolock)  ON p.codPersonal=vcs.valor    
WHERE vcs.codCaracteristica=195    
and vcs.valor not like '%[a-z]%'    
------------------------------------------------------------------------------------    
    
    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion into #DetalleMotivo  from valor_caracteristica_requerimiento vcr with (nolock)     
inner join caracteristica_requerimiento cr with (nolock)  on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch with (nolock)  ON  cr.codcaracteristica=ch.codcaracteristica     
inner join valor_posible_requerimiento vpr with (nolock)  on cr.codCaracteristica_Requerimiento = vpr.codCaracteristica_Requerimiento and  convert(varchar,vcr.valor)=vpr.valorposible    
where ch.descaracteristica='Detalle Motivo'    
    
INSERT INTO #DetalleMotivo    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion from historico_netplus.dbo.valor_caracteristica_requerimiento vcr with (nolock)     
INNER JOIN historico_netplus.dbo.requerimiento_contrato_plan rcp with (nolock)  ON vcr.codRequerimiento_Contrato_Plan = rcp.codRequerimiento_Contrato_Plan    
inner join caracteristica_requerimiento cr with (nolock)  on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch with (nolock)  on cr.codcaracteristica=ch.codcaracteristica     
inner join valor_posible_requerimiento vpr with (nolock)  on cr.codCaracteristica_Requerimiento = vpr.codCaracteristica_Requerimiento and  convert(varchar,vcr.valor)=vpr.valorposible    
where ch.descaracteristica='Detalle Motivo'    
AND rcp.fechaRequerimiento>='20170101'    
    
    
    
/*PRIMERA Y ULTIMA FACTURA DE SERVICIO*/    
/*Fecha primera y ultima factura*/    
    
SELECT  a.codContrato_Plan,     
     max(f.codContrato)as codcontrato,      
  MIN(f.fecha) AS fecha_Primera_factura,    
  MAX(f.fecha) AS fecha_Ultima_factura,    
  MIN(f.emision) AS emision_Primera_factura,    
  MAX(f.emision) AS emision_Ultima_factura    
into #tmpPUFAcBase    
FROM    dbo.factura AS f with (nolock)  INNER JOIN    
        dbo.asiento AS a with (nolock)  ON f.codSucursal = a.codSucursal and f.puntoEmision = a.puntoEmision AND f.numFactura = a.numFactura    
        left outer join view_facturas_anuladas fa with (nolock)  on f.codSucursal = fa.codSucursal and f.puntoEmision = fa.puntoEmision AND f.numFactura = fa.numFactura and fa.total>= f.total    
WHERE     ((a.codTipo_Asiento = 'B' or a.codTipo_Asiento = 'V')) and fa.codSucursal is null    
GROUP BY a.codContrato_Plan    
    
    
/*Primera y Ultima factura Final*/     
select p.codcontrato as codcontrato, p.codcontrato_plan,     
p.fecha_primera_Factura as fecha_primera_factura,     
MAX(fp.codSucursal) as codSucursal,    
MAX(fp.codcliente) as codcliente,    
MIN(fp.emision) as emisionPriFac,     
min(fp.numfactura) as priFactura,    
p.fecha_ultima_factura  AS fecha_ultima_factura,    
max(fu.numfactura)as ultFactura    
into #tmpPUFac    
from #tmpPUFAcBase p    
inner join factura fp with (nolock)  ON  p.codcontrato = fp.codContrato and fp.emision=p.emision_primera_factura    
inner join factura fu with (nolock)  on p.codcontrato = fu.codContrato and fu.emision=p.emision_ultima_factura    
group by p.codcontrato_plan, p.codcontrato, p.fecha_primera_Factura, p.fecha_ultima_factura    
    
    
    
/*Primer Asiento*/    
    
SELECT pfa.codSucursal,pfa.codCliente,pfa.codContrato_Plan,pfa.priFactura,    
MIN(pa.valor_Total) as Valor_1ra_Factura,min(f.puntoEmision) as puntoEmision    
into #tmpPA    
FROM #tmpPUFac pfa    
inner join factura f with (nolock)  on pfa.codSucursal =f.codSucursal and pfa.priFactura = f.numFactura and     
  pfa.codcliente = f.codCliente    
INNER JOIN  dbo.asiento pa with (nolock)  ON f.codSucursal = pa.codSucursal     
        AND f.numFactura = pa.numFactura     
        and f.puntoEmision=pa.puntoEmision    
        and pfa.codcontrato_plan = pa.codcontrato_plan     
        and (pa.codTipo_Asiento = 'B' OR pa.codTipo_Asiento = 'V')    
group by pfa.codSucursal,pfa.codCliente,pfa.codContrato_Plan,pfa.priFactura    
     
--116668    
    
/*Ultimo Asiento*/    
    
SELECT ufa.codSucursal,ufa.codCliente,ufa.codContrato_Plan,ufa.ultFactura,    
MAX(pa.valor_Total) as valor_ult_Factura,min(f.puntoEmision) as puntoEmision    
into #tmpUA    
FROM #tmpPUFac ufa    
inner join factura f with (nolock)  on ufa.codSucursal =f.codSucursal and ufa.ultFactura = f.numFactura and     
  ufa.codcliente = f.codCliente    
INNER JOIN  dbo.asiento pa with (nolock)  ON f.codSucursal = pa.codSucursal     
        AND f.numFactura = pa.numFactura     
        and f.puntoEmision=pa.puntoEmision    
        and ufa.codcontrato_plan = pa.codcontrato_plan     
        and (pa.codTipo_Asiento = 'B' OR pa.codTipo_Asiento = 'V')    
group by ufa.codSucursal,ufa.codCliente,ufa.codContrato_Plan,ufa.ultFactura    
    
    
/*Valores pendientes de la primera factura*/    
select pf.codSucursal,pf.codCliente,pf.codContrato_Plan,min(f.servicio) as servicio,    
min(f.por_Pagar) as por_Pagar    
into #tmpVPF    
from #tmpPUFac pf    
inner join dbo.factura f with (nolock)  on  f.codsucursal = pf.codsucursal     
       and f.numFactura= pf.priFactura    
       and f.codCliente=pf.codCliente    
group by pf.codSucursal,pf.codCliente,pf.codContrato_Plan    
    
           
    
/*final*/    
    
select  t.*, pa.Valor_1ra_Factura,ua.valor_ult_Factura,vpf.servicio,vpf.por_Pagar,    
pa.puntoEmision as priPuntoEmision,ua.puntoEmision as ultPuntoEmision    
into #tmpPrevFacturas    
from #tmpPUFac t    
left outer join #tmpPA pa on pa.codSucursal= t.codSucursal    
      and pa.codContrato_Plan=t.codContrato_Plan    
      and pa.codCliente=t.codCliente    
      and pa.priFactura=t.priFactura    
left outer join #tmpUA ua on ua.codSucursal=t.codSucursal    
      and ua.codContrato_Plan=t.codContrato_Plan    
      and ua.codCliente=t.codCliente    
      and ua.ultFactura=t.ultFactura    
left outer join #tmpVPF vpf on vpf.codSucursal=t.codSucursal    
       and vpf.codContrato_Plan=t.codContrato_Plan    
       and vpf.codCliente=t.codCliente    
          
           
/*Valor pagado a la primera factura*/    
select f.codsucursal, f.codcliente, f.puntoEmision,f.numfactura,    
MAX(p.fecha) as fechaPago    
into #tmpPrevPag    
from pago p with (nolock)     
inner join factura f with (nolock)  on p.codSucursal = f.codSucursal and p.codCliente= f.codCliente and    
 p.numFactura = f.numFactura    
group by f.codsucursal, f.codcliente, f.puntoEmision, f.numfactura    
    
select t.codSucursal,t.codContrato_Plan,t.codCliente,t.priFactura,    
MAX(p.fechaPago) as fechaPago     
into #tmpFechaPago    
from #tmpPrevFacturas t    
inner join #tmpPrevPag p on t.codSucursal=p.codSucursal     
     and t.priFactura=p.numFactura    
     and t.codCliente=p.codCliente    
group by t.codSucursal,t.codContrato_Plan, t.codcliente,t.priFactura    
    
    
select codSucursal,codCliente,SUM(por_pagar) as saldo_pendiente     
into #tmp_medios_totales     
from medios_totales mt with (nolock)     
group by codSucursal,codCliente    
    
    
    
    
/*ULTIMO REQUERIMIENTO SUSPENSION*/    
select cp.codContrato_Plan,    
case    
      when (e.suspensoDefinitivo = 1 and cp.codcontrato_plan= ds.codcontrato_plan) then ds.ultimoTicket     
      else dt.ultimoTicket        
end as codRequerimiento_Contrato_Plan    
into #tmpUltReq    
from contrato_plan cp with (nolock)     
inner join estado_csp e with (nolock)  on cp.codestado_csp = e.codestado_csp    
left outer join view_ReqUTDesactivacionServicio ds with (nolock)  on cp.codcontrato_plan = ds.codcontrato_plan    
left outer join view_ReqUTDesactivaciontemporal dt with (nolock)  on cp.codcontrato_plan = dt.codcontrato_plan    
where ( e.suspensoDefinitivo = 1 and cp.codcontrato_plan= ds.codcontrato_plan ) or     
( (e.codestado_csp = 5 or e.codestado_csp = 10) and cp.codcontrato_plan= dt.codcontrato_plan )     
    
    
select dm.*,tu.codContrato_Plan into #MotivoDetalle from  #tmpUltReq tu    
inner join #DetalleMotivo dm on dm.codRequerimiento_Contrato_Plan=tu.codRequerimiento_Contrato_Plan    
    
    
/*LOS DATOS DEL ULTIMO REQUERIMIENTO*/    
select rcp.codContrato_Plan, rcp.codRequerimiento_Contrato_Plan,    
rcp.fechaRegistro,u.nombre_Usuario as usuarioRegistro,    
isnull(ua.nombre_usuario,'') as usuarioAsignado,    
r.codRequerimiento,rcp.fechaRequerimiento,er.desEstadoRequerimiento    
into #tmpPrevReqSusp    
from requerimiento_contrato_plan rcp with (nolock)     
inner join #tmpUltReq trs on trs.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
inner join requerimiento r with (nolock)  on r.codRequerimiento=rcp.codRequerimiento    
inner join usuario u with (nolock)  on u.codUsuario=rcp.codUsuario_Registro    
inner join estado_requerimiento er with (nolock)  on rcp.codEstado =er.codEstado    
left outer join usuario ua on ua.codUsuario=rcp.codUsuario_Asignado    
where (r.desRequerimiento like '%susp%' or r.desrequerimiento like '%Desactiva%')    
    
    
    
/*LA FECHA QUE SE DEBE APLICAR*/    
select vcs.codRequerimiento_Contrato_Plan, convert(varchar(20),vcs.valor) as fechaAplicar ,    
vcs.codCaracteristica_Requerimiento    
into #tmpFecAplicar from #tmpPrevReqSusp rs    
inner join valor_caracteristica_requerimiento vcs with (nolock)  on vcs.codRequerimiento_Contrato_Plan=rs.codrequerimiento_contrato_plan    
inner join caracteristica_requerimiento cr with (nolock)  on rs.codrequerimiento=cr.codRequerimiento and      
            vcs.codCaracteristica_Requerimiento=cr.codCaracteristica_Requerimiento    
inner join caracteristica_helpdesk ch with (nolock)  on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r with (nolock) on cr.codRequerimiento=r.codRequerimiento    
where    
((r.desRequerimiento like '%susp%' or r.desrequerimiento like '%Desactiva%')) and     
ch.descaracteristica like '%fecha%' and ch.descaracteristica like '%aplicar%'    
    
/*MOTIVO DESACTIVACION EN TICKET*/    
select vcs.codRequerimiento_Contrato_Plan, convert(varchar(70),vp.descripcion) as motivo_suspension,    
vcs.codCaracteristica_Requerimiento    
into #tmpMotivoDesac from #tmpPrevReqSusp rs    
inner join valor_caracteristica_requerimiento vcs with (nolock)  on vcs.codRequerimiento_Contrato_Plan=rs.codrequerimiento_contrato_plan    
inner join caracteristica_requerimiento cr with (nolock)  on rs.codrequerimiento=cr.codRequerimiento and      
            vcs.codCaracteristica_Requerimiento=cr.codCaracteristica_Requerimiento    
inner join caracteristica_helpdesk ch with (nolock)  on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r with (nolock)  on cr.codRequerimiento=r.codRequerimiento    
left outer join valor_posible_requerimiento vp with (nolock)  on  vcs.codcaracteristica_requerimiento = vp.codcaracteristica_requerimiento      
                             and vcs.valor like vp.valorposible    
where     
((r.desRequerimiento like '%susp%' or r.desrequerimiento like '%Desactiva%')) and    
ch.descaracteristica like 'motivo%'     
    
    
/*REQUERIMIENTO CON DATOS*/    
select rcp.*, isnull(f.fechaAplicar,'') as fechaAplicar,f.codcaracteristica_requerimiento,    
ISNULL(md.motivo_suspension,'') as motivo_suspension    
into #tmpReqSusp     
from #tmpPrevReqSusp rcp    
left outer join #tmpFecAplicar f on rcp.codrequerimiento_contrato_plan= f.codrequerimiento_contrato_plan    
left outer join #tmpMotivoDesac md on rcp.codrequerimiento_contrato_plan= md.codRequerimiento_Contrato_Plan    
    
    
/*NODO POR CONTRATO_PLAN*/    
SELECT      cp.codContrato_Plan, isnull(n.codNodo,0) as codNodo,    
ISNULL(n.desNodo, '') AS nodo , isnull(n.codigo_nodo,'') as codigo_nodo , isnull(n.codArea,'') as area       
into #tmpNodo    
FROM  dbo.contrato_plan AS cp with (nolock)     
INNER JOIN dbo.servicio AS s with (nolock)  ON cp.codServicio = s.codServicio     
inner join caracteristica_servicio cs with (nolock)  on cp.codPlan = cs.codPlan and cp.codServicio = cs.codServicio and cp.codUltima_Milla=cs.codUltima_Milla    
INNER JOIN dbo.valor_caracteristica_servicio AS v with (nolock)  ON cp.codContrato_Plan = v.codContratoPlan and cs.codCaracteristica = v.codCaracteristica    
left outer join nodo n on v.valor = n.codNodo    
WHERE   (s.aplica_Ultima_Milla = 1) AND     
      (v.codCaracteristica = 53 or v.codCaracteristica = 5)    
    
     
    
    
    
    
/*--MPDIFICADO dceli 20140319 campos nodo y base corpotrativos--*/    
---nodo corporativos    
select 0 as codPadre, codNodo as Codigo,  desNodo+ ' (' +rangoIPBase + ') '+ Ubicacion as Descripcion    
into #tmpNodosCorp    
from nodo with (nolock)  where codEstado>= 0 and codtipoNodo=1    
--base corporativos    
select codNodo as codPadre, codBase as codigo, desbase +' ('+ ipBase +  ')->['+rangoIpClienteDesde+' .. ' + rangoIpClienteHasta + ']' as Descripcion     
into #tmpNodosCorpBase    
from base where estado='A' order by desBase    
    
    
    
/*ACTIVOS A LA FECHA CORPORATIVOS*/    
--drop table #tmpprevio     
select tco.destipo_contrato as Tipo_Contrato, ci.desCiudad as Sucursal,c.codcliente,c.cedula_ruc,    
case     
when co.codTipo_Contrato = 1 and co.codcliente = codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(c.apellidos,'') + ' ' + isnull(c.nombres,'') + ' ' +    
            isnull(c.nombre_empresa,''))),1,244)     
when co.codTipo_Contrato = 1 and co.codcliente <> codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(c.apellidos,'') + ' ' + isnull(c.nombres,'') + ' ' +    
            isnull(c.nombre_empresa,''))),1,115)+' (TERCEROS): '+ substring(ltrim(rtrim(isnull(cl2.nombre_Empresa,'') + ' ' + isnull(cl2.apellidos,'') + ' ' +isnull(cl2.nombres,'') )),1,115)    
when co.codTipo_Contrato = 2 and co.codcliente = codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(c.nombre_empresa,'') + ' ' + isnull(c.apellidos,'') + ' ' + isnull(c.nombres,''))),1,244)    
when co.codTipo_Contrato = 2 and co.codcliente <> codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(c.nombre_empresa,'') + ' ' + isnull(c.apellidos,'') + ' ' + isnull(c.nombres,''))),1,115) +    
      ' (TERCEROS): '+ substring(ltrim(rtrim(isnull(cl2.nombre_Empresa,'') + ' ' + isnull(cl2.apellidos,'') + ' ' +isnull(cl2.nombres,'') )),1,115)     
end as nombres,    
case     
      when co.codcliente = codAsociado_ACliente then substring(c.direccion,1,150)     
      else substring(cl2.direccion,1,150)    
end as direcionFacturacion,     
--case     
--      when co.codcliente = codAsociado_ACliente then     
--            substring(ltrim(rtrim((isnull(c.telefono1,' ') +' '+isnull(c.telefono2,' ')+ ' ' + isnull(c.telefono3,' ') + ' ' + isnull(c.telefono4,' ')+ ' ' + isnull(c.telefono5,' ')+ ' ' + isnull(c.telefono6,' ')))),1,244)      
--      else substring(ltrim(rtrim((isnull(cl2.telefono1,' ') +' '+isnull(cl2.telefono2,' ')+ ' ' + isnull(cl2.telefono3,' ') + ' ' + isnull(cl2.telefono4,' ')+ ' ' + isnull(cl2.telefono5,' ')+ ' ' + isnull(cl2.telefono6,' ')))),1,244)      
--end as telefonoFacturacion,   

--AUMENTO 307 CORP

case     
      when co.codcliente = codAsociado_ACliente then     
     --3campos  
   isnull(c.telefono1,' ')  --,  

   else   
    isnull(cl2.telefono1,' ')-- as Celular  
   end as Celular,   
  
 case     
      when co.codcliente = codAsociado_ACliente then     
   isnull(c.telefono3,' ')   
   else   
  isnull(cl2.telefono3,' ')  
 end as Telefono1,   
 case     
      when co.codcliente = codAsociado_ACliente then     
   isnull(c.telefono5,' ')   
   else   
  isnull(cl2.telefono5,' ')  
 end as Telefono2,   

s.desservicio as Servicio,    
um.desultima_milla as Ultima_Milla,    
ci1.desCiudad as ciudad_Enlace,      
prv.desProvincia as provincia_Enlace,    
cp.codcontrato_plan,     
e.desestado_csp as estado,     
p.desPlan,    
ab.desancho_banda as Ancho_Banda,    
substring(isnull(vt.tecnologia,''),1,50) as tecnologia,    
case    
      when s.abrevia = 'HO' then substring(isnull(nod.nodo,''),1,50)    
      when s.abrevia = 'WI' then substring(isnull(nw.nodo,''),1,50)    
     -- when s.abrevia = 'In' then substring(isnull(tnc.Descripcion,''),1,50)      
      else substring(isnull(tnc.Descripcion,''),1,50)      
end as Nodo,    
case     
 when s.abrevia = 'HO' then substring(isnull(vrw.radio,''),1,100)     
 when s.abrevia = 'WI' then substring(isnull(vrw.radio,''),1,100)    
 --when s.abrevia = 'In' then substring(isnull(tb.Descripcion,''),1,100)     
 else substring(isnull(tb.Descripcion,''),1,100)     
 end    
 as Radio_Base,    
substring(isnull(vip.ipcliente,''),1,20) as ipcliente,    
cp.fecha_inicio as fechaUltimaModificacionFacturar,    
substring(isnull(cp.ubicacion,''),1,150) as ubicacion,    
substring(isnull(cp.direccionOrigen,' '),1,100) as origen,     
substring(isnull(cp.direccionDestino,''),1,100) as destino,    
substring(isnull(nc.descripcion,' '),1,50) as nivel_Comparticion,    
substring(isnull(cp.nombrescontacto,' '),1,150) as nombres_Contacto,    
substring(isnull(cp.telefonoscontacto,' '),1,150) as telefonos_Contacto,    
substring(isnull(cp.emailcontacto,' '),1,100) as mail_Contacto,    
case    
      when facturar_desde_tarifario =0 then cp.valor_Instalacion    
      when facturar_desde_tarifario <>0 then t.costo_instalacion    
end as valor_InstalacionActual,     
case    
      when facturar_desde_tarifario =0 then cp.valor_Servicio    
      when facturar_desde_tarifario <>0 then t.tarifa_basica    
end as valor_ServicioActual,    
case    
   when facturar_desde_tarifario = 0 and p.mesesPorFactura  > 1 then Convert(money, convert(decimal(15,2), cp.valor_Servicio/p.mesesPorFactura, 0))    
   when facturar_desde_tarifario <>0 and p.mesesPorFactura  > 1 then Convert(money, convert(decimal(15,2), t.tarifa_basica/p.mesesPorFactura, 0))    
   when facturar_desde_tarifario = 0 and p.mesesPorFactura <= 1 then Convert(money, convert(decimal(15,2), cp.valor_Servicio, 0))    
   when facturar_desde_tarifario <>0 and p.mesesPorFactura <= 1 then Convert(money, convert(decimal(15,2), t.tarifa_basica, 0))    
end as valor_mensualizado,    
p.mesesPorFactura,    
case    
      when facturar_desde_tarifario =0 then cp.valor_Ultima_Milla    
      when facturar_desde_tarifario <>0 then 0    
end as valor_Ultima_MillaActual,     
case    
      when facturar_desde_tarifario =0 then cp.valor_Renta_Equipo    
      when facturar_desde_tarifario <>0 then t.costo_Renta_Equipo    
end as valor_Renta_EquipoActual,     
ta.destipo_actividad,    
ltrim(rtrim(isnull(ve.apellidos_vendedor,'') + ' ' + isnull(ve.nombres_vendedor,''))) as nombres_vendedor,    
substring(isnull(pv.identificacion,''),1,20) as cedula_vendedor,    
tpp.destipo_periodo_pago as Periodo_Pago,    
fp.desforma_pago as Forma_Pago,    
cp.codSucursal,    
ec.descripcion as calificacion,    
case    
      when e.facturable=1 then 'SI'    
      else 'NO'    
end as      ACTIVO,    
case    
      when e.facturable=1 then ''    
      else convert(varchar(8),isnull(cp.fecha_fin,0),112)    
end as      fecha_desactivacion,    
case    
      when rs.codcontrato_plan is not null and isdate(replace(rs.fechaAplicar,'/',''))>0 then substring(isnull(convert(varchar(8),replace(rs.fechaAplicar,'/',''),112),''),1,8)--convert(datetime,replace(rs.fechaAplicar,'/',''))    
      else substring(isnull(convert(varchar(8),cp.fecha_fin,112),''),1,8)    
end as fechaAplicar,    
case    
      when rs.codcontrato_plan is not null and isdate(replace(rs.fechaAplicar,'/',''))>0 then substring(isnull(convert(varchar(6),replace(rs.fechaAplicar,'/',''),112),''),1,8)    
      else substring(isnull(convert(varchar(6),cp.fecha_fin,112),''),1,8)    
end as mesAplicar,    gc.Descripcion as grupo_comercial,    
pr.apellidos +' '+pr.nombres as ejecutivoPostVenta    
,ab.subida    
,isnull(prc.apellidos+' '+prc.nombres,'') as ejecutivoCobranzas    
,u.nombre_Usuario    
into #tmpprevio     
from contrato_plan cp with (nolock)     
inner join contrato co with (nolock)  on cp.codcontrato = co.codcontrato     
inner join tipo_contrato tco with (nolock) on co.codtipo_contrato = tco.codtipo_contrato    
inner join cliente c with (nolock) on cp.codsucursal = c.codciudad_sucursal and    
            cp.codcliente = c.codcliente    
inner join ciudad ci with (nolock) on cp.codsucursal = ci.codciudad    
inner join ciudad ci1 with (nolock) on cp.codciudad = ci1.codciudad    
inner join provincia prv with (nolock) on prv.codprovincia=ci1.codProvincia    
inner join servicio s with (nolock) on cp.codservicio = s.codservicio    
inner join planes p with (nolock) on cp.codplan = p.codplan    
inner join ancho_banda ab with (nolock) on ab.codancho_banda = cp.codancho_banda    
inner join ultima_milla um with (nolock) on cp.codultima_milla = um.codultima_milla    
inner join estado_csp e with (nolock) on cp.codestado_csp = e.codestado_csp    
inner join tipo_periodo_pago tpp with (nolock) on co.codTipo_Periodo_Pago = tpp.codtipo_periodo_pago    
LEFT outer join valor_caracteristica_servicio vcs with (nolock) on vcs.codContratoPlan=cp.codContrato_Plan and codCaracteristica=89    
LEFT outer join #tmpNodosCorp tnc on tnc.Codigo=vcs.valor    
left outer join #tmpNodosCorpBase tb on tb.codigo=vcs.valor    
left outer join #tmpReqSusp rs on rs.codcontrato_plan=cp.codContrato_Plan    
left outer join contrato_forma_pago cfp with (nolock) on cfp.codContrato = co.codContrato    
left outer join forma_pago fp with (nolock) on cfp.codForma_Pago = fp.codforma_pago    
left outer join cliente cl2 with (nolock) on co.codSucursal = cl2.codCiudad_Sucursal and co.codAsociado_ACliente = cl2.codCliente    
left outer join tipo_actividad ta with (nolock) on ta.codtipo_actividad = c.codtipo_actividad     
left outer join estados_cliente ec with (nolock) on ec.nombre_campo ='calificacion' and ec.codigo = c.calificacion    
left outer join tarifario t with (nolock) on cp.codservicio = t.codservicio and      cp.codplan = t.codplan and    
            cp.codancho_banda = t.codancho_banda    
left outer join estados_cliente nc with (nolock) on nc.nombre_campo ='codNivelComparticion' and convert(int,nc.codigo) = cp.codNivelComparticion    
left outer join vendedor ve with (nolock) on cp.codvendedor = ve.codvendedor    
left outer join personal pv with (nolock) on pv.codPersonal=ve.codPersonal    
left outer join view_tecnologia vt with (nolock) on cp.codContrato_Plan = vt.codcontrato_plan       
left outer join view_nodo nod with (nolock) on cp.codcontrato_plan = nod.codcontrato_plan    
left outer join view_nodoWifi nw with (nolock) on cp.codcontrato_plan = nw.codcontrato_plan    
left outer join view_radioWifi vrw with (nolock) on cp.codContrato_Plan = vrw.codContrato_Plan    
left outer join view_ipcliente vip with (nolock) on cp.codContrato_Plan = vip.codcontrato_plan    
left outer join grupo_comercial gc with (nolock) on gc.codGrupo=c.codGrupoComercial    
left outer join personal pr with (nolock) on pr.codPersonal = cp.codEjecutivoPosventa    
left outer join personal prc with (nolock) on prc.codPersonal = co.codejecutivo and prc.estado='A'    
left outer join usuario u with (nolock) on u.codUsuario = c.codUsuario    
where     
(desservicio like '%dial% up%'  or     
desservicio like '%adsl%'  or     
desservicio like '%wifi%'  or     
desservicio like '%fibra%'  or     
desservicio like '%internet%' or     
desservicio like '%canal%conexion%' or    
desservicio like '%vsat%' or    
desservicio like '%Punto Cam%' or    
desservicio like '%adicional%' or    
desservicio like '%CLOUD%' or    
desservicio like '%Colocation%' OR    
desservicio  ='Mantenimiento' OR    
desservicio  ='Seguridad Gestionada' OR    
desservicio  ='Dominio' OR    
desservicio  ='Web Hosting' OR    
desservicio ='Otros' or    
(desservicio  ='Servicios Varios' AND p.desPlan='Servicio Firewall')    
) and     
p.esInfraestructura<>1 and     
--(e.facturable=1) and    
co.codtipo_contrato=2    
--order by s.codsucursal, cp.codcliente, co.codAsociado_ACliente,s.desservicio,p.desplan    
    
    
select max(codperiodo_comision) as codperiodo_comision ,codsucursal,codcontrato_plan into #ComisionesC    
from detalle_comision_corporativa dcc with (nolock)     
group by codcontrato_plan, codsucursal    
    
    
select dcc.codcontrato_plan,max(dcc.codperiodo_comision) as codperiodo_comision,    
max(dcc.codsucursal) as codsucursal,max(dcc.tipo_1) as tipo,max(dcc.tipo_2) as tipo2,max(dcc.valor_upgrade) as valor_upgrade,    
MAX(dcc.valor_comisionado) as valor_comisionado    
into #ComisionesCorporativas    
from #ComisionesC c    
inner join detalle_comision_corporativa dcc with (nolock) on c.codperiodo_comision=dcc.codperiodo_comision and     
c.codcontrato_plan=dcc.codcontrato_plan    
group by dcc.codContrato_Plan    
    
    
/*FACTURA DE LA INSTALACION(PRIMERA FACTURA-INSTALACION)*/    
--drop table #tmpFacInstalacion    
SELECT c.codCiudad_Sucursal as codSucursal,cp.codContrato_Plan,c.codCliente,    
MIN(f.fecha) AS fec_pri_factura    
INTO #tmpFacInstalacion    
FROM dbo.factura  f with (nolock)     
INNER JOIN  dbo.asiento  a with (nolock) ON f.codSucursal = a.codSucursal     
 AND f.numFactura = a.numFactura    
 and f.puntoEmision=a.puntoEmision    
inner join cliente c with (nolock) on c.codCliente=f.codCliente    
 and c.codCiudad_Sucursal=f.codSucursal    
inner join contrato_plan cp with (nolock)  on a.codContrato_Plan =cp.codContrato_Plan    
left outer join view_facturas_anuladas fa with (nolock) on f.codSucursal = fa.codSucursal and f.puntoEmision = fa.puntoEmision AND f.numFactura = fa.numFactura and fa.total>= f.total    
WHERE (a.codTipo_Asiento='I' or a.codTipo_Asiento='J') and fa.codSucursal is null    
GROUP BY c.codCiudad_Sucursal,cp.codContrato_Plan,c.codCliente    
      
    
/* para activos corporativos-- vizualiza estadoReq*/    
select codContrato_Plan, MAX(rcp.codRequerimiento_Contrato_Plan)  as codRequerimiento_Contrato_Plan,    
MAX(desEstadoRequerimiento) as estadoReqSolicitudInstalacion    
into #tmpReqSI     
from requerimiento_contrato_plan rcp with (nolock)     
inner join requerimiento r with (nolock) on r.codRequerimiento=rcp.codRequerimiento    
inner join estado_requerimiento er with (nolock) on er.codEstado=rcp.codEstado    
where desRequerimiento like '%Solicitud Instalacion%'    
group by codContrato_Plan    
 union     
SELECT rc.codContratoPlan as codContrato_Plan, max(rc.idTicket) as codRequerimiento_Contrato_Plan, MAX(rc.desEstado) as estadoReqSolicitudInstalacion    
FROM requerimiento_consolidado rc with (nolock)     
WHERE rc.desRequerimiento LIKE '%solici%instalaci%'    
GROUP BY rc.codContratoPlan    
    
    
    
/* Conocer el usuario que migró  Corporativo*/    
SELECT *     
into #tmpMigradoUser    
FROM valor_caracteristica_servicio vcs with (nolock)     
WHERE vcs.codCaracteristica=179    
    
/* Conocer si esta migrado Corporativo*/    
SELECT vcs.codContratoPlan    
 ,vpc.descripcion     
 into #tmpMigradoProceso    
FROM valor_caracteristica_servicio vcs with (nolock)     
INNER JOIN valor_posible_caracteristica vpc with (nolock) on  vcs.codCaracteristica = vpc.codCaracteristica    
AND vpc.valorPosible=vcs.valor    
WHERE vcs.codCaracteristica=238    
    
IF EXISTS (SELECT * FROM consolidado.sys.objects WHERE object_id = OBJECT_ID(N'[consolidado].[dbo].[tmp_Calidad_ActivosCorporativos]') AND type in (N'U'))    
      drop table consolidado.dbo.tmp_Calidad_ActivosCorporativos    
    
select p.*,isnull(v.fecha_primera_factura,'') as Fecha_1ra_Factura,     
convert(money,isnull(v.Valor_1ra_Factura,0)) as Valor_1ra_Factura,    
isnull(v.fecha_ultima_factura,'') as Fecha_Ult_Factura,     
convert(money,isnull(v.Valor_Ult_Factura,0)) as  Valor_Ult_Factura,    
case    
      when v.por_pagar is not null and v.por_pagar<= round(isnull(v.servicio,0)*0.07,2) then 'Si'     
      else 'No'    
end as Pagada_1ra_Factura,    
cp.fecha_Registro as FechaIngresoSistema,    
substring(CONVERT(varchar(8),cp.fecha_Registro,112),1,6) as MesIngresoSistema,    
substring(convert(varchar(244),cp.comentario),1,245) as comentario,    
case    
      when dcc.codcontrato_plan is not null then 'S'     
      else ''    
      end as comisionado,    
case    
      when dcc.codcontrato_plan is not null then substring(convert(varchar(6),pcc.fechaGeneracion,112),1,6)    
      else ''    
      end as mesComisionado,    
isnull(dcc.tipo,'') as tipoComision,    
isnull(dcc.tipo2,'') as subtipoComision,    
convert(money,isnull(dcc.valor_comisionado,0)) as valor_comisionado,    
fechaP.fechaPago as fechaPago,    
case  when cp.codCliente_Original <> p.codCliente then cp.codCliente_Original else 0 end as codClienteAnt,     
case  when cp.codContrato_PlanOriginal<>p.codContrato_Plan then cp.codContrato_PlanOriginal else 0 end as codContratoPlanAnt,    
convert(varchar(6),fechaP.fechaPago,112) as mesUltimoPago,    
case when mt.saldo_pendiente is not null then convert(money,mt.saldo_pendiente)     
 else convert(money,0) end as saldo_pendiente,    
convert(money,dcc.valor_upgrade) as valor_upg,    
isnull(substring(convert(varchar(8),fi.fec_pri_factura,112),1,6),' ') as MesFacInst,    
SUBSTRING(isnull(vp.piloto,''), 1, 15)  as piloto,    
SUBSTRING(isnull(vnm.numeroAMultiplexar,''), 1, 15) as numeroAMultiplexar,    
et.EjecutivoTecnico,rsi.estadoReqSolicitudInstalacion    
,isnull(substring(CONVERT(varchar(8),tpp.fechaPrimeraInstalacion,112),1,8),v.fecha_primera_factura) as fechaPrimeraInstalacion    
,isnull(tpp.MesPrimeraInstalacion,v.fecha_primera_factura) as mesPrimeraInstalacion    
,convert(varchar(25),isnull(lon.longitud,'')) as longitud    
,convert(varchar(25),isnull(lat.latitud,'')) as latitud     
--,convert(varchar(25),isnull(tpp.FechaActivacionTecnica,'')) as FechaActivacionTecnica    
,isnull(cp.fecha_Inicio,'') as  FechaActivacionFacturacion    
,isnull(tma.account,'') AS accountManager,    
vcpcn.des_linea_negocio as des_linea_negocio,    
vcpcn.des_producto_oracle as des_producto_oracle,    
vcpcn.des_geografia_oracle as des_geografia_oracle,    
case when cp.cobrarInstalacion=0 then 'NO' else 'SI' end as cobrarInstalacion,    
tu.valor AS migradoUsuario,    
tp.descripcion AS migrado,    
CASE     
  WHEN cpp.codPersonalPreventa=0 THEN 'No Aplica'    
  WHEN cpp.codPersonalPreventa=-1 THEN 'Sin Asignar'    
  WHEN p3.codPersonal IS NOT NULL THEN p3.nombres + ' ' +p3.apellidos    
  ELSE ''    
 END AS preventa,    
 op.codOportunidad,    
 op.nombreOportunidad ,

 isnull(dir.barrioSector,'')+' '+isnull(dir.callePrincipal,'')+' '  
 +isnull(dir.numeracion,'')+' '+isnull(dir.calleSecundaria,'')+' '+isnull(dir.Urbanizacion,'')+' '  
 +isnull(dir.referencia,'')  
   
 AS Direccion,  
 CASE     
  WHEN dir.fActualizacion IS NOT NULL  THEN 'SI'   
  ELSE 'NO'    
 END AS EstadoActualizacionCliente  
    
 
--, convert(varchar(25),isnull(cp.fecha_Inicio,'')) as Fecha_aplicado_netplus    
into consolidado.dbo.tmp_Calidad_ActivosCorporativos    
from #tmpprevio p    
inner join contrato_plan cp with (nolock) on cp.codContrato_Plan=p.codcontrato_plan    
LEFT outer join #tmpFacInstalacion fi on fi.codContrato_Plan=cp.codContrato_Plan     
          and fi.codSucursal=cp.codSucursal    
left outer join #tmpPrevFacturas v on p.codcontrato_plan = v.codContrato_Plan    
          and p.codSucursal = v.codSucursal    
left outer join #ComisionesCorporativas dcc on dcc.codcontrato_plan=cp.codcontrato_plan    
left outer join periodo_comision_corporativa pcc with (nolock) on pcc.codsucursal=dcc.codsucursal and pcc.codperiodo_comision=dcc.codperiodo_comision     
left outer join #tmpFechaPago fechaP on p.codContrato_Plan=fechaP.codContrato_Plan    
left outer join #tmp_medios_totales mt on p.codSucursal=mt.codSucursal and p.codCliente= mt.codCliente     
left outer join view_piloto vp with (nolock) on  vp.codcontrato_plan=cp.codContrato_Plan    
left outer join view_NumeroMultiplexar vnm with (nolock) on vnm.codcontrato_plan=cp.codContrato_Plan    
left outer join view_EjecutivoTecnico et with (nolock) on et.codContrato_Plan=cp.codContrato_Plan    
left outer join #tmpReqSI rsi on rsi.codContrato_Plan=cp.codContrato_Plan    
LEFT OUTER JOIN #PrimeraInstalacion tpp ON tpp.codContrato_Plan=cp.codContrato_Plan    
left outer join view_longitud lon with (nolock) on p.codContrato_Plan = lon.codContrato_Plan    
left outer join view_latitud lat with (nolock) on p.codContrato_Plan = lat.codContrato_Plan    
LEFT OUTER JOIN #tmpBaseAccountManager tma ON tma.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN view_contratoPlan_costeo_netplus  vcpcn with (nolock) on vcpcn.codContrato_Plan=cp.codContrato_Plan    
LEFT OUTER JOIN #tmpMigradoUser tu ON tu.codContratoPlan=cp.codcontrato_plan    
LEFT OUTER JOIN #tmpMigradoProceso tp ON tp.codContratoPlan=cp.codcontrato_plan    
LEFT OUTER JOIN contrato_plan_personal_preventa cpp with (nolock) ON cpp.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN personal_preventa pp ON pp.id = cpp.codPersonalPreventa    
LEFT OUTER JOIN personal p3 ON p3.codPersonal=pp.codPersonal    
LEFT OUTER JOIN oportunidad op on op.codOportunidad=cp.idOportunidad    

left outer join direccion dir with (nolock) on p.codSucursal = dir.codCiudadSucursal and p.codcliente = dir.codcliente    
--LEFT OUTER JOIN consolidado.dbo.fecha_activacion_corporativa fan on fan.codContratoPlan = cp.codcontrato_plan    
order by p.codSucursal, p.codcliente    
    
    
/*ACTIVOS A LA FECHA MASIVOS*/    
select tco.destipo_contrato as Tipo_Contrato, ci.desCiudad as Sucursal,c.codcliente,    
convert(varchar(20),ltrim(rtrim(replace(c.cedula_ruc,char(9),'')))) as cedula_ruc,    
case      
when co.codTipo_Contrato = 1 and co.codcliente = codAsociado_ACliente then    
     substring(ltrim(rtrim(isnull(replace(c.apellidos,char(9),''),'') + ' ' + isnull(c.nombres,'') + ' ' +    
            isnull(c.nombre_empresa,''))),1,244)     
when co.codTipo_Contrato = 1 and co.codcliente <> codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(replace(c.apellidos,CHAR(9),''),'') + ' ' + isnull(c.nombres,'') + ' ' +    
            isnull(c.nombre_empresa,''))),1,115)+' (TERCEROS): '+ substring(ltrim(rtrim(isnull(cl2.nombre_Empresa,'') + ' ' + isnull(cl2.apellidos,'') + ' ' +isnull(cl2.nombres,'') )),1,115)    
when co.codTipo_Contrato = 2 and co.codcliente = codAsociado_ACliente then    
     substring(ltrim(rtrim(isnull( replace(c.nombre_empresa,CHAR(9),''),'') + ' ' + isnull(replace(c.apellidos,CHAR(9),''),'') + ' ' + isnull(replace(c.nombres,CHAR(9),''),''))),1,244)    
when co.codTipo_Contrato = 2 and co.codcliente <> codAsociado_ACliente then    
      substring(ltrim(rtrim(isnull(replace(c.nombre_empresa,char(9),''),'') + ' ' + isnull(c.apellidos,'') + ' ' + isnull(c.nombres,''))),1,115) +    
      ' (TERCEROS): '+ substring(ltrim(rtrim(isnull(replace(cl2.nombre_Empresa,CHAR(9),'') ,'') + ' ' + isnull(cl2.apellidos,'') + ' ' +isnull(cl2.nombres,'') )),1,115)    
end as nombres,    
case     
      when co.codcliente = codAsociado_ACliente then substring(replace(c.direccion,char(59),''),1,150)     
      else substring(replace(cl2.direccion,char(59),''),1,150)    
end as direcionFacturacion,     
  
--case     
--      when co.codcliente = codAsociado_ACliente then     
--            substring(ltrim(rtrim((isnull(c.telefono1,' ') +' '+isnull(c.telefono2,' ')+ ' ' + isnull(c.telefono3,' ') + ' ' + isnull(c.telefono4,' ')+ ' ' + isnull(c.telefono5,' ')+ ' ' + isnull(c.telefono6,' ')))),1,244)      
--      else substring(ltrim(rtrim((isnull(cl2.telefono1,' ') +' '+isnull(cl2.telefono2,' ')+ ' ' + isnull(cl2.telefono3,' ') + ' ' + isnull(cl2.telefono4,' ')+ ' ' + isnull(cl2.telefono5,' ')+ ' ' + isnull(cl2.telefono6,' ')))),1,244)      
--end as telefonoFacturacion,    
  
case     
      when co.codcliente = codAsociado_ACliente then     
            --substring(ltrim(rtrim((isnull(c.telefono1,' ') +' '+isnull(c.telefono2,' ')+ ' ' + isnull(c.telefono3,' ') + ' ' + isnull(c.telefono4,' ')+ ' ' + isnull(c.telefono5,' ')+ ' ' + isnull(c.telefono6,' ')))),1,244)      
     --3campos  
   isnull(c.telefono1,' ')  --,  
  --isnull(c.telefono2,' ') as Telefono1,  
  --isnull(c.telefono2,' ') as Telefono2  
   else   
  --substring(ltrim(rtrim((isnull(cl2.telefono1,' ') +' '+isnull(cl2.telefono2,' ')+ ' ' + isnull(cl2.telefono3,' ') + ' ' + isnull(cl2.telefono4,' ')+ ' ' + isnull(cl2.telefono5,' ')+ ' ' + isnull(cl2.telefono6,' ')))),1,244)      
    isnull(cl2.telefono1,' ')-- as Celular  
   end as Celular,   
  
 case     
      when co.codcliente = codAsociado_ACliente then     
   isnull(c.telefono2,' ')   
   else   
  isnull(cl2.telefono2,' ')  
 end as Telefono1,   
 case     
      when co.codcliente = codAsociado_ACliente then     
   isnull(c.telefono2,' ')   
   else   
  isnull(cl2.telefono3,' ')  
 end as Telefono2,   
  
  
s.desservicio as Servicio,ltrim(rtrim(um.desultima_milla)) as Ultima_Milla,    
ci1.desCiudad as ciudad_Enlace, prv.desProvincia as provincia_Enlace, cp.codcontrato_plan, e.desestado_csp as estado    
    
,CASE    
 WHEN p.desPlan='CELERITY 2019 BASICO' THEN 'CELERITY 2019 BASICO MIGRACION'    
 ELSE p.desPlan    
END AS desPlan    
    
,ab.desancho_banda as Ancho_Banda,    
substring(isnull(vt.tecnologia,''),1,50) as tecnologia,    
case    
      when s.abrevia = 'HO' then substring(isnull(nod.nodo,''),1,50)    
      when s.abrevia = 'WI' then substring(isnull(nw.nodo,''),1,50)     
      when s.abrevia = 'FH' then substring(ISNULL(ol.ipOLT,''),1,100)    
      else ''    
end as Nodo,    
substring(isnull(vrw.radio,''),1,100) as Radio,    
substring(isnull(vip.ipcliente,''),1,20) as ipcliente,    
cp.fecha_inicio as Activacion,    
substring(isnull(replace(cp.ubicacion,char(59),''),''),1,150) as ubicacion,    
case    
      when facturar_desde_tarifario =0 then cp.valor_Ultima_Milla    
      when facturar_desde_tarifario <>0 then 0    
end as valor_Ultima_Milla,     
    
/*    
case    
      when facturar_desde_tarifario =0 then cp.valor_Instalacion    
      when facturar_desde_tarifario <>0 then t.costo_instalacion    
end as valor_Instalacion,     
*/    
case      
  when cp.facturar_Desde_Tarifario = 1 and vdia.codContratoPlan IS null then isnull(costo_Instalacion,0)    
  when cp.facturar_Desde_Tarifario = 1 and vdia.codContratoPlan IS NOT null then isnull(vdia.valorUnitario,0)-((isnull(vdia.valorUnitario,0)*isnull(vdia.porcentajeDescInstalacion,0))/100)    
  else cp.valor_Instalacion    
 end as valor_Instalacion,    
    
case    
      when facturar_desde_tarifario =0 then cp.valor_Servicio    
      when facturar_desde_tarifario <>0 then t.tarifa_basica    
end as valor_Servicio,    
case    
      when facturar_desde_tarifario =0 then cp.valor_Renta_Equipo    
      when facturar_desde_tarifario <>0 then t.costo_Renta_Equipo    
end as valor_Renta_Equipo,     
substring(c.correoFacturacionElectronica+' '+c.correoFacturacionElectronica2+' '+c.correoFacturacionElectronica3,1,50) as email,    
cp.codSucursal,    
isnull(cv.abreviatura,'')+'-'+isnull(reg.abreviatura,'')+'-'+ltrim(rtrim(isnull(ve.apellidos_vendedor,'')+' '+ isnull(ve.nombres_vendedor,''))) as nombres_vendedor,    
isnull(pv.identificacion,'') as cedula_vendedor,    
tpp.destipo_periodo_pago as Periodo_Pago,    
fp.desforma_pago as Forma_Pago,    
cp.fecha_Registro as fechaVenta,    
ur.nombre_Usuario as usuarioRegistroVenta,    
substring(CONVERT(varchar(8),cp.fecha_Registro,112),1,6) as MesVenta,    
substring(CONVERT(varchar(8),cp.fecha_Registro,112),7,2) as diaVenta,    
nw.codigo_nodo,isnull(nodo.codArea,'') as area,    
        isnull(ciud.desciudad,'') as desCanton,    
--      isnull(prov.desprovincia,'') as desProvincia,    
    
  case when isnull(prov.desprovincia,'') <> '' then     
   isnull(prov.desprovincia,'')     
  when s.desservicio='Wifi' and isnull(prov.desprovincia,'') ='' then    
   pr.desprovincia       
  else ''    
  end as desProvincia,    
    
      substring(isnull(parr.desParroquia,''),1,150) as desParroquia,  c.segmentacion,    
      mc.antiguedadMeses as permanencia,    
      mc.comportamientoCliente    
      ,c.cedula_Representante    
      ,ab.subida    
      ,isnull(ec2.descripcion,' ') AS nivelComparticion    
   ,c.riesgo, c.valuePontencial, c.bancarizado, c.tiene_tarjeta    
into #tmpprevioM     
from contrato_plan cp with (nolock)     
inner join contrato co with (nolock) on cp.codcontrato = co.codcontrato     
inner join tipo_contrato tco with (nolock) on co.codtipo_contrato = tco.codtipo_contrato    
inner join cliente c with (nolock) on cp.codsucursal = c.codciudad_sucursal and    
            cp.codcliente = c.codcliente    
inner join ciudad ci with (nolock) on cp.codsucursal = ci.codciudad    
inner join ciudad ci1 on cp.codciudad = ci1.codciudad    
inner join provincia prv with (nolock) on prv.codprovincia=ci1.codProvincia    
inner join servicio s with (nolock) on cp.codservicio = s.codservicio    
inner join planes p with (nolock) on cp.codplan = p.codplan    
inner join ancho_banda ab with (nolock) on ab.codancho_banda = cp.codancho_banda    
inner join ultima_milla um with (nolock) on cp.codultima_milla = um.codultima_milla    
inner join estado_csp e with (nolock) on cp.codestado_csp = e.codestado_csp    
inner join tipo_periodo_pago tpp with (nolock) on co.codTipo_Periodo_Pago = tpp.codtipo_periodo_pago    
left outer join contrato_forma_pago cfp with (nolock) on cfp.codContrato = co.codContrato    
left outer join forma_pago fp with (nolock) on cfp.codForma_Pago = fp.codforma_pago    
left outer join cliente cl2 with (nolock) on co.codSucursal = cl2.codCiudad_Sucursal and co.codAsociado_ACliente = cl2.codCliente    
left outer join tipo_actividad ta with (nolock) on ta.codtipo_actividad = c.codtipo_actividad     
left outer join estados_cliente ec with (nolock) on ec.nombre_campo ='calificacion' and ec.codigo = c.calificacion    
left outer join estados_cliente ec2 with (nolock) on ec2.codigo=cp.codNivelComparticion and ec2.nombre_campo = 'codNivelComparticion'    
left outer join view_nodo nod with (nolock) on cp.codcontrato_plan = nod.codcontrato_plan    
left outer join view_nodoWifi nw with (nolock) on cp.codcontrato_plan = nw.codcontrato_plan    
left outer join vendedor ve with (nolock) on cp.codvendedor = ve.codvendedor    
left outer join personal pv with (nolock) on pv.codPersonal=ve.codPersonal    
left outer join regional reg with (nolock) on reg.codRegional= ve.codRegional    
left outer join ciudad cv with (nolock) on cv.codCiudad=pv.codCiudad    
left outer join tarifario t with (nolock) on cp.codservicio = t.codservicio and      cp.codplan = t.codplan and    
            cp.codancho_banda = t.codancho_banda               
left outer join view_tecnologia vt with (nolock) on cp.codContrato_Plan = vt.codcontrato_plan             
left outer join view_ipcliente vip with (nolock) on cp.codContrato_Plan = vip.codcontrato_plan    
left outer join usuario ur with (nolock) on cp.codusuario_registro = ur.codusuario    
left outer join provincia prov with (nolock) on convert(int,substring(nw.codigo_nodo,1,2)) = convert(int,prov.codprovincia)    
left outer join ciudad ciud with (nolock) on convert(int,substring(nw.codigo_nodo,3,2)) = ciud.codCiudad     
left outer join parroquia parr with (nolock) on convert(int,substring(nw.codigo_nodo,1,6)) =(substring(parr.codParroquia,2,2)+ substring(parr.codParroquia,5,2)+substring(parr.codParroquia,8,2))    
left outer join nodo with (nolock) on nw.codNodo = nodo.codNodo     
left outer join view_radioWifi vrw with (nolock) on cp.codContrato_Plan = vrw.codContrato_Plan    
left outer join view_ipOLT ol with (nolock) on cp.codContrato_Plan = ol.codContrato_Plan    
left outer join provincia pr with (nolock) on pr.codprovincia =  cp.codprovincia    
left outer join tmp_modelo_cobro_ciclo mc with (nolock) on mc.codSucursal=c.codCiudad_Sucursal and mc.codCliente=c.codcliente    
LEFT OUTER JOIN view_descuentos_instalacion_adicional vdia with (nolock) ON vdia.codContratoPlan=cp.codContrato_Plan    
where     
      co.codTipo_Contrato = 1 and     
(abrevia = 'HO' or abrevia = 'VS' or     
abrevia = 'IN' or abrevia = 'CA' or     
abrevia = 'FH' or abrevia = 'Wi'    
)     
and isnull(p.esInfraestructura,0)<>1    
--and (e.facturable=1)    
--order by s.codsucursal, s.desservicio,p.desplan, cp.codcliente    
    
    
select codContrato_Plan , MAX(codrequerimiento_contrato_plan)as codrequerimiento_contrato_plan    
into #instalaciones from requerimiento_contrato_plan rcp with (nolock)     
inner join requerimiento r with (nolock) on rcp.codRequerimiento=r.codRequerimiento    
where r.desRequerimiento= 'Solicitud Instalacion'    
group by codContrato_Plan    
    
    
select rcp.codrequerimiento_contrato_plan, rcp.codContrato_Plan, rcp.fechaRegistro,    
case    
      when rcp.fechaAgendamiento is not null then rcp.fechaAgendamiento    
      else rcp.fechaRequerimiento    
end  as fechaAgendamiento,    
er.desestadoRequerimiento as EstadoRequerimiento    
into #FechaInstalaciones    
from requerimiento_contrato_plan rcp with (nolock)     
inner join #instalaciones ins on rcp.codrequerimiento_contrato_plan=ins.codrequerimiento_contrato_plan    
inner join estado_requerimiento er with (nolock) on rcp.codestado = er.codestado    
    
/*     
AGREGA COLUMNA INSTALADOR Y TIPO INSTALACION    
REALIZADO POR PAUL RAMON    
INICIO*/    
    
select rcp.codrequerimiento_contrato_plan, rcp.codContrato_Plan, rcp.codInstalador,    
isnull(u.nombre_Usuario,' ') as Instalador    
into #instalador    
from requerimiento_contrato_plan rcp with (nolock)     
inner join #instalaciones ins on rcp.codrequerimiento_contrato_plan=ins.codrequerimiento_contrato_plan    
inner join estado_requerimiento er with (nolock) on rcp.codestado = er.codestado    
LEFT OUTER JOIN usuario u with (nolock) ON u.codUsuario=rcp.codInstalador    
    
select rcp.codrequerimiento_contrato_plan, isnull(vp.descripcion,'') as valor    
into #tmpMotInstalacion    
from requerimiento_contrato_plan rcp with (nolock)     
inner join requerimiento r with (nolock) on rcp.codrequerimiento = r.codrequerimiento    
inner join caracteristica_requerimiento cr with (nolock) on r.codrequerimiento = cr.codrequerimiento    
inner join caracteristica_helpdesk ch with (nolock) on cr.codcaracteristica = ch.codcaracteristica    
inner join valor_caracteristica_requerimiento  v with (nolock) on rcp.codrequerimiento_contrato_plan = v.codrequerimiento_contrato_plan    
            and v.codcaracteristica_requerimiento = cr.codcaracteristica_requerimiento    
left outer join valor_posible_requerimiento vp with (nolock) on  v.codcaracteristica_requerimiento = vp.codcaracteristica_requerimiento      
                             and v.valor like vp.valorposible    
where r.desrequerimiento like '%solicitud%instalacion%'    
and ch.etiqueta_a_mostrar like '%motivo%'    
order by rcp.codrequerimiento_contrato_plan, cr.codcaracteristica_requerimiento    
    
SELECT tmi.codrequerimiento_contrato_plan,i.codContrato_Plan,tmi.valor    
into #tmpTipoInstalacion    
FROM #tmpMotInstalacion tmi     
INNER JOIN  #instalaciones i   ON i.codrequerimiento_contrato_plan=tmi.codrequerimiento_contrato_plan    
 /*FIN    
*/    
    
select codContrato_Plan, MAX(rcp.codRequerimiento_Contrato_Plan) as codRequerimiento_Contrato_Plan    
into #ultReq from requerimiento_contrato_plan rcp with (nolock)     
inner join requerimiento r with (nolock) on rcp.codRequerimiento=r.codRequerimiento    
where r.desRequerimiento= 'No desea tarjeta'    
group by codContrato_Plan    
    
select ur.codContrato_Plan,ur.codRequerimiento_Contrato_Plan    
into #tarjeta from #ultReq ur    
left outer join requerimiento_contrato_plan rcp with (nolock) on ur.codRequerimiento_Contrato_Plan = rcp.codRequerimiento_Contrato_Plan    
inner join requerimiento r with (nolock) on rcp.codRequerimiento=r.codRequerimiento    
where r.desRequerimiento= 'No desea tarjeta'    
    
    
select rcp.codrequerimiento_contrato_plan, rcp.codContrato_Plan,    
er.desestadoRequerimiento as EstadoRequerimiento    
into #EstadoNoDeseaTarjeta    
from requerimiento_contrato_plan rcp with (nolock)     
inner join #tarjeta ins on rcp.codrequerimiento_contrato_plan=ins.codrequerimiento_contrato_plan    
inner join estado_requerimiento er on rcp.codestado = er.codestado    
    
    
    
SELECT t.codcontrato_plan     
    ,t.cedula_ruc    
    ,CASE    
    WHEN sc.numeroIdentificacion IS NULL THEN sc2.fechaConsulta    
 ELSE sc.fechaConsulta    
 END AS fechaConsulta    
 ,CASE    
    WHEN sc.numeroIdentificacion IS NULL THEN sc2.fechaUltimaConsulta    
 ELSE sc.fechaUltimaConsulta    
 END AS fechaUltimaConsulta    
 ,CASE    
    WHEN sc.numeroIdentificacion IS NULL THEN sc2.puntajeScore    
 ELSE sc.puntajeScore    
 END AS puntajeScore    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.moroso_tipo    
 ELSE vs.moroso_tipo    
 END AS moroso_tipo    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.tipo_riesgo    
 ELSE vs.tipo_riesgo    
 END AS tipo_riesgo    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.income_predictor    
 ELSE vs.income_predictor    
 END AS income_predictor    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.capacidad_pago    
 ELSE vs.capacidad_pago    
 END AS capacidad_pago    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.probabilidad_pago    
 ELSE vs.probabilidad_pago    
 END AS probabilidad_pago    
 ,CASE    
    WHEN vs.identificacion IS NULL THEN vs2.tiene_tarjeta    
 ELSE vs.tiene_tarjeta    
 END AS tiene_tarjeta    
 INTO #tmpScoreCrediticio    
FROM #tmpprevioM t    
LEFT OUTER JOIN scoreCrediticio sc with (nolock)  ON sc.numeroIdentificacion=t.cedula_ruc    
LEFT OUTER JOIN scoreCrediticio sc2 with (nolock) ON sc2.numeroIdentificacion=t.cedula_Representante    
LEFT OUTER JOIN variable_score_socioeconomica vs with (nolock) ON vs.identificacion=t.cedula_ruc    
LEFT OUTER JOIN variable_score_socioeconomica vs2 with (nolock) ON vs2.identificacion=t.cedula_Representante    
    
    
--estado office    
SELECT sacp.codContratoPlan,     
       ea.desEstado    
    INTO #estado_office    
FROM servicios_adicionales_contratoPlan sacp with (nolock)     
     INNER JOIN promociones_planes_caracteristicas ppc with (nolock) ON ppc.codPromocionCaracteristica = sacp.codPromocionCaracteristica    
     INNER JOIN servicio_adicional_caracteristicas sac with (nolock) ON sac.codServicio_adicional_caracteristicas = ppc.codServicio_adicional_caracteristicas    
     LEFT OUTER JOIN servicios_adicionales sa with (nolock) ON sa.codServicios_adicionales_contratoPlan = sacp.codServicios_adicionales_contratoPlan    
     LEFT OUTER JOIN estado_antivirus ea with (nolock) ON ea.codEstadoAntivirus = sa.codEstadoAntivirus    
WHERE sac.codServicio_adicional_caracteristicas = 12;    
    
    
IF EXISTS (SELECT * FROM consolidado.sys.objects WHERE object_id = OBJECT_ID(N'[consolidado].[dbo].[tmp_Calidad_ActivosMasivosTotal]') AND type in (N'U'))    
      drop table consolidado.dbo.tmp_Calidad_ActivosMasivosTotal    
    
select p.*,    
isnull(v.fecha_primera_factura,'') as Fecha_1ra_Factura,     
isnull(v.Valor_1ra_Factura,0) Valor_1ra_Factura,    
isnull(v.fecha_ultima_factura,'') as Fecha_Ult_Factura,     
isnull(v.valor_ult_Factura,0) Valor_Ult_Factura,    
isnull(CONVERT(VARCHAR(10),v.emisionPriFac, 103),'') emisionPriFac,    
case    
      when v.por_pagar is not null and v.por_pagar<= 0 then 'Si'     
      else 'No'    
end as Pagada_1ra_Factura,    
isnull(dc.pagar,'') as comisionado,    
    
--estado requerimiento    
CASE     
    WHEN nrf.idTicket is null then isnull(fi.EstadoRequerimiento,'')     
    ELSE isnull(nrf.desEstado,'')    
 END as EstadoRequerimiento,    
    
CASE     
    WHEN nrf.idTicket is null THEN fi.fechaRegistro     
    ELSE nrf.fechaRegistroRequerimiento    
end as fechaRegistroRequerimiento,    
    
CASE     
    WHEN nrf.idTicket is NULL THEN isnull(substring(CONVERT(varchar(8),fi.fechaRegistro,112),1,6),'')     
    else isnull(nrf.MesRegistroRequerimiento,'')    
END as MesRegistroRequerimiento,    
    
CASE     
    WHEN nrf.idTicket is NULL THEN isnull(substring(CONVERT(varchar(8),fi.fechaRegistro,112),7,2),'')      
    else isnull(nrf.diaRegistroRequerimiento,'')    
END as diaRegistroRequerimiento,    
    
CASE     
    WHEN nrf.idTicket is null THEN fi.fechaAgendamiento     
    ELSE nrf.fechaInstalacion    
END as fechaInstalacion,    
    
CASE     
    WHEN nrf.idTicket is null THEN isnull(substring(CONVERT(varchar(8),fi.fechaAgendamiento,112),1,6),'')     
    else isnull(nrf.MesInstalacion,'')    
END as MesInstalacion,    
    
    
substring(convert(varchar(244),replace(replace(replace(cp.comentario,CHAR(9),''),CHAR(13),''),CHAR(59),'')),1,244) as comentario,    
case     
when cp.facturar_desde_tarifario=1 and pl.mesesporfactura=1 then t.tarifa_basica    
when cp.facturar_desde_tarifario=1 and pl.mesesporfactura>1 then t.tarifa_basica/pl.mesesporfactura    
when cp.facturar_desde_tarifario=0 and pl.mesesporfactura=1 then cp.valor_servicio    
when cp.facturar_desde_tarifario=0 and pl.mesesporfactura>1 then cp.valor_servicio/pl.mesesporfactura    
end as valorMensual,    
substring(isnull(cel.primerCelular,''),1,50) as primerCelular,    
case  when cp.codCliente_Original <> p.codCliente then cp.codCliente_Original else 0 end as codClienteAnt,     
case  when cp.codContrato_PlanOriginal<>p.codContrato_Plan then cp.codContrato_PlanOriginal else 0 end as codContratoPlanAnt,    
isnull(ndt.EstadoRequerimiento,'') as NoDeseaTarjeta,    
convert(varchar(25),isnull(lon.longitud,'')) as longitud,    
convert(varchar(25),isnull(lat.latitud,'')) as latitud,    
olt.ipOLT    
    
,spl.splitterFibra as Splitter,spl.fechaCreacion as fechaCreacionSplitter, ch.colorHilo,    
SUBSTRING(isnull(vp.piloto,''), 1, 15)  as piloto,ISNULL(gh.grupoHilo,'')as grupoHilo    
,isnull(i.Instalador,'') AS Instalador,    
    
CASE     
    WHEN nrf.idTicket is NULL THEN isnull(tti.valor,'')      
    else nrf.motivoInstalacionMasivo    
END AS TipoInstalacion    
, cargoInstalador    
    
      ,convert(varchar(30),isnull(tme.metrosEstimados,''))as metrosEstimados    
      ,convert(varchar(30),isnull(tmi.metrosInstalados,''))as metrosInstalados    
      ,convert(varchar(30),isnull(tuf.UsuarioFO,''))as UsuarioFO          
      , CASE     
            WHEN pl.esCelerity=1 THEN 'Si'               
            ELSE 'No'    
        END as  esCelerity    
      ,isnull(sc.fechaConsulta,'') as fechaConsulta,    
      CASE    
  WHEN sc.fechaUltimaConsulta IS NOT NULL OR sc.fechaUltimaConsulta <> '' THEN sc.fechaUltimaConsulta    
  ELSE isnull(sc.fechaConsulta,'')    
      END as fechaUltimaConsulta    
         
     ,CASE     
    WHEN tpp2.idTicket is null THEN CONVERT(VARCHAR(10), tpp.fechaPrimeraInstalacion, 103)    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaPrimeraInstalacion, 103)    
    END as fechaPrimeraInstalacion    
    /*    
    ,CASE     
    WHEN tpp2.idTicket is null THEN tpp.MesPrimeraInstalacion    
    ELSE tpp2.mesPrimeraInstalacion    
    END as mesPrimeraInstalacion      
    */    
    ---fecha primera activacion    
    ,CASE     
    WHEN tpp2.idTicket is null THEN CONVERT(VARCHAR(10),tpp.fechaPrimeraInstalacion, 103)    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaPrimeraActivacion, 103)    
    END as fechaPrimeraActivacion    
    /*    
    ,CASE     
    WHEN tpp2.idTicket is null THEN tpp.MesPrimeraInstalacion    
    ELSE tpp2.MesPrimeraActivacion    
    END as mesPrimeraActivacion    
    */    
     ---fecha primer agendamiento    
    ,CASE     
    WHEN tpp2.idTicket is null THEN CONVERT(VARCHAR(10),tpp.fechaPrimeraInstalacion, 103)    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaPrimerAgendamiento, 103)    
    END as fechaPrimerAgendamiento    
    /*    
    ,CASE     
    WHEN tpp2.idTicket is null THEN tpp.MesPrimeraInstalacion    
    ELSE tpp2.MesPrimerAgendamiento    
    END as mesPrimerAgendamiento    
    */    
    ,CASE     
    WHEN tpp2.idTicket is null THEN CONVERT(VARCHAR(10),tpp.fechaRegistro, 103)    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaRegistroRequerimiento, 103)    
    END as fechaTicketPrimeraInstaNuevo    
        
    --fechaReagendamiento    
    ,CASE     
    WHEN tpp2.idTicket is null THEN NULL    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaReagendamiento, 103)    
    END as fechaReagendamiento    
        
    --fechaFactibilidad    
    ,CASE     
    WHEN tpp2.idTicket is null THEN NULL    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaFactibilidad, 103)    
    END as fechaFactibilidad    
        
        
    --fechaGestionOperaciones    
    ,CASE     
    WHEN tpp2.idTicket is null THEN NULL    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaGestionOperaciones, 103)    
    END as fechaGestionOperaciones    
        
    --fechaGestionCliente    
    ,CASE     
    WHEN tpp2.idTicket is null THEN NULL    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaGestionCliente, 103)    
    END as fechaGestionCliente    
        
    --fechaRechazoCliente    
    ,CASE     
    WHEN tpp2.idTicket is null THEN NULL    
    ELSE CONVERT(VARCHAR(10),tpp2.fechaRechazoCliente, 103)    
    END as fechaRechazoCliente,    
    '' as descripcion,    
 vcpcn.des_linea_negocio as des_linea_negocio,    
 vcpcn.des_producto_oracle as des_producto_oracle,    
 vcpcn.des_geografia_oracle as des_geografia_oracle,    
    case when cp.cobrarInstalacion=0 then 'NO' else 'SI' end as cobrarInstalacion    
    
 ,CASE     
  WHEN cpp.codPersonalPreventa=0 THEN 'No Aplica'    
  WHEN cpp.codPersonalPreventa=-1 THEN 'Sin Asignar'    
  WHEN p3.codPersonal IS NOT NULL THEN p3.nombres + ' ' +p3.apellidos    
  ELSE ''    
 END AS preventa    
 --,p.riesgo,p.valuePontencial, p.bancarizado, p.tiene_tarjeta    
 ,eo.desEstado AS subproductoOffice    
 ,vm.macOriginal AS macWifi    
 ,sc.puntajeScore    
 ,sc.moroso_tipo AS morosoTipo     
 ,sc.tipo_riesgo AS tipoRiesgo    
 ,sc.income_predictor AS incomePredictor    
 ,sc.capacidad_pago AS capacidadPago    
 ,sc.probabilidad_pago AS probabilidadPago    
 ,sc.tiene_tarjeta AS tieneTarjeta   
 ---Requerimiento 307
 ,op.codOportunidad    
 ,op.nombreOportunidad   
 ,isnull(dir.barrioSector,'')+' '+isnull(dir.callePrincipal,'')+' '  
 +isnull(dir.numeracion,'')+' '+isnull(dir.calleSecundaria,'')+' '+isnull(dir.Urbanizacion,'')+' '  
 +isnull(dir.referencia,'')  
   
 AS Direccion,  
 CASE     
  WHEN dir.fActualizacion IS NOT NULL  THEN 'SI'   
  ELSE 'NO'    
 END AS EstadoActualizacionCliente  
    
into consolidado.dbo.tmp_Calidad_ActivosMasivosTotal    
from #tmpprevioM p    
inner join contrato_plan cp with (nolock) on p.codcontrato_plan=cp.codContrato_Plan    
inner join planes pl with (nolock) on pl.codplan=cp.codplan    
LEFT outer join detalle_comision dc with (nolock) on dc.codContrato_Plan=cp.codContrato_Plan    
left outer join #tmpPrevFacturas v on p.codcontrato_plan = v.codContrato_Plan    
left outer join #FechaInstalaciones fi on fi.codcontrato_plan=cp.codContrato_Plan    
LEFT OUTER JOIN #instalador i ON i.codContrato_Plan=cp.codContrato_Plan    
LEFT OUTER JOIN #tmpTipoInstalacion tti ON tti.codContrato_Plan=cp.codContrato_Plan    
left outer join #EstadoNoDeseaTarjeta ndt on ndt.codcontrato_plan=cp.codContrato_Plan     
left outer join tarifario t with (nolock) on t.codplan=cp.codplan and t.codservicio=cp.codservicio and t.codancho_banda=cp.codancho_banda    
left outer join view_telefono_celular cel with (nolock) on p.codsucursal = cel.codsucursal and p.codcliente = cel.codcliente    
left outer join view_longitud lon with (nolock) on p.codContrato_Plan = lon.codContrato_Plan    
left outer join view_latitud lat with (nolock) on p.codContrato_Plan = lat.codContrato_Plan    
left outer join view_ipOLT olt with (nolock) on olt.codContrato_Plan=cp.codContrato_Plan    
left outer join view_splitterFibra spl with (nolock) on spl.codContrato_Plan=cp.codContrato_Plan    
left outer join view_colorHilo ch with (nolock) on ch.codContrato_Plan=cp.codContrato_Plan    
left outer join view_piloto vp with (nolock) on  vp.codcontrato_plan=cp.codContrato_Plan    
left outer join view_GrupoHilo gh with (nolock) on gh.codContrato_Plan=cp.codContrato_Plan    
left outer join #tmpMetrosEstimados tme on tme.codContratoPlan=cp.codContrato_Plan    
left outer join #tmpMetrosInstalados tmi on tmi.codContratoPlan=cp.codContrato_Plan    
left outer join #tmpUsuarioFO tuf  on tuf.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN #tmpScoreCrediticio sc ON sc.codcontrato_plan=p.codcontrato_plan    
LEFT OUTER JOIN #tmpUltimoTicketInstNuevoReqFinal nrf ON nrf.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN #PrimeraInstalacion tpp ON tpp.codContrato_Plan=cp.codContrato_Plan    
LEFT OUTER JOIN #tmpprimeroTicketInstNuevoReqFinal tpp2 ON  tpp2.codContratoPlan=cp.codContrato_Plan    
--LEFT OUTER JOIN servicios_adicionales sa on sa.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN view_contratoPlan_costeo_netplus  vcpcn with (nolock) on vcpcn.codContrato_Plan=cp.codContrato_Plan    
--LEFT OUTER JOIN planes_servicios_adicionales psa on psa.codSeguro=sa.codPlanesServicioAdicional    
LEFT OUTER JOIN contrato_plan_personal_preventa cpp with (nolock) ON cpp.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN personal_preventa pp ON pp.id = cpp.codPersonalPreventa    
LEFT OUTER JOIN personal p3 with (nolock) ON p3.codPersonal=pp.codPersonal    
LEFT OUTER JOIN #estado_office eo ON eo.codContratoPlan=cp.codContrato_Plan    
LEFT OUTER JOIN view_mac vm with (nolock) ON cp.codContrato_Plan = vm.codContrato_Plan    
LEFT OUTER JOIN oportunidad op on op.codOportunidad=cp.idOportunidad    
--  
left outer join direccion dir with (nolock) on p.codSucursal = dir.codCiudadSucursal and p.codcliente = dir.codcliente    
order by p.codSucursal,p.codcliente, p.fechaventa    
    
    
      
         
-----CONSOLIDADO REQUERIMIENTOS SOLICITUD DE PAGOS    
    
IF EXISTS (SELECT * FROM consolidado.sys.objects WHERE object_id = OBJECT_ID(N'[consolidado].[dbo].[tmp_Calidad_TransaccionPago]') AND type in (N'U'))    
      drop table consolidado.dbo.tmp_Calidad_TransaccionPago    
    
--drop table #Tipo    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion as Tipo into #Tipo from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join valor_posible_requerimiento vpr on  vpr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento and convert(varchar,vcr.valor)=vpr.valorposible    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Tipo'    
    
--drop table #Entidad    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion as Entidad into #Entidad from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join valor_posible_requerimiento vpr on  vpr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento and convert(varchar,vcr.valor)=vpr.valorposible    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Entidad'    
    
--drop table #Documento    
select vcr.codrequerimiento_contrato_plan,vcr.valor as NumDocumento into #Documento from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Numero Documento'    
    
--drop table #Fecha    
select vcr.codrequerimiento_contrato_plan,vcr.valor as fecha into #Fecha from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Fecha'    
    
--drop table #Valor    
select vcr.codrequerimiento_contrato_plan,vcr.valor as valor into #Valor from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Valor'    
    
--drop table #Comentario    
select vcr.codrequerimiento_contrato_plan,vcr.valor as comentario into #Comentario from caracteristica_requerimiento cr    
inner join valor_caracteristica_requerimiento vcr on  cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento    
inner join caracteristica_helpdesk ch on cr.codCaracteristica=ch.codCaracteristica    
inner join requerimiento r on r.codRequerimiento=cr.codRequerimiento    
where r.desRequerimiento like '%Transaccion de Pago%'    
and ch.descaracteristica='Comentario'    
    
    
SELECT c.desciudad as sucursal,cp.codcliente,    
substring(ltrim(ci.nombres+' '+ci.apellidos+' '+ci.nombre_empresa),1,244) as cliente,    
cp.codcontrato_plan,s.desservicio,p.desplan,ab.desancho_banda,    
rcp.codrequerimiento_contrato_plan,r.desrequerimiento,    
er.desestadorequerimiento,rcp.fechaRequerimiento,u.nombre_usuario as usuarioModificacion,    
u1.nombre_usuario as usuarioAsignado,convert(varchar(50),t.tipo)as tipo, convert(varchar(50),en.entidad)as entidad,    
convert(varchar(50),doc.NumDocumento) as NumDocumento,convert(varchar(50),fec.fecha) as fecha,    
convert(varchar(50),val.valor) as valor,convert(varchar(50),com.comentario) as comentario    
into consolidado.dbo.tmp_Calidad_TransaccionPago    
from requerimiento_contrato_plan rcp with (nolock)     
inner join contrato_plan cp with (nolock) on rcp.codContrato_Plan=cp.codContrato_Plan    
inner join servicio s with (nolock) on s.codservicio=cp.codservicio    
inner join planes p with (nolock) on p.codplan=cp.codplan    
inner join ancho_banda ab with (nolock) on ab.codancho_banda=cp.codancho_banda    
inner join requerimiento r with (nolock) on r.codrequerimiento=rcp.codrequerimiento    
inner join estado_requerimiento er with (nolock) on er.codestado=rcp.codestado    
inner join cliente ci with (nolock) on ci.codciudad_sucursal=cp.codsucursal and  ci.codcliente=cp.codcliente    
inner join ciudad c with (nolock) on rcp.codsucursal=c.codciudad    
inner join usuario u with (nolock) on u.codUsuario=rcp.codUsuarioEstado    
inner join usuario u1 with (nolock) on u1.codUsuario=rcp.codUsuario_Asignado    
left outer join #Tipo t on t.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
left outer join #Entidad en on en.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
left outer join #Documento doc on doc.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
left outer join #Fecha fec on fec.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
left outer join #Valor val on val.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
left outer join #Comentario com on com.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
where r.desrequerimiento like 'Transaccion de Pago'    
and rcp.fecharegistro >= @fechaDesde    
    
    
--drop table #FechaCargo    
select vcr.* into #FechaCargo from valor_caracteristica_requerimiento vcr     
inner join caracteristica_requerimiento cr on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch on cr.codcaracteristica=ch.codcaracteristica    
where ch.descaracteristica='Fecha'    
    
--drop table #MotivoCargo    
select vcr.codrequerimiento_contrato_plan,vpr.descripcion into #MotivoCargo from valor_posible_requerimiento vpr    
inner join valor_caracteristica_requerimiento vcr on vpr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento and convert(varchar,vcr.valor)=vpr.valorposible    
inner join caracteristica_requerimiento cr on cr.codcaracteristica_requerimiento=vpr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch on cr.codcaracteristica=ch.codcaracteristica    
where ch.descaracteristica like'Motivo%'    
    
    
--drop table #ValorCargo    
select vcr.* into #ValorCargo from valor_caracteristica_requerimiento vcr     
inner join caracteristica_requerimiento cr on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch on cr.codcaracteristica=ch.codcaracteristica    
where ch.descaracteristica like 'Valor%'    
    
    
--drop table #ComentarioCargo    
select vcr.* into #ComentarioCargo from valor_caracteristica_requerimiento vcr     
inner join caracteristica_requerimiento cr on cr.codcaracteristica_requerimiento=vcr.codcaracteristica_requerimiento     
inner join caracteristica_helpdesk ch on cr.codcaracteristica=ch.codcaracteristica    
where ch.descaracteristica='Comentario'    
    
    
IF EXISTS (SELECT * FROM consolidado.sys.objects WHERE object_id = OBJECT_ID(N'[consolidado].[dbo].[tmp_Calidad_Cargos_Adicionales]') AND type in (N'U'))    
      drop table consolidado.dbo.tmp_Calidad_Cargos_Adicionales    
    
select tc.destipo_contrato,rcp.codrequerimiento_contrato_plan,    
ciu.abreviatura,((convert(varchar(7),c.codcliente))+' '+ltrim(+c.apellidos+ ' ' +c.nombres) + ' ' + c.nombre_empresa) as Cliente,    
cp.codContrato_Plan,cp.ubicacion,s.desservicio,pl.desplan,ab.desAncho_Banda,ec.desestado_csp,r.desrequerimiento,er.desestadorequerimiento,    
rcp.fecharegistro, fechaRequerimiento,u.nombre_usuario as usuarioRegistro,ur.nombre_usuario as usuarioasignado,    
substring(convert(varchar(2048),rcp.Detalle_actual),1,245) as Detalle_actual,    
substring(convert(varchar(2048),rcp.Detalle_actual),246,245) as Detalle_actual1,    
case    
      when --(er.bloquear =0) and     
      (datediff(hour,dateadd(day,dateDiff(day,rcp.fechaRequerimiento,getdate()),rcp.fechaRequerimiento), getdate())> 0) then    
      ltrim(convert(varchar(10),dateDiff(day,rcp.fechaRequerimiento,getdate()))) + 'D' +' '+ ltrim(convert(varchar(10),datediff(hour,dateadd(day,dateDiff(day,rcp.fechaRequerimiento,getdate()),rcp.fechaRequerimiento), getdate()))) + 'H'            
      when --(er.bloquear =0) and      
    (datediff(hour,dateadd(day,dateDiff(day,rcp.fechaRequerimiento,getdate()),rcp.fechaRequerimiento), getdate())<= 0) then    
     ltrim(convert(varchar(10),dateDiff(day,rcp.fechaRequerimiento,getdate()))) + 'D'     
      else ''    
      end as tiempoTranscurrido, isnull(ui.nombre_usuario,'') as instalador,    
      isnull(w.nodo,'') as nodo,    
      ltrim(rtrim(isnull(pv.apellidos,'') + ' ' + isnull(pv.nombres,''))) as nombreVendedor,    
      case     
when cp.facturar_desde_tarifario=1 then 'Si'    
when cp.facturar_desde_tarifario=0 then 'No'    
end as facturar_desde_tarifario,    
case     
when cp.facturar_desde_tarifario=1 then t.costo_instalacion    
when cp.facturar_desde_tarifario=0 then cp.valor_instalacion    
end as valor_instalacion,    
case     
when cp.facturar_desde_tarifario=1 then t.tarifa_basica    
when cp.facturar_desde_tarifario=0 then cp.valor_servicio    
end as valor_Servicio,    
convert(varchar(200),mc.descripcion) as Motivo,    
convert(varchar(200),fc.valor) as FechaCargo,    
convert(varchar(200),vc.valor) as valor,    
convert(varchar(200),com.valor) as comentario    
into consolidado.dbo.tmp_Calidad_Cargos_Adicionales    
from requerimiento_contrato_plan rcp with (nolock)     
            inner join contrato_plan cp with (nolock) on rcp.codcontrato_plan=cp.codcontrato_plan     
            inner join contrato co with (nolock) on co.codContrato=cp.codContrato    
            inner join tipo_contrato tc with (nolock) on tc.codTipo_Contrato=co.codTipo_Contrato    
            inner join planes pl with (nolock) on pl.codplan=cp.codplan    
            inner join ancho_banda ab with (nolock) on ab.codAncho_Banda = cp.codAncho_Banda    
            inner join estado_csp ec with (nolock) on ec.codEstado_Csp=cp.codEstado_csp    
            inner join cliente c with (nolock) on cp.codsucursal=c.codciudad_sucursal and cp.codcliente=c.codcliente     
            inner join servicio s with (nolock) on cp.codservicio=s.codservicio    
            inner join estado_requerimiento er with (nolock) on rcp.codestado=er.codestado    
            inner join usuario u with (nolock) on rcp.codusuario_registro=u.codusuario    
            inner join personal p with (nolock) on u.codpersonal=p.codpersonal    
            inner join requerimiento_servicio rs with (nolock) on rcp.codrequerimiento = rs.codrequerimiento and    
            rs.codservicio = cp.codservicio    
            inner join requerimiento r with (nolock) on rs.codrequerimiento=r.codrequerimiento    
            inner join ciudad ciu with (nolock) on rcp.codsucursal=ciu.codciudad    
            left outer join usuario us with (nolock) on rcp.codusuarioestado=us.codusuario    
            left outer join usuario ur with (nolock) on rcp.codusuario_asignado=ur.codusuario    
            left outer join vendedor v with (nolock) on cp.codvendedor = v.codvendedor    
            left outer join personal pv with (nolock) on v.codpersonal = pv.codpersonal    
            left outer join usuario ui with (nolock) on rcp.codInstalador=ui.codusuario    
            left outer  join view_nodoWifi W with (nolock) on cp.codcontrato_plan = w.codcontrato_plan    
            left outer join tarifario t with (nolock) on t.codplan=cp.codplan and t.codservicio=cp.codservicio and t.codancho_banda=cp.codancho_banda    
            left outer join #MotivoCargo mc with (nolock) on mc.codrequerimiento_contrato_plan=rcp.codrequerimiento_contrato_plan    
            left outer join #FechaCargo fc with (nolock) on fc.codRequerimiento_Contrato_Plan=rcp.codRequerimiento_Contrato_Plan    
            left outer join #ValorCargo vc with (nolock) on vc.codRequerimiento_Contrato_Plan=rcp.codRequerimiento_Contrato_Plan    
            left outer join #ComentarioCargo com with (nolock) on com.codRequerimiento_Contrato_Plan=rcp.codRequerimiento_Contrato_Plan    
            where  (fechaRequerimiento>=@fechaDesde)    
      
  and r.desRequerimiento like 'Cargos Adicionales'    
            order by fechaRequerimiento    
    
    
    
    
end 
GO

