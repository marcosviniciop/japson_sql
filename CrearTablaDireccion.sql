SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
CREATE TABLE direccion(
    idDir int IDENTITY(1,1) NOT NULL, 
	codCiudadSucursal int NOT NULL,
	codCliente int NOT NULL,
	barrioSector varchar(100) NULL,
	callePrincipal varchar(100) NULL,
	numeracion varchar(25) NULL,
	calleSecundaria varchar(100) NULL,
	Urbanizacion varchar(100) NULL,
	referencia varchar(100) NULL,
	fActualizacion datetime NULL,
	fechaVencimiento datetime NULL,
	informacionActualizada varchar(1) NULL,
	CONSTRAINT PK_direccion PRIMARY KEY (idDir),
    CONSTRAINT FK_cliente_direccion_ciudad FOREIGN KEY (codCiudadSucursal, codCliente) REFERENCES cliente(codCiudad_Sucursal, codCliente)
	);