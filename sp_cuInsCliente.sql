USE [netplus_pruebas]
GO

/****** Object:  StoredProcedure [dbo].[sp_cuInsCliente]    Script Date: 15/6/2022 11:21:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[sp_cuInsCliente](
@i_codEmpresa int,
@i_codCiudad_Sucursal int ,
@i_codCiudad_Cliente int ,
@i_codTipo_Cliente int ,
@i_cedula_ruc varchar(20),
@i_esEmpresa int ,
@i_nombres varchar(100),
@i_apellidos varchar(100),
@i_direccion varchar(255),
@i_sector varchar(100),  --SE LO PUEDE UTILIZAR PARA AGREGAR EL SECTOR
@i_lugar_trabajo varchar(100),
@i_dir_trabajo varchar(100),           
@i_sexo varchar(1),
@i_fecha_nacimiento datetime,           
@i_estado_civil varchar(1),           
@i_numero_hijos_dependientes int,           
@i_nombre_empresa varchar(100),
@i_nombre_comercial varchar(100),
@i_nombres_Representante varchar(100),
@i_apellidos_Representante varchar(100),
@i_cedula_Representante varchar(20),
@i_cargo varchar(100),                       
@i_codTipo_Actividad int ,
@i_codUltimo_ISP int ,
@i_estado varchar(1),
@i_covinco varchar(1),
@i_verificado varchar(1),
@i_observacion varchar(100),
@i_verifica varchar(100),
@i_zona varchar(100),--30
@i_esPrincipal int,
@i_codCourier int,
@i_codUsuario int,
@i_coniva int,
@i_email varchar(255),
@i_sujetoRetencion int,
@i_contribuyenteEspecial int,
@i_exportador int,
@i_nombreReferencial varchar(100),
@i_telefono1 varchar(10),
@i_extension1 varchar(6),
@i_tipoTlf1 varchar(1),
@i_marcaTlf1 smallint,
@i_telefono2 varchar(10),
@i_extension2 varchar(6),
@i_tipoTlf2 varchar(1),
@i_marcaTlf2 smallint,
@i_telefono3 varchar(10),
@i_extension3 varchar(6),
@i_tipoTlf3 varchar(1),--50
@i_marcaTlf3 smallint,
@i_telefono4 varchar(10),
@i_extension4 varchar(6),
@i_tipoTlf4 varchar(1),
@i_marcaTlf4 smallint,
@i_telefono5 varchar(10),
@i_extension5 varchar(6),
@i_tipoTlf5 varchar(1),
@i_marcaTlf5 smallint,
@i_telefono6 varchar(10),
@i_extension6 varchar(6),
@i_tipoTlf6 varchar(1),
@i_marcaTlf6 smallint,
@i_verificaTlf smallint,
@i_codOperadora1 varchar(1),
@i_codOperadora2 varchar(1),
@i_codOperadora3 varchar(1),
@i_codOperadora4 varchar(1),
@i_codOperadora5 varchar(1),
@i_codOperadora6 varchar(1),
@i_codCalificacion    varchar(3),
@i_diapago int,
@i_NombresReferencia varchar(100),
@i_parentesco varchar(50),
@i_telefonos varchar(100),
@i_seguro int,
@i_devolucionSeguro int,
@i_discapacitado int,
@i_codProvincia varchar(10),
@i_codParroquia varchar(10),
@i_facturacionElectronica int,
@i_correoFacturacionElectronica varchar(255),
@i_correoFacturacionElectronica2 varchar(255),
@i_correoFacturacionElectronica3 varchar(255),
@i_codGrupoComercial int,
@i_esCanalVentaPromocional int,	
@i_codCanal int,
@i_codCiudad_Sucursal_Referido int,
@i_codCliente_Referido int,
@i_clave varchar(255),
@i_resultadoModelo varchar(30),
@i_segmentacion varchar(30),
@i_capacidadPago money,
@i_rangoIngresos varchar(30),
@i_formaPago varchar(50),
@i_enviadoTarjeta int,
@i_confirmacionTelefonica int,
@i_calificacionAsesor int,
@i_fechaDiscapacidad date,--99
/*MOFICICACIONES REALIZADAS POR: JAPSON MOREIRA - 22/03/2022*/
@i_barrioSector varchar(100),
@i_callePrincipal varchar(100),
@i_numeracion varchar(25),
@i_calleSecundaria varchar(100),
@i_urbanizacion varchar(100),
@i_referencia varchar(100),

/*FIN MOFICICACIONES REALIZADAS POR: JAPSON MOREIRA - 22/03/2022*/
@i_codCliente int output


)
AS
BEGIN
declare @v_codUsuarioVerifica int,
        @v_fechaVerificaTlf datetime,
        @v_fechaSeguro datetime,
		@v_riesgo varchar(50) = null,
		@v_valuePotencial varchar(60) = null,
		@v_bancarizado varchar(5) = null,
		@v_tieneTarjeta varchar(5) = null


select @v_codUsuarioVerifica =0, @v_fechaVerificaTlf =null
if (@i_verificaTlf >0)
begin
    select @v_codUsuarioVerifica = @i_codUsuario,
    @v_fechaVerificaTlf = getdate()
end

select @i_codCliente= max(codCliente) + 1 from cliente where codCiudad_Sucursal = @i_codCiudad_Sucursal
if (@i_codCliente is null) select @i_codCliente = 1

select @v_fechaSeguro=null
if (@i_seguro >0)
    select @v_fechaSeguro=getdate()
   
select @i_cedula_ruc = replace(ltrim(rtrim(isnull(@i_cedula_ruc,''))),' ','')   
select @i_cedula_ruc = replace(replace(replace(replace(replace(replace(@i_cedula_ruc,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),'')

if @i_nombre_empresa is not null
begin 
	select @i_nombre_empresa = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombre_empresa,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 
if @i_nombre_comercial is not null
begin 
	select @i_nombre_comercial = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombre_comercial,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 

if @i_nombres is not null
begin 
	select @i_nombres = replace(replace(replace(replace(replace(replace(replace(replace(@i_nombres,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 
if @i_apellidos is not null
begin 
	select @i_apellidos = replace(replace(replace(replace(replace(replace(replace(replace(@i_apellidos,CHAR(9),''),CHAR(13),''),CHAR(35),''),CHAR(45),''),CHAR(46),''),CHAR(47),''),'"',''),'''','')
end 

if len(ltrim(rtrim(isnull(@i_email,''))))<12
    select @i_email = ''
else
    select @i_email = ltrim(rtrim(isnull(@i_email,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica,''))))<12
    select @i_correoFacturacionElectronica = ''
else
    select @i_correoFacturacionElectronica = ltrim(rtrim(isnull(@i_correoFacturacionElectronica,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica2,''))))<12
    select @i_correoFacturacionElectronica2 = ''
else
    select @i_correoFacturacionElectronica2 = ltrim(rtrim(isnull(@i_correoFacturacionElectronica2,'')))
if len(ltrim(rtrim(isnull(@i_correoFacturacionElectronica3,''))))<12
    select @i_correoFacturacionElectronica3 = ''
else
    select @i_correoFacturacionElectronica3 = ltrim(rtrim(isnull(@i_correoFacturacionElectronica3,'')))

if (isnull(@i_email,'') <>''    and
    not (
    ascii(substring(isnull(@i_email,''),1,1))>=65 and ascii(substring(isnull(@i_email,''),1,1))<=90 or
    ascii(substring(isnull(@i_email,''),1,1))>=97 and ascii(substring(isnull(@i_email,''),1,1))<=122 or
    ascii(substring(isnull(@i_email,''),1,1))>=48 and ascii(substring(isnull(@i_email,''),1,1))<=57
    ))
    select @i_email = SUBSTRING(@i_email, 2,LEN(@i_email))   
if (isnull(@i_correoFacturacionElectronica,'') <>''
    and
    not (
    ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=90 or
    ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=122 or
    ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica,''),1,1))<=57
    ))
    select @i_correoFacturacionElectronica = SUBSTRING(@i_correoFacturacionElectronica, 2,LEN(@i_correoFacturacionElectronica))
if (isnull(@i_correoFacturacionElectronica2,'') <>''
    and
    not (
    ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=90 or
    ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=122 or
    ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica2,''),1,1))<=57
    ))
    select @i_correoFacturacionElectronica2 = SUBSTRING(@i_correoFacturacionElectronica2, 2,LEN(@i_correoFacturacionElectronica2))

if (isnull(@i_correoFacturacionElectronica3,'') <>''
    and
    not (
    ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=65 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=90 or
    ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=97 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=122 or
    ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))>=48 and ascii(substring(isnull(@i_correoFacturacionElectronica3,''),1,1))<=57
    ))
    select @i_correoFacturacionElectronica3 = SUBSTRING(@i_correoFacturacionElectronica3, 2,LEN(@i_correoFacturacionElectronica3))
   
if @i_telefono1=''
	select @i_tipotlf1='', @i_extension1='', @i_codOperadora1='', @i_marcaTlf1=0
if @i_telefono2=''
	select @i_tipotlf2='', @i_extension2='', @i_codOperadora2='', @i_marcaTlf2=0
if @i_telefono3=''
	select @i_tipotlf3='', @i_extension3='', @i_codOperadora3='', @i_marcaTlf3=0
if @i_telefono4=''
	select @i_tipotlf4='', @i_extension4='', @i_codOperadora4='', @i_marcaTlf4=0
if @i_telefono5=''
	select @i_tipotlf5='', @i_extension5='', @i_codOperadora5='', @i_marcaTlf5=0

insert into Cliente ( codCliente,
codEmpresa, codCiudad_Sucursal, codCiudad_Cliente, codTipo_Cliente,
cedula_ruc, esEmpresa, nombres, apellidos, direccion, sector, lugar_trabajo,
dir_trabajo, sexo, fecha_nacimiento, estado_civil, numero_hijos_dependientes,
nombre_empresa,nombre_comercial, nombres_Representante, apellidos_Representante,
cedula_Representante, cargo, codTipo_Actividad,
codUltimo_ISp, estado, covinco, verificado, observacion, verifica, fecha_verifica,
zona, esPrincipal, codCourier, fecha_Registro,coniva,email,sujetoRetencion,
esContribuyenteEspecial, esExportador, nombreReferencial,
telefono1, extension1, tipoTlf1, marcaTlf1, codOperadora1,
telefono2, extension2, tipoTlf2, marcaTlf2, codOperadora2,
telefono3, extension3, tipoTlf3, marcaTlf3, codOperadora3,
telefono4, extension4, tipoTlf4, marcaTlf4, codOperadora4,
telefono5, extension5, tipoTlf5, marcaTlf5, codOperadora5,
telefono6, extension6, tipoTlf6, marcaTlf6, codOperadora6,
verificaTlf, codUsuarioVerifica, fechaVerificaTlf,calificacion,
diaPago,nombresReferencia,parentescoReferencia,telefonosReferencia,
seguro, fechaSeguro, devolucionSeguro, discapacitado,codProvincia,
codParroquia,codUsuario,facturacionElectronica,correoFacturacionElectronica,
correoFacturacionElectronica2,correoFacturacionElectronica3,
codGrupoComercial, clave, esCanalVentaPromocional , codCanal ,codCiudad_Sucursal_Referido,codCliente_Referido,
resultadoModelo,segmentacion,capacidadPago,rangoIngresos,formaPago,fecha_actualizacion,enviadoTarjeta,
confirmacionTelefonica,calificacionAsesor,fecha_actualizacion_discapacitado,
riesgo,valuePontencial,bancarizado,tiene_tarjeta
) values
(@i_codCliente, @i_codEmpresa, @i_codCiudad_Sucursal, @i_codCiudad_Cliente, @i_codTipo_Cliente,
replace(ltrim(rtrim(isnull(@i_cedula_ruc,''))),' ',''), @i_esEmpresa, ltrim(rtrim(isnull(@i_nombres,''))), ltrim(rtrim(isnull(@i_apellidos,''))),
replace(replace(ltrim(rtrim(isnull(@i_direccion,''))),CHAR(9),''),CHAR(13),''),
ltrim(rtrim(isnull(@i_sector,''))),
@i_lugar_trabajo, @i_dir_trabajo, @i_sexo, @i_fecha_nacimiento, @i_estado_civil,
@i_numero_hijos_dependientes, ltrim(rtrim(isnull(@i_nombre_empresa,''))), ltrim(rtrim(isnull(@i_nombre_comercial,''))),
ltrim(rtrim(isnull(@i_nombres_Representante,''))), ltrim(rtrim(isnull(@i_apellidos_Representante,''))),
ltrim(rtrim(isnull(@i_cedula_Representante,''))),
@i_cargo, @i_codTipo_Actividad,
@i_codUltimo_ISp, @i_estado, @i_covinco, @i_verificado, @i_observacion, @i_verifica,
getDate(), @i_zona, @i_esPrincipal, @i_codCourier, getDate(),@i_coniva,
lower(replace(isnull(@i_email,''),' ','')),
@i_sujetoRetencion, @i_contribuyenteEspecial,@i_exportador,@i_nombreReferencial,
@i_telefono1, @i_extension1, @i_tipoTlf1, @i_marcaTlf1, @i_codOperadora1,
@i_telefono2, @i_extension2, @i_tipoTlf2, @i_marcaTlf2, @i_codOperadora2,
@i_telefono3, @i_extension3, @i_tipoTlf3, @i_marcaTlf3, @i_codOperadora3,
@i_telefono4, @i_extension4, @i_tipoTlf4, @i_marcaTlf4, @i_codOperadora4,
@i_telefono5, @i_extension5, @i_tipoTlf5, @i_marcaTlf5, @i_codOperadora5,
@i_telefono6, @i_extension6, @i_tipoTlf6, @i_marcaTlf6, @i_codOperadora6,
@i_verificaTlf, @v_codUsuarioVerifica, @v_fechaVerificaTlf,@i_codCalificacion,@i_diapago,
@i_NombresReferencia, @i_parentesco,@i_telefonos,@i_seguro, @v_fechaSeguro,
@i_devolucionSeguro, @i_discapacitado,@i_codProvincia,@i_codParroquia, @i_codUsuario,
@i_facturacionElectronica,
lower(replace(isnull(@i_correoFacturacionElectronica,''),' ','')),
lower(replace(isnull(@i_correoFacturacionElectronica2,''),' ','')),
lower(replace(isnull(@i_correoFacturacionElectronica3,''),' ','')),
@i_codGrupoComercial, @i_clave , @i_esCanalVentaPromocional , @i_codCanal ,@i_codCiudad_Sucursal_Referido,@i_codCliente_Referido,
@i_resultadoModelo,@i_segmentacion,@i_capacidadPago,@i_rangoIngresos,@i_formaPago,getDate(),@i_enviadoTarjeta,
@i_confirmacionTelefonica, @i_calificacionAsesor, @i_fechaDiscapacidad,
@v_riesgo,@v_valuePotencial,@v_bancarizado,@v_tieneTarjeta

)

/*MODIFICACIONES REALIZADAS POR: JAPSON MOREIRA - 22/03/2022
INSERTAR LA SEGREGACIÓN DE LOS CAMPOS EN LA NUETA TABLA DE DIRECCION*/

INSERT INTO DIRECCION(codCiudadSucursal, codCliente, barrioSector, callePrincipal, numeracion,
calleSecundaria, Urbanizacion, referencia, fActualizacion, fechaVencimiento) 
VALUES(@i_codCiudad_Sucursal, @i_codCliente, @i_barrioSector, @i_callePrincipal, @i_numeracion, @i_calleSecundaria,
@i_Urbanizacion, @i_referencia, getDate(), getDate())




declare @v_datos_actuales varchar(2048)
select @v_datos_actuales = 'codEmp:' + ltrim(rtrim(convert(varchar(20),@i_codEmpresa))) + CHAR(9) +
'codCiuSuc:' + ltrim(rtrim(convert(varchar(20),@i_codCiudad_Sucursal))) + CHAR(9)+
'codCiuCli:' + ltrim(rtrim(convert(varchar(20),@i_codCiudad_Cliente)))+ CHAR(9)+
'codTipCli:' + ltrim(rtrim(convert(varchar(20),@i_codTipo_Cliente)))+ CHAR(9)+
'cedRuc:' + ltrim(rtrim(@i_cedula_ruc)) +  CHAR(9) +
'esEmp:' + ltrim(rtrim(convert(varchar(20),@i_esEmpresa)))+ CHAR(9)+
'nomb:' + ltrim(rtrim(@i_nombres)) + CHAR(9) +
'apel:' + ltrim(rtrim(@i_apellidos)) + CHAR(9) +
'dire:' + ltrim(rtrim(@i_direccion)) + CHAR(9) +
'sect' + ltrim(rtrim(@i_sector)) + CHAR(9) +
'lugTrab:' + ltrim(rtrim(@i_lugar_trabajo)) + CHAR(9) +
'dirTrab:' + ltrim(rtrim(@i_dir_trabajo)) + CHAR(9) +
'sexo:' + ltrim(rtrim(isnull(@i_sexo,''))) + CHAR(9) +
'fecNac:' + ltrim(rtrim(convert(varchar(8),@i_fecha_nacimiento,112))) + CHAR(9) +
'estCiv:' + ltrim(rtrim(isnull(@i_estado_civil,''))) + CHAR(9) +
'numHij:' + ltrim(rtrim(@i_numero_hijos_dependientes)) + CHAR(9) +
'nomEmp:' + ltrim(rtrim(@i_nombre_empresa)) + CHAR(9)+
'nomCom:' + ltrim(rtrim(@i_nombre_comercial))+ CHAR(9) +
'nomRep:' + ltrim(rtrim(@i_nombres_Representante)) + CHAR(9)+
'apeRep:' + ltrim(rtrim(@i_apellidos_Representante)) + CHAR(9)+
'cedRep:' + ltrim(rtrim(@i_cedula_Representante)) + CHAR(9) +
'cargo:' + ltrim(rtrim(@i_cargo)) + CHAR(9) +
'codTipAct:' + ltrim(rtrim(convert(varchar(20),@i_codTipo_Actividad))) + CHAR(9) +
'codUltISP:' + ltrim(rtrim(convert(varchar(20),@i_codUltimo_ISP))) + CHAR(9) +
'estado:' + ltrim(rtrim(@i_estado)) + CHAR(9) +
'covinco:' + ltrim(rtrim(@i_covinco)) + CHAR(9) +
'verif:' + ltrim(rtrim(@i_verificado)) +  CHAR(9) +
'obser:' + ltrim(rtrim(@i_observacion))+ CHAR(9)+
'verif:' + ltrim(rtrim(@i_verifica)) + CHAR(9) +
'CodCou:' + ltrim(rtrim(convert(varchar(20),@i_codCourier)))+ CHAR(9) +
'coniva:' + ltrim(rtrim(convert(varchar(2),@i_coniva)))+ CHAR(9) +
'esPrin:' + ltrim(rtrim(convert(varchar(20),@i_esPrincipal)))+
'Zona:'+ltrim(rtrim(@i_zona))+ CHAR(9) +
'Email:'+ltrim(rtrim(@i_email))+ CHAR(9) +
'SujRet:'+ltrim(rtrim(@i_sujetoRetencion))+ CHAR(9) +
'ContEsp:'+ltrim(rtrim(@i_contribuyenteEspecial))+ CHAR(9) +
'Exportador:'+ltrim(rtrim(@i_exportador))+ CHAR(9) +
'tlf1:'+ltrim(rtrim(@i_telefono1))+ CHAR(9) +
'Ext1:'+ltrim(rtrim(@i_extension1))+ CHAR(9) +
'tipTlf1:'+ltrim(rtrim(@i_tipoTlf1))+ CHAR(9) +
'codOpe1:'+ltrim(rtrim(@i_codOperadora1))+ CHAR(9) +
'marTlf1:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf1)))+ CHAR(9) +
'tlf2:'+ltrim(rtrim(@i_telefono2))+ CHAR(9) +
'Ext2:'+ltrim(rtrim(@i_extension2))+ CHAR(9) +
'tipTlf2:'+ltrim(rtrim(@i_tipoTlf2))+ CHAR(9) +
'codOpe2:'+ltrim(rtrim(@i_codOperadora2))+ CHAR(9) +
'marTlf2:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf2)))+ CHAR(9) +
'tlf3:'+ltrim(rtrim(@i_telefono3))+ CHAR(9) +
'Ext3:'+ltrim(rtrim(@i_extension3))+ CHAR(9) +
'tipTlf3:'+ltrim(rtrim(@i_tipoTlf3))+ CHAR(9) +
'codOpe3:'+ltrim(rtrim(@i_codOperadora3))+ CHAR(9) +
'marTlf3:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf3)))+ CHAR(9) +
'tlf4:'+ltrim(rtrim(@i_telefono4))+ CHAR(9) +
'Ext4:'+ltrim(rtrim(@i_extension4))+ CHAR(9) +
'tipTlf4:'+ltrim(rtrim(@i_tipoTlf4))+ CHAR(9) +
'codOpe4:'+ltrim(rtrim(@i_codOperadora4))+ CHAR(9) +
'marTlf4:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf4)))+ CHAR(9) +
'tlf5:'+ltrim(rtrim(@i_telefono5))+ CHAR(9) +
'Ext5:'+ltrim(rtrim(@i_extension5))+ CHAR(9) +
'tipTlf5:'+ltrim(rtrim(@i_tipoTlf5))+ CHAR(9) +
'codOpe5:'+ltrim(rtrim(@i_codOperadora5))+ CHAR(9) +
'marTlf5:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf5)))+ CHAR(9) +
'tlf6:'+ltrim(rtrim(@i_telefono6))+ CHAR(9) +
'Ext6:'+ltrim(rtrim(@i_extension6))+ CHAR(9) +
'tipTlf6:'+ltrim(rtrim(@i_tipoTlf6))+ CHAR(9) +
'codOpe6:'+ltrim(rtrim(@i_codOperadora6))+ CHAR(9) +
'marTlf6:' + ltrim(rtrim(convert(varchar(2),@i_marcaTlf6))) + CHAR(9) +
'verifTlf:' + ltrim(rtrim(convert(varchar(2),@i_verificaTlf))) + CHAR(9) +
'codUsuVerif:' + ltrim(rtrim(convert(varchar(10),@v_codUsuarioVerifica))) + CHAR(9) +
'fecVerifTlf:' + ltrim(rtrim(convert(varchar(8),isnull(@v_fechaVerificaTlf,0),112))) +
'Calif:' + ltrim(rtrim(convert(varchar(10),@i_codCalificacion))) + CHAR(9) +
'diaPago:' + ltrim(rtrim(convert(varchar(10),@i_diapago))) +
'NombRef:' + ltrim(rtrim(convert(varchar(100),@i_NombresReferencia))) + CHAR(9) +
'Parentesco:' + ltrim(rtrim(convert(varchar(100),@i_parentesco))) + CHAR(9) +
'TlfRef:' + ltrim(rtrim(convert(varchar(100),@i_telefonos)))+ CHAR(9) +
'Seguro:' + ltrim(rtrim(convert(varchar(1),@i_seguro)))+ CHAR(9) +
'DevSeguro:' + ltrim(rtrim(convert(varchar(1),@i_devolucionSeguro)))+ CHAR(9) +
'discapacitado:' + ltrim(rtrim(convert(varchar(1),@i_discapacitado)))+
'codProvincia:' + ltrim(rtrim(convert(varchar(1),@i_codProvincia)))+ CHAR(9) +
'codParroquia:' + ltrim(rtrim(convert(varchar(1),@i_codParroquia)))+ CHAR(9) +
'facElec:' + ltrim(rtrim(convert(varchar(1),@i_facturacionElectronica)))+ CHAR(9) +
'corFacElec:' + ltrim(rtrim(@i_correoFacturacionElectronica))+ CHAR(9) +
'corFacElec2:' + ltrim(rtrim(@i_correoFacturacionElectronica2))+ CHAR(9)+
'corFacElec3:' + ltrim(rtrim(@i_correoFacturacionElectronica3))+ CHAR(9) +
'grupoComercial:'+ltrim(rtrim(convert(varchar(10),@i_codGrupoComercial)))+ CHAR(9) +
'clave:' + ltrim(rtrim(@i_clave)) + CHAR(9) +
'esCanalVentaPromocional:' + ltrim(rtrim(convert(varchar(1),@i_esCanalVentaPromocional))) + CHAR(9) + 
'codCanal:' + ltrim(rtrim(convert(varchar(10),@i_codCanal))) + CHAR(9) +
'codCiudad_Sucursal_Referido:' + ltrim(rtrim(convert(varchar(20),@i_codCiudad_Sucursal_Referido))) + CHAR(9)+
'codCliente_Referido:' + ltrim(rtrim(convert(varchar(20),@i_codCliente_Referido)))+ CHAR(9)+
'resultadoModelo:' + ltrim(rtrim(convert(varchar(30),isnull(@i_resultadoModelo,''))))+ CHAR(9)+
'segmentacion:' + ltrim(rtrim(convert(varchar(30),isnull(@i_segmentacion,''))))+ CHAR(9)+
'capacidadPago:' + ltrim(rtrim(convert(varchar(30),isnull(@i_capacidadPago,0))))+ CHAR(9)+
'rangoIngresos:' + ltrim(rtrim(convert(varchar(30),isnull(@i_rangoIngresos,''))))+ CHAR(9)+
'formaPago:' + ltrim(rtrim(convert(varchar(30),isnull(@i_formaPago,'')))) + CHAR(9)+
'fecha_Actualizacion:' + ltrim(rtrim(convert(varchar(8),GETDATE(),112))) + CHAR(9)+
'enviadoTarjeta:' + ltrim(rtrim(convert(varchar(30),isnull(@i_enviadoTarjeta,''))))+ CHAR(9)+
'confirmacionTelefonica:' + ltrim(rtrim(convert(varchar(30),isnull(@i_confirmacionTelefonica,''))))+ CHAR(9)+
'calificacionAsesor:' + ltrim(rtrim(convert(varchar(30),isnull(@i_calificacionAsesor,''))))+ CHAR(9)+
'datos_actualizados:' + ltrim(rtrim(convert(varchar(30),1)))
insert into log_general_transaccion (
nombre_Tabla, codSucursal, codigo,  datos_Actuales, fecha_Registro, codUsuario, codProceso)
values('cliente', @i_codCiudad_Sucursal, @i_codCliente,@v_datos_Actuales,getdate(),@i_codUsuario,'I')
END



GO

